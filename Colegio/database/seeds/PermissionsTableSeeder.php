<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Permisos de Usuarios
        Permission::create([
        	'name'	=> 'Navegar usuarios',
        	'slug'	=> 'users.index',
        	'description'	=> 'Lista y navega los usuarios del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Ver detalle de usuario',
        	'slug'	=> 'users.show',
        	'description'	=> 'Ver en detaller cada usuario del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Editar usuarios',
        	'slug'	=> 'users.edit',
        	'description'	=> 'Editar datos de usuarios del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Eliminar usuario',
        	'slug'	=> 'users.destroy',
        	'description'	=> 'Eliminar cualquier usuario del sistema',
        ]);

        //Roles de Usuarios
        Permission::create([
        	'name'	=> 'Navegar roles',
        	'slug'	=> 'roles.index',
        	'description'	=> 'Lista y navega los roles del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Ver detalle de rol',
        	'slug'	=> 'roles.show',
        	'description'	=> 'Ver en detalle cada rol del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Crear roles',
        	'slug'	=> 'roles.create',
        	'description'	=> 'Crear roles del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Editar roles',
        	'slug'	=> 'roles.edit',
        	'description'	=> 'Editar datos de roles del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Eliminar rol',
        	'slug'	=> 'roles.destroy',
        	'description'	=> 'Eliminar cualquier rol del sistema',
        ]);

         //Permisos para Modelo CATEGORIA
        Permission::create([
        	'name'	=> 'Navegar categorias',
        	'slug'	=> 'colegio.categoria.index',
        	'description'	=> 'Lista y navega los categorias del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Ver detalle de categoria',
        	'slug'	=> 'colegio.categoria.show',
        	'description'	=> 'Ver en detalle cada categoria del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Crear categorias',
        	'slug'	=> 'colegio.categoria.create',
        	'description'	=> 'Crear categorias del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Editar categorias',
        	'slug'	=> 'colegio.categoria.edit',
        	'description'	=> 'Editar datos de categorias del sistema',
        ]);
        Permission::create([
        	'name'	=> 'Eliminar categoria',
        	'slug'	=> 'colegio.categoria.destroy',
        	'description'	=> 'Eliminar cualquier categoria del sistema',
        ]);
    }
}
