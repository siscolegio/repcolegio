<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table='direccion';

	protected $primaryKey='id';

	public $timestamps = false;

		protected $fillable = [
		'id_tipo_direccion',
		'id_matricula',
		'calle',
		'numero',
		'id_localidad',
		'provincia',
		'telefono',
		'correo',
		'estado'
	];

	protected $guarded = [	

	];
}
