<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Suspension extends Model
{
    protected $table='suspension';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_matricula',
		'id_resolucion',
		'desde',
		'hasta',
		'actual',
		'estado'		
	];

	protected $guarded = [	

	];
}
