<?php

namespace Colegio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuspensionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_matricula' => 'required',
            'desde' => 'required',
            'hasta' => 'required',
            'numero' => 'required|unique:resolucion|max:20',
        ];
    }
}
