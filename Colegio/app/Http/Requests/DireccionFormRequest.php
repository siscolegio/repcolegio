<?php

namespace Colegio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DireccionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [           
            'calle'=> 'required|max:100',
            'numero'=> 'required|max:20',
            /*'provincia'=> 'required|max:20',*/
            'telefono'=> 'required|max:30',
            'correo'=> 'required|email',
        ];
    }
}
