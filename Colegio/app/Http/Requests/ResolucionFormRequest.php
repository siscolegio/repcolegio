<?php

namespace Colegio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResolucionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero' => 'required|unique:resolucion|max:20',
            'fecha' => 'required',
            'lugar' => 'required|max:100',
            'link' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',             /*son los kb maximo que puede pesar el archivo (definir bien despues)*/
        ];
    }
}
