<?php

namespace Colegio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatriculaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:200',
            'fecha_alta' => 'date_format:d-m-Y',
            'numero_matricula' => 'numeric|min:0|max:999999|unique:matricula',
            'link_foto' => 'mimes:jpeg,jpg,bmp,png|max:2000',              /*son los kb maximo que puede pesar el archivo (definir bien despues)*/
            'nacionalidad' => 'required|max:100',
            /*ver que no se valide cuando se edita el matriculado y no cambia su dni*/
            'dni' => 'required|numeric|min:1000000|max:100000000|unique:matricula',
            'link_dni' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',               /*son los kb maximo que puede pesar el archivo (definir bien despues)(definir tipo de archivo aceptado)*/
            'fecha_nacimiento' => 'required',
            'titulo' => 'required|max:100',
            'link_titulo' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',            /*son los kb maximo que puede pesar el archivo (definir bien despues)(definir tipo de archivo aceptado)*/
            'link_buena_conducta' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',    /*son los kb maximo que puede pesar el archivo (definir bien despues)(definir tipo de archivo aceptado)*/
            //'id_estado' => 'required'
            'numero'=>'required|max:20',
            'correo'=> 'email|max:100',
        ];
    }
}
