<?php

namespace Colegio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CapacitacionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'required|max:200',
            'link_foto' => 'mimes:jpeg,jpg,bmp,png|max:2000',/*son los kb maximo que puede pesar el archivo (definir bien despues)*/
        ];
    }
}
