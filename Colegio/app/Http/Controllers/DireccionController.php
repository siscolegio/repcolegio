<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Colegio\Direccion;
use Colegio\Direccion_Fuero;
use Colegio\Direccion_Categoria;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Colegio\Http\Requests\DireccionFormRequest;
use DB;

class DireccionController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){
		    $query=trim($request->get('searchText'));
		    $mat=DB::table('matricula as m')
		      				->join('estado as e','m.id_estado','=','e.id')
		      				->select('m.id','m.nombre','m.fecha_alta','m.numero_matricula','m.link_foto','m.nacionalidad','m.dni','m.link_dni','m.fecha_nacimiento','m.titulo','m.link_titulo','m.link_buena_conducta','e.nombre as estado')
							->where('m.id','=',$query)
							->where('m.id_estado','=','2')
							->get();
		$matricula=new Matricula;
		foreach ($mat as $m) {
			$matricula=Matricula::findOrFail($m->id);
		}
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas las direcciones del matriculado*/
		$direcciones=DB::table('direccion as d')
							->join('localidad as l','d.id_localidad','=','l.id')
							->join('departamento as dep','l.id_departamento','=','dep.id')
							->join('tipo_direccion as t','d.id_tipo_direccion','=','t.id')
							->select('d.id','t.nombre as tipo_direccion','dep.nombre as departamento','l.nombre as localidad','d.calle','d.numero','d.telefono','d.correo','d.fecha_modificacion')
							->where('id_matricula','=',$matricula->id)
							->where('d.estado','=','1')
							->where('t.estado','=','1')
							->paginate(10);

		      return view('colegio.direccion.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"direcciones"=>$direcciones,"searchText"=>$query]);
		};
	}

	public function create()
	{
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();

		foreach ($matriculas as $Ma) {
			$idMatric=$Ma->id;
		}			

		$localidades=DB::table('localidad as l')
	      				->join('departamento as d','l.id_departamento','=','d.id')
	      				->select(DB::raw('CONCAT(d.nombre," - ",l.nombre) as localidad'),'l.id')
	      				->orderBy('l.id','asc')
						->get();

		$dir=DB::table('direccion')->where('id_matricula','=',$idMatric)->where('id_tipo_direccion','=','1')->where('estado','=','1')->get();
		$direccion=new Direccion;
		foreach ($dir as $d) {
			$direccion=Direccion::findOrFail($d->id);
		}

		return view('colegio.direccion.create',["matriculas"=>$matriculas,"direccion"=>$direccion,"localidades"=>$localidades]);
	}

	public function store(DireccionFormRequest $request)
	{
		/*Carga de los datos de la dirección procesal*/
		$direccion= new Direccion;
		$matricula=Matricula::findOrFail($request->get('id_matricula'));

		$direccion->id_matricula=$request->get('id_matricula');
		$direccion->id_tipo_direccion='2';
		$direccion->id_localidad=$request->get('id_localidad');
		$direccion->calle=$request->get('calle');
		$direccion->numero=$request->get('numero');
		$direccion->Provincia='Entre Ríos';
		$direccion->telefono=$request->get('telefono');
		$direccion->correo=$request->get('correo');
		$direccion->estado='1';
		$direccion->save();

		/*Vuelve a cargar las variables para actualizar el index*/
		/*Busca todos los matriculados*/
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas las direccionesdel matriculado*/
		$direcciones=DB::table('direccion as d')
							->join('localidad as l','d.id_localidad','=','l.id')
							->join('departamento as dep','l.id_departamento','=','dep.id')
							->join('tipo_direccion as t','d.id_tipo_direccion','=','t.id')
							->select('d.id','t.nombre as tipo_direccion','dep.nombre as departamento','l.nombre as localidad','d.calle','d.numero','d.telefono','d.correo','d.fecha_modificacion')
							->where('id_matricula','=',$matricula->id)
							->where('d.estado','=','1')
							->where('t.estado','=','1')
							->paginate(10);

		return redirect()->route('direccion.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"direcciones"=>$direcciones,"searchText"=>$matricula->id])->with('info', 'Dirección guardada con éxito!');
	}

	public function show($id)
	{
		
	}

	public function edit($id)
	{
		$direccion=Direccion::findOrFail($id);
		$matricula=Matricula::findOrFail($direccion->id_matricula);

		$categorias=DB::table('categoria as c')
						->join('matricula_categoria as mc','mc.id_categoria','=','c.id')
	      				->select('c.id','c.nombre')
	      				->where('mc.id_matricula','=',$direccion->id_matricula)
	      				->where('c.estado','=','1')
	      				->where('mc.estado','=','1')
	      				->orderBy('c.id','asc')
						->get();

		$fueros=DB::table('fuero as f')
	      				->select('f.id','f.nombre')
	      				->where('f.estado','=','1')
	      				->orderBy('f.id','asc')
						->get();

		$localidades=DB::table('localidad as l')
	      				->join('departamento as d','l.id_departamento','=','d.id')
	      				->select(DB::raw('CONCAT(d.nombre," - ",l.nombre) as localidad'),'l.id')
	      				->orderBy('l.id','asc')
						->get();

		$catHabilitante=$this::categorias($id);
		$fueHabilitante=$this::fueros($id);

		return view('colegio.direccion.edit',["matricula"=>$matricula,"fueros"=>$fueros,"categorias"=>$categorias,"fueHabilitante"=>$fueHabilitante,"catHabilitante"=>$catHabilitante,"direccion"=>$direccion,"localidades"=>$localidades]);
	}

	public function update(DireccionFormRequest $request,$id)
	{
		$direccion=Direccion::findOrFail($id);
		$matricula=Matricula::findOrFail($direccion->id_matricula);
		$catHabilitante=$this::categorias($id);					/** Lista todas las categorías que tiene habilitada el Matriculado (Activas) **/
		$fueHabilitante=$this::fueros($id);						/** Lista todos los fueros que tiene habilitado el Matriculado (Activos) **/
		$fueros=DB::table('fuero as f') 	/** Lista todas los fueros (Activos) **/
	      				->select('f.id','f.nombre')
	      				->where('f.estado','=','1')
	      				->orderBy('f.id','asc')
						->get();
		$categorias=DB::table('categoria as c')					/*Lista todas las categorias que se puede inscribir el Matriculado (Activas)*/
						->join('matricula_categoria as mc','mc.id_categoria','=','c.id')
	      				->select('c.id','c.nombre')
	      				->where('mc.id_matricula','=',$direccion->id_matricula)
	      				->where('c.estado','=','1')
	      				->where('mc.estado','=','1')
	      				->orderBy('c.id','asc')
						->get();
		/* Recorre todos los fueros (Activos) */
		foreach ($fueros as $fue) {
			/* Pregunta si se tildo ese fuero */
			if ($request->get('fue'.$fue->id)==$fue->id) {
				/* Busca si ese fuero ya no se encuentra cargada en la DB (Activo) */
				$bandera=true;
				foreach ($fueHabilitante as $fueH) {
					if ($fueH->id==$fue->id) {
						$bandera=false;
					}
				}
				/* Si no esta o no esta activo */
				if ($bandera) {
					$dirFue=DB::table('direccion_fuero as df') /* Busca si hay un fuero del matriculado inactivo*/
		      				->select('df.id')
		      				->where('df.id_direccion','=',$direccion->id)
		      				->where('df.id_fuero','=',$fue->id)
							->get();
					$aux=true;
					/* Se recorre el resultado obtenido (por mas que solo sea un registro encontrado) */
					foreach ($dirFue as $df) {
						/* Si se encontro registro se activa */
						if ($df->id!='') {
							$aux=false;
							/* Se instancia un objeto y se carga mediante el ID con los datos de la DB */
							$dFue=Direccion_Fuero::findOrFail($df->id);
							/* Se activa el registro */
							$dFue->estado='1';
							$dFue->update();
						}
					}
					/* Si no se encontro registro se crea y se guarda en la DB*/
					if ($aux) {
						$dFue=new Direccion_Fuero;
							$dFue->id_direccion=$direccion->id;
							$dFue->id_fuero=$fue->id;
							/* Se activa el registro */
							$dFue->estado='1';
							$dFue->save();
					}	
				}
			} else {
				/* Si no esta tildado */
				foreach ($fueHabilitante as $fueH) {
					/* Pregunta si no estaba avtivo ese fuero en la DB */
					if ($fueH->id==$fue->id) {
						$dirFue=DB::table('direccion_fuero as df') /* Busca en la DB ese registro para obtener su ID */
		      				->select('df.id')
		      				->where('df.id_direccion','=',$direccion->id)
		      				->where('df.id_fuero','=',$fue->id)
							->get();
						/* Se recorre el resultado obtenido (por mas que solo sea un registro encontrado) */
						foreach ($dirFue as $df) {
							/* Se instancia un objeto y se carga mediante el ID con los datos de la DB */
							$dFu=Direccion_Fuero::findOrFail($df->id);
							/* Se desactiva el registro */
							$dFu->estado='0';
							$dFu->update();						
						}
					}
				}
			}	
		}
		
		foreach ($categorias as $cat) {
			if ($request->get('cat'.$cat->id)==$cat->id) {
				$bandera=true;
				foreach ($catHabilitante as $catH) {
					if ($catH->id==$cat->id) {
						$bandera=false;
					}
				}
				if ($bandera) {
					$dirCat=DB::table('direccion_categoria as dc')
		      				->select('dc.id')
		      				->where('dc.id_direccion','=',$direccion->id)
		      				->where('dc.id_categoria','=',$cat->id)
							->get();
					$aux=true;
					foreach ($dirCat as $dc) {
						if ($dc->id!='') {
							$aux=false;
							$dCat=Direccion_Categoria::findOrFail($dc->id);
							$dCat->estado='1';
							$dCat->update();
						}
					}
					if ($aux) {
						$dCat=new Direccion_Categoria;
						$dCat->id_direccion=$direccion->id;
						$dCat->id_categoria=$cat->id;
						$dCat->estado='1';
						$dCat->save();
					}
				}
			} else {
				foreach ($catHabilitante as $catH) {
					if ($catH->id==$cat->id) {
						$dirCat=DB::table('direccion_categoria as dc')
		      				->select('dc.id')
		      				->where('dc.id_direccion','=',$direccion->id)
		      				->where('dc.id_categoria','=',$cat->id)
							->get();
						foreach ($dirCat as $dc) {	
							$dCt=Direccion_Categoria::findOrFail($dc->id);
							$dCt->estado='0';
							$dCt->update();
						}
					}
				}
			}
		}

		/*Carga de datos de la dirección para la edición*/
		$direccion->id_localidad=$request->get('id_localidad');
		$direccion->calle=$request->get('calle');
		$direccion->numero=$request->get('numero');
		$direccion->telefono=$request->get('telefono');
		$direccion->correo=$request->get('correo');
		$direccion->estado='0';
		$direccion->update();
		$direccion->estado='1';
		$direccion->update();

		/*return Redirect::to('colegio/direccion');*/
		/*Vuelve a cargar las variables para actualizar el index*/
		/*Busca todos los matriculados*/
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas las direccionesdel matriculado*/
		$direcciones=DB::table('direccion as d')
							->join('localidad as l','d.id_localidad','=','l.id')
							->join('departamento as dep','l.id_departamento','=','dep.id')
							->join('tipo_direccion as t','d.id_tipo_direccion','=','t.id')
							->select('d.id','t.nombre as tipo_direccion','dep.nombre as departamento','l.nombre as localidad','d.calle','d.numero','d.telefono','d.correo','d.fecha_modificacion')
							->where('id_matricula','=',$matricula->id)
							->where('d.estado','=','1')
							->where('t.estado','=','1')
							->paginate(10);

		return redirect()->route('direccion.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"direcciones"=>$direcciones,"searchText"=>$matricula->id])->with('info', 'Dirección editada con éxito!');
	}

	public function destroy($id)
	{
		$direccion=Direccion::findOrFail($id);
		if ($direccion->id_tipo_direccion==1) {
			$mensaje="No se puede eliminar la Dirección Principal";
			$tipoMensaje='danger';
		} else {
			$direccion->estado='0';
			$direccion->update();
			$mensaje="Dirección eliminado con éxito!";
			$tipoMensaje='info';
		}
		
		/*Vuelve a cargar las variables para actualizar el index*/
		$matricula=Matricula::findOrFail($direccion->id_matricula);
		/*Busca todos los matriculados*/
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas las direccionesdel matriculado*/
		$direcciones=DB::table('direccion as d')
							->join('localidad as l','d.id_localidad','=','l.id')
							->join('departamento as dep','l.id_departamento','=','dep.id')
							->join('tipo_direccion as t','d.id_tipo_direccion','=','t.id')
							->select('d.id','t.nombre as tipo_direccion','dep.nombre as departamento','l.nombre as localidad','d.calle','d.numero','d.telefono','d.correo','d.fecha_modificacion')
							->where('id_matricula','=',$matricula->id)
							->where('d.estado','=','1')
							->where('t.estado','=','1')
							->paginate(10);

		return redirect()->route('direccion.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"direcciones"=>$direcciones,"searchText"=>$matricula->id])->with($tipoMensaje, $mensaje);
	}

	static function categorias($id)
	{
		return DB::table('categoria as c')
		      				->join('direccion_categoria as dc','c.id','=','dc.id_categoria')
		      				->select('c.id as id','c.nombre as nombre')
		      				->where('dc.id_direccion','=',$id)
		      				->where('dc.estado','=','1')
		      				->where('c.estado','=','1')
		      				->orderBy('c.id','asc')
							->get();
	}

	static function fueros($id)
	{
		return DB::table('fuero as f')
		      				->join('direccion_fuero as df','f.id','=','df.id_fuero')
		      				->select('f.id as id','f.nombre as nombre')
		      				->where('df.id_direccion','=',$id)
		      				->where('df.estado','=','1')
		      				->where('f.estado','=','1')
		      				->orderBy('f.id','asc')
							->get();
	}

}
