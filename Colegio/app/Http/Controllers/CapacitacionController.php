<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Colegio\Capacitacion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Colegio\Http\Requests\CapacitacionFormRequest;
use DB;

class CapacitacionController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){
		    $query=trim($request->get('searchText'));
		    $mat=DB::table('matricula as m')
		      				->select('m.id','m.nombre','m.fecha_alta','m.numero_matricula','m.link_foto','m.nacionalidad','m.dni','m.fecha_nacimiento','m.titulo')
							->where('m.id','=',$query)
							->where('m.id_estado','=','2')
							->get();
		$matricula=new Matricula;
		foreach ($mat as $m) {
			$matricula=Matricula::findOrFail($m->id);
		}
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas las capacitaciones del matriculado*/
		$capacitaciones=DB::table('capacitacion')
							->select('id','descripcion','id_matricula','link','fecha')
							->where('id_matricula','=',$matricula->id)
							->where('estado','=','1')
							->orderBy('fecha','desc')
							->paginate(10);
		
		return view('colegio.capacitacion.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"capacitaciones"=>$capacitaciones,"searchText"=>$query]);
		};
	}

	public function create()
	{
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();

		return view('colegio.capacitacion.create',["matriculas"=>$matriculas]);
	}

	public function store(CapacitacionFormRequest $request)
	{
		/*Carga de los datos de la capacitación*/
		$capacitacion= new Capacitacion;
		$matricula=Matricula::findOrFail($request->get('id_matricula'));

		$capacitacion->id_matricula=$request->get('id_matricula');
		$capacitacion->descripcion=$request->get('descripcion');
		$capacitacion->fecha=$request->get('fecha');

		if(Input::hasFile('link')){
			$file=Input::file('link');
			$file->move(public_path().'/imagenes/capacitacion/',$capacitacion->fecha." ".$matricula->nombre."-".$file->getClientOriginalName());
			$capacitacion->link=$capacitacion->fecha." ".$matricula->nombre."-".$file->getClientOriginalName();
		}
		
		$capacitacion->estado='1';
		$capacitacion->save();

		/*Vuelve a cargar las variables para actualizar el index*/
		/*Busca todos los matriculados*/
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas las capacitaciones del matriculado*/
		$capacitaciones=DB::table('capacitacion')
							->select('id','descripcion','id_matricula','link','fecha')
							->where('id_matricula','=',$matricula->id)
							->where('estado','=','1')
							->orderBy('id','desc')
							->paginate(10);

		return redirect()->route('capacitacion.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"capacitaciones"=>$capacitaciones,"searchText"=>$matricula->id])->with('info', 'Capacitación guardada con éxito!');
	}

	public function show($id)
	{
		
	}

	public function edit($id)
	{
		$capacitacion=Capacitacion::findOrFail($id);
		$matricula=Matricula::findOrFail($capacitacion->id_matricula);

		return view('colegio.capacitacion.edit',["matricula"=>$matricula,"capacitacion"=>$capacitacion]);
	}

	public function update(CapacitacionFormRequest $request,$id)
	{
		$capacitacion=Capacitacion::findOrFail($id);
		$matricula=Matricula::findOrFail($capacitacion->id_matricula);

		/*Carga de datos de la capacitacion para la edición*/
		$capacitacion->descripcion=$request->get('descripcion');
		$capacitacion->fecha=$request->get('fecha');

		if(Input::hasFile('link')){
			$file=Input::file('link');
			if(($capacitacion->link)!=""){
				File::delete(public_path('imagenes/capacitacion/'.$capacitacion->link));
			}
			$file->move(public_path().'/imagenes/capacitacion/',$capacitacion->fecha." ".$matricula->nombre."-".$file->getClientOriginalName());
			$capacitacion->link=$capacitacion->fecha." ".$matricula->nombre."-".$file->getClientOriginalName();
		}

		$capacitacion->update();

		/*Vuelve a cargar las variables para actualizar el index*/
		/*Busca todos los matriculados*/
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas las capacitaciones del matriculado*/
		$capacitaciones=DB::table('capacitacion')
							->select('id','descripcion','id_matricula','link','fecha')
							->where('id_matricula','=',$matricula->id)
							->where('estado','=','1')
							->orderBy('id','desc')
							->paginate(10);

		return redirect()->route('capacitacion.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"capacitaciones"=>$capacitaciones,"searchText"=>$matricula->id])->with('info', 'Capacitación editada con éxito!');
	}

	public function destroy($id)
	{
		$capacitacion=Capacitacion::findOrFail($id);
		$capacitacion->estado='0';
		$capacitacion->update();
		
		/*Vuelve a cargar las variables para actualizar el index*/
		$matricula=Matricula::findOrFail($capacitacion->id_matricula);
		/*Busca todos los matriculados*/
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas las capacitaciones del matriculado*/
		$capacitaciones=DB::table('capacitacion')
							->select('id','descripcion','id_matricula','link','fecha')
							->where('id_matricula','=',$matricula->id)
							->where('estado','=','1')
							->orderBy('id','desc')
							->paginate(10);

		return redirect()->route('capacitacion.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"capacitaciones"=>$capacitaciones,"searchText"=>$matricula->id])->with('info', 'Capacitación eliminada con éxito!');
	}

}
