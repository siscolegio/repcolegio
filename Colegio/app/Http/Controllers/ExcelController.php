<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class ExcelController extends Controller
{
     public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		Excel::create('Colegio Lista de Profesionales', function($excel) {

            $excel->sheet('Profesionales', function($sheet) {
                $Mats = Matricula::select('matricula.id','matricula.nombre as Apellido y Nombre','matricula.fecha_alta as Fecha Alta','matricula.numero_matricula as Nº Matrícula','matricula.nacionalidad as Nacionalidad','matricula.dni as DNI','matricula.fecha_nacimiento as Fecha Nacimiento','matricula.titulo as Titulo','estado.nombre as Estado','departamento.nombre as Departamento','localidad.nombre as Localidad','direccion.calle as Calle','direccion.numero as Número','direccion.telefono as Teléfono','direccion.correo as Correo')->join('estado','matricula.id_estado','=','estado.id')->join('direccion','direccion.id_matricula','=','matricula.id')->join('localidad','direccion.id_localidad','=','localidad.id')->join('departamento','localidad.id_departamento','=','departamento.id')->where('direccion.id_tipo_direccion','=','1')->orderBy('matricula.nombre', 'asc')->get();
                $sheet->fromArray($Mats);
            });
        })->export('xls');
	}

	public function create()
	{
		Excel::create('Colegio Lista de Profesionales Pendientes de Matriculación', function($excel) {

            $excel->sheet('Profesionales Nuevos', function($sheet) {
                $Mats = Matricula::select('matricula.id','matricula.nombre as Apellido y Nombre','matricula.nacionalidad as Nacionalidad','matricula.dni as DNI','matricula.fecha_nacimiento as Fecha Nacimiento','matricula.titulo as Titulo','departamento.nombre as Departamento','localidad.nombre as Localidad','direccion.calle as Calle','direccion.numero as Número','direccion.telefono as Teléfono','direccion.correo as Correo')->join('direccion','direccion.id_matricula','=','matricula.id')->join('localidad','direccion.id_localidad','=','localidad.id')->join('departamento','localidad.id_departamento','=','departamento.id')->where('direccion.id_tipo_direccion','=','1')->where('matricula.id_estado','=','1')->where('direccion.estado','=','1')->orderBy('matricula.nombre', 'asc')->get();
                $sheet->fromArray($Mats);
            });
        })->export('xls');
	}

	public function store(Request $request)
	{
		
	}

	public function show($id)
	{
		
	}

	public function edit($id)
	{
		
	}

	public function update(Request $request,$id)
	{

	}

	public function destroy($id)
	{

	}

}
