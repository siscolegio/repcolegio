<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\TipoDireccion;
use Illuminate\Support\Facades\Redirect;
use Colegio\Http\Requests\TipoDireccionFormRequest;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Persmission;

use DB;

class TipoDireccionController extends Controller
{
    public function __construct()
	{
		$this->middleware('permission:colegio.tipo_direccion.create')->only(['create', 'store']);
		$this->middleware('permission:colegio.tipo_direccion.index')->only('index');
		$this->middleware('permission:colegio.tipo_direccion.edit')->only(['edit', 'update']);
		$this->middleware('permission:colegio.tipo_direccion.show')->only('show');
		$this->middleware('permission:colegio.tipo_direccion.destroy')->only('destroy');
	}

	public function index(Request $request)
	{		
		if ($request){
		      $query=trim($request->get('searchText'));
		      $tipo_direcciones=DB::table('tipo_direccion')->where('nombre','like','%'.$query.'%')
							->where('estado','=','1')
							->orderBy('id','asc')
							->paginate(10);
		      return view('colegio.tipo_direccion.index',["tipo_direcciones"=>$tipo_direcciones,"searchText"=>$query]);
		};
	}

	public function create()
	{
		return view("colegio.tipo_direccion.create");
	}

	public function store(TipoDireccionFormRequest $request)
	{
		$tipo_direccion=new TipoDireccion;
		$tipo_direccion->nombre=$request->get('nombre');
		$tipo_direccion->estado='1';
		$tipo_direccion->save();
		//return Redirect::to('colegio/tipo_direccion');
		return redirect()->route('tipo_direccion.index')
            ->with('info', 'Tipo de Dirección guardada con éxito');
	}

	public function show($id)
	{
		return view("colegio.tipo_direccion.show",["tipo_direccion"=>TipoDireccion::findOrFail($id)]);
	}

	public function edit($id)
	{
		return view("colegio.tipo_direccion.edit",["tipo_direccion"=>TipoDireccion::findOrFail($id)]);
	}

	public function update(TipoDireccionFormRequest $request,$id)
	{
		$tipo_direccion=TipoDireccion::findOrFail($id);
		$tipo_direccion->nombre=$request->get('nombre');
		$tipo_direccion->update();
		//return Redirect::to('colegio/tipo_direccion');
		return redirect()->route('tipo_direccion.index')
            ->with('info', 'Tipo de Dirección actualizada con éxito');
	}

	public function destroy($id)
	{
		$tipo_direccion=TipoDireccion::findOrFail($id);
		$tipo_direccion->estado='0';
		$tipo_direccion->update();
		//return Redirect::to('colegio/tipo_direccion');
		return back()->with('danger', 'Tipo de Dirección eliminada correctamente');
	}
}
