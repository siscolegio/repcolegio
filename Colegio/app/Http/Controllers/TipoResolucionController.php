<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\TipoResolucion;
use Illuminate\Support\Facades\Redirect;
use Colegio\Http\Requests\TipoResolucionFormRequest;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Persmission;

use DB;

class TipoResolucionController extends Controller
{
    public function __construct()
	{
		$this->middleware('permission:colegio.tipo_resolucion.create')->only(['create', 'store']);
		$this->middleware('permission:colegio.tipo_resolucion.index')->only('index');
		$this->middleware('permission:colegio.tipo_resolucion.edit')->only(['edit', 'update']);
		$this->middleware('permission:colegio.tipo_resolucion.show')->only('show');
		$this->middleware('permission:colegio.tipo_resolucion.destroy')->only('destroy');
	}

	public function index(Request $request)
	{		
		if ($request){
		      $query=trim($request->get('searchText'));
		      $tipo_resoluciones=DB::table('tipo_resolucion')->where('nombre','like','%'.$query.'%')
							->where('estado','=','1')
							->orderBy('id','asc')
							->paginate(10);
		      return view('colegio.tipo_resolucion.index',["tipo_resoluciones"=>$tipo_resoluciones,"searchText"=>$query]);
		};
	}

	public function create()
	{
		return view("colegio.tipo_resolucion.create");
	}

	public function store(TipoResolucionFormRequest $request)
	{
		$tipo_resolucion=new TipoResolucion;
		$tipo_resolucion->nombre=$request->get('nombre');
		$tipo_resolucion->estado='1';
		$tipo_resolucion->save();
		//return Redirect::to('colegio/tipo_resolucion');
		return redirect()->route('tipo_resolucion.index')
            ->with('info', 'Tipo Resolución guardada con éxito');
	}

	public function show($id)
	{
		return view("colegio.tipo_resolucion.show",["tipo_resolucion"=>TipoResolucion::findOrFail($id)]);
	}

	public function edit($id)
	{
		return view("colegio.tipo_resolucion.edit",["tipo_resolucion"=>TipoResolucion::findOrFail($id)]);
	}

	public function update(TipoResolucionFormRequest $request,$id)
	{
		$tipo_resolucion=TipoResolucion::findOrFail($id);
		$tipo_resolucion->nombre=$request->get('nombre');
		$tipo_resolucion->update();
		//return Redirect::to('colegio/tipo_resolucion');
		return redirect()->route('tipo_resolucion.index')
            ->with('info', 'Tipo Resolución actualizada con éxito');
	}

	public function destroy($id)
	{
		$tipo_resolucion=TipoResolucion::findOrFail($id);
		$tipo_resolucion->estado='0';
		$tipo_resolucion->update();
		//return Redirect::to('colegio/tipo_resolucion');
		return back()->with('danger', 'Tipo Resolución eliminada correctamente');
	}
}
