<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Fuero;
use Illuminate\Support\Facades\Redirect;
use Colegio\Http\Requests\FueroFormRequest;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Persmission;

use DB;


class FueroController extends Controller
{
    
	public function __construct()
	{
		$this->middleware('permission:colegio.fuero.create')->only(['create', 'store']);
		$this->middleware('permission:colegio.fuero.index')->only('index');
		$this->middleware('permission:colegio.fuero.edit')->only(['edit', 'update']);
		$this->middleware('permission:colegio.fuero.show')->only('show');
		$this->middleware('permission:colegio.fuero.destroy')->only('destroy');
	}

	public function index(Request $request)
	{		
		if ($request){
		      $query=trim($request->get('searchText'));
		      $fueros=DB::table('fuero')->where('nombre','like','%'.$query.'%')
							->where('estado','=','1')
							->orderBy('id','asc')
							->paginate(10);
		      return view('colegio.fuero.index',["fueros"=>$fueros,"searchText"=>$query]);
		};
	}

	public function create()
	{
		return view('colegio.fuero.create');
	}

	public function store(FueroFormRequest $request)
	{
		$fuero=new Fuero;
		$fuero->nombre=$request->get('nombre');
		$fuero->estado='1';
		$fuero->save();
		//return Redirect::to('colegio/fuero');
		return redirect()->route('fuero.index')
            ->with('info', 'Fuero guardado con éxito');
	}

	public function show($id)
	{
		return view('colegio.fuero.show',["fuero"=>Fuero::findOrFail($id)]);
	}

	public function edit($id)
	{
		return view('colegio.fuero.edit',["fuero"=>Fuero::findOrFail($id)]);
	}

	public function update(FueroFormRequest $request,$id)
	{
		$fuero=Fuero::findOrFail($id);
		$fuero->nombre=$request->get('nombre');
		$fuero->update();
		//return Redirect::to('colegio/fuero');
		return redirect()->route('fuero.index')
            ->with('info', 'Fuero actualizado con éxito');
	}

	public function destroy($id)
	{
		$fuero=Fuero::findOrFail($id);
		$fuero->estado='0';
		$fuero->update();
		//return Redirect::to('colegio/fuero');
		return back()->with('danger', 'Fuero eliminado correctamente');
	}

}
