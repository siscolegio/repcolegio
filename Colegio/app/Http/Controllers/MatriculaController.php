<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Colegio\Resolucion;
use Colegio\Direccion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Colegio\Http\Requests\MatriculaFormRequest;
use DB;

class MatriculaController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){
		     $query=trim($request->get('searchText'));
		     $matriculas=DB::table('matricula as m')
		      				->join('estado as e','m.id_estado','=','e.id')
		      				->select('m.*','e.nombre as estado')
							->where('m.nombre','like','%'.$query.'%')
							->orwhere('m.numero_matricula','like','%'.$query.'%')
							->orderBy('m.id','desc')
							->paginate(10);

		    return view('colegio.matricula.index',["matriculas"=>$matriculas,"searchText"=>$query]);
		};
	}

	public function create()
	{
		$estados=DB::table('estado')->where('estado','=','1')->get();
		$localidades=DB::table('localidad as l')
	      				->join('departamento as d','l.id_departamento','=','d.id')
	      				->select(DB::raw('CONCAT(d.nombre," - ",l.nombre) as localidad'),'l.id')
	      				->orderBy('l.id','asc')
						->get();
		return view('colegio.matricula.create',["estados"=>$estados,"localidades"=>$localidades]);
	}

	public function store(MatriculaFormRequest $request)
	{
		/*Carga de los datos del Profesional*/
		$matricula=new Matricula;
		$matricula->nombre=$request->get('nombre');
		$matricula->fecha_alta=$request->get('fecha_alta');
		$matricula->numero_matricula=$request->get('numero_matricula');

		if(Input::hasFile('link_foto')){
			$file=Input::file('link_foto');
			$file->move(public_path().'/imagenes/matricula/foto/',$matricula->nombre."-".$file->getClientOriginalName());
			$matricula->link_foto=$matricula->nombre."-".$file->getClientOriginalName();
		}

		$matricula->nacionalidad=$request->get('nacionalidad');
		$matricula->dni=$request->get('dni');

		if(Input::hasFile('link_dni')){
			$file=Input::file('link_dni');
			$file->move(public_path().'/imagenes/matricula/dni/',$matricula->nombre."-".$file->getClientOriginalName());
			$matricula->link_dni=$matricula->nombre."-".$file->getClientOriginalName();
		}

		$matricula->fecha_nacimiento=$request->get('fecha_nacimiento');
		$matricula->titulo=$request->get('titulo');

		if(Input::hasFile('link_titulo')){
			$file=Input::file('link_titulo');
			$file->move(public_path().'/imagenes/matricula/titulo/',$matricula->nombre."-".$file->getClientOriginalName());
			$matricula->link_titulo=$matricula->nombre."-".$file->getClientOriginalName();
		}

		if(Input::hasFile('link_buena_conducta')){
			$file=Input::file('link_buena_conducta');
			$file->move(public_path().'/imagenes/matricula/conducta/',$matricula->nombre."-".$file->getClientOriginalName());
			$matricula->link_buena_conducta=$matricula->nombre."-".$file->getClientOriginalName();
		}

		$matricula->id_estado='1';
		$matricula->save();
		
		/*Carga de los datos de la dirección principal*/
		$direccion= new Direccion;

		/*Busca la matricula guardada para obtener su id*/
		$mat=DB::table('matricula')->where('dni','=',$matricula->dni)->get();
		/*Se recorre el resultado de la consulta y se asigna el id obtenido*/
		foreach ($mat as $m) {
			$direccion->id_matricula=$m->id;
		}

		$direccion->id_tipo_direccion='1';
		$direccion->id_localidad=$request->get('id_localidad');
		$direccion->calle=$request->get('calle');
		$direccion->numero=$request->get('numero');
		$direccion->Provincia='Entre Ríos';
		$direccion->telefono=$request->get('telefono');
		$direccion->correo=$request->get('correo');
		$direccion->estado='1';
		$direccion->save();

		return redirect()->route('matricula.index')->with('info', 'Profesional guardado con éxito!');
	}

	public function show($id)
	{
		return view('colegio.matricula.show',["matricula"=>Matricula::findOrFail($id)]);
	}

	public function edit($id)
	{
		$matricula=Matricula::findOrFail($id);
		$estados=DB::table('estado')->where('estado','=','1')->get();
		$localidades=DB::table('localidad as l')
	      				->join('departamento as d','l.id_departamento','=','d.id')
	      				->select(DB::raw('CONCAT(d.nombre," - ",l.nombre) as localidad'),'l.id')
	      				->orderBy('l.id','asc')
						->get();
		$dir=DB::table('direccion')->where('id_matricula','=',$id)->where('id_tipo_direccion','=','1')->where('estado','=','1')->get();
		$direccion=new Direccion;
		foreach ($dir as $d) {
			$direccion=Direccion::findOrFail($d->id);
		}
		$resol=$this->resolucionMatriculacion($id);
		$resolucion=new Resolucion;
		foreach ($resol as $re) {
			$resolucion=Resolucion::findOrFail($re->id);
		}
		$resolB=$this->resolucionBaja($id);
		$resolucionBaja=new Resolucion;
		foreach ($resolB as $re) {
			$resolucionBaja=Resolucion::findOrFail($re->id);
		}
		return view('colegio.matricula.edit',["matricula"=>$matricula,"estados"=>$estados,"localidades"=>$localidades,"direccion"=>$direccion,"resolucion"=>$resolucion,"resolucionBaja"=>$resolucionBaja]);
	}

	public function update(Request $request,$id)
	{
		$this->validate($request,[
			'nombre' => 'required|max:200',
            'fecha_alta' => 'date_format:d-m-Y',
            'link_foto' => 'mimes:jpeg,jpg,bmp,png|max:2000',   /*son los kb maximo que puede pesar el archivo (definir bien despues)*/
            'nacionalidad' => 'required|max:100',         
            'link_dni' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',/*son los kb maximo que puede pesar el archivo (definir bien despues)(definir tipo de archivo aceptado)*/
            'dni' => 'numeric|min:1000000|max:100000000',
            'fecha_nacimiento' => 'required',
            'titulo' => 'required|max:100',
            'link_titulo' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',/*son los kb maximo que puede pesar el archivo (definir bien despues)(definir tipo de archivo aceptado)*/
            'link_buena_conducta' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',/*son los kb maximo que puede pesar el archivo (definir bien despues)(definir tipo de archivo aceptado)*/
            'numeroDir'=>'required|max:20',
            'correo'=> 'required|email|max:100',]);

		$matricula=Matricula::findOrFail($id);
		
		if ($matricula->numero_matricula!='') {
			$this->validate($request,[
					'numero' => 'required|max:20',
		            'fecha' => 'required',
		            'lugar' => 'required|max:100',
		            'link' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',/*Son los kb maximo que puede pesar el archivo (definir bien despues)*/
		            'numero_matricula' => 'required|numeric|min:0|max:999999',]);
			if ($matricula->numero_matricula!=$request->get('numero_matricula')) {
				$this->validate($request,['numero_matricula' => 'required|numeric|min:0|max:999999|unique:matricula',]);
			}
			$resol=$this->resolucionMatriculacion($id);
			$resolucion=new Resolucion;
			foreach ($resol as $re) {
				$resolucion=Resolucion::findOrFail($re->id);
			}
			if ($resolucion->numero!=$request->get('numero')) {
				$this->validate($request,['numero' => 'required|unique:resolucion|max:20',]);
			}
			$matricula->numero_matricula=$request->get('numero_matricula');
			$resolucion->numero=$request->get('numero');
			$resolucion->fecha=$request->get('fecha');
			$resolucion->lugar=$request->get('lugar');
			if(Input::hasFile('link')){
				$file=Input::file('link');
				if(($resolucion->link)!=""){
					File::delete(public_path('imagenes/resolucion/matriculacion/'.$resolucion->link));
				}
				$file->move(public_path().'/imagenes/resolucion/matriculacion/',$matricula->numero_matricula." ".$matricula->nombre."-".$file->getClientOriginalName());
				$resolucion->link=$matricula->numero_matricula." ".$matricula->nombre."-".$file->getClientOriginalName();
			}
			if ($matricula->fecha_baja!='') {
				$matricula->fecha_baja=$request->get('fecha_baja');
				$resol=$this->resolucionBaja($id);
				$resolucionBaja=new Resolucion;
				foreach ($resol as $re) {
					$resolucionBaja=Resolucion::findOrFail($re->id);
				}
				if ($resolucionBaja->numero!=$request->get('numero_resolucion_baja')) {
					$this->validate($request,['numero_resolucion_baja' => 'required|max:20',]);
					if ($this->resolucionNumeroExiste($request->get('numero_resolucion_baja'))) {
						$mensError1="Ya existe ese número de Resolucion";
						$messages = ['err.required' => $mensError1,];
						$this->validate($request,['err'=> 'required',],$messages);
					}
				}
				$resolucionBaja->numero=$request->get('numero_resolucion_baja');
				$resolucionBaja->fecha=$request->get('fechaRB');
				$resolucionBaja->lugar=$request->get('lugarRB');			
				if(Input::hasFile('linkRB')){
					$file=Input::file('linkRB');
					if(($resolucionBaja->link)!=""){
						File::delete(public_path('imagenes/resolucion/baja/'.$resolucionBaja->link));
					}
					$file->move(public_path().'/imagenes/resolucion/baja/',$matricula->numero_matricula." ".$matricula->nombre."-".$file->getClientOriginalName());
					$resolucionBaja->link=$matricula->numero_matricula." ".$matricula->nombre."-".$file->getClientOriginalName();
				}
				$resolucionBaja->update();
			}
			$resolucion->update();
		}

		/* Ver para validar solo si se edita el dni*/
		if($matricula->dni!=$request->get('dni')){
			$this->validate($request,['dni' => 'unique:matricula',]);
		}
		/*Carga de datos del profecional para editar*/
		$matricula->nombre=$request->get('nombre');

		if(Input::hasFile('link_foto')){
			$file=Input::file('link_foto');
			if(($matricula->link_foto)!=""){
				File::delete(public_path('imagenes/matricula/foto/'.$matricula->link_foto));
			}
			$file->move(public_path().'/imagenes/matricula/foto/',$matricula->nombre."-".$file->getClientOriginalName());
			$matricula->link_foto=$matricula->nombre."-".$file->getClientOriginalName();
		}

		$matricula->nacionalidad=$request->get('nacionalidad');

		$matricula->dni=$request->get('dni');

		if(Input::hasFile('link_dni')){
			$file=Input::file('link_dni');
			if(($matricula->link_dni)!=""){
				File::delete(public_path('imagenes/matricula/dni/'.$matricula->link_dni));
			}
			$file->move(public_path().'/imagenes/matricula/dni/',$matricula->nombre."-".$file->getClientOriginalName());
			$matricula->link_dni=$matricula->nombre."-".$file->getClientOriginalName();
		}

		$matricula->fecha_nacimiento=$request->get('fecha_nacimiento');
		$matricula->titulo=$request->get('titulo');

		if(Input::hasFile('link_titulo')){
			$file=Input::file('link_titulo');
			if(($matricula->link_titulo)!=""){
				File::delete(public_path('imagenes/matricula/titulo/'.$matricula->link_titulo));
			}
			$file->move(public_path().'/imagenes/matricula/titulo/',$matricula->nombre."-".$file->getClientOriginalName());
			$matricula->link_titulo=$matricula->nombre."-".$file->getClientOriginalName();
		}

		if(Input::hasFile('link_buena_conducta')){
			$file=Input::file('link_buena_conducta');
			if(($matricula->link_titulo)!=""){
				File::delete(public_path('imagenes/matricula/conducta/'.$matricula->link_buena_conducta));
			}
			$file->move(public_path().'/imagenes/matricula/conducta/',$matricula->nombre."-".$file->getClientOriginalName());
			$matricula->link_buena_conducta=$matricula->nombre."-".$file->getClientOriginalName();
		}

		$matricula->id_estado=$request->get('id_estado');
		/*Carga de datos de la dirección principal para editar*/
		$dir=DB::table('direccion')->where('id_matricula','=',$id)->where('id_tipo_direccion','=','1')->where('estado','=','1')->get();
		$direccion=new Direccion;
		foreach ($dir as $d) {
			$direccion=Direccion::findOrFail($d->id);
		}

		$direccion->id_localidad=$request->get('id_localidad');
		$direccion->calle=$request->get('calle');
		$direccion->numero=$request->get('numeroDir');
		$direccion->Provincia='Entre Ríos';
		$direccion->telefono=$request->get('telefono');
		$direccion->correo=$request->get('correo');

		$direccion->update();
		$matricula->update();

		return redirect()->route('matricula.index')->with('info', 'Profesional editado con éxito!');
	}

	public function destroy($id)
	{
		$matricula=Matricula::findOrFail($id);
		$matricula->id_estado='4';
		$matricula->update();
		return redirect()->route('matricula.index')->with('info', 'Profesional pasado a Inactivo con éxito!');
	}

	static function resolucionMatriculacion($id)
	{
		return DB::table('resolucion as r')
		      				->select('r.*')
		      				->where('r.id_matricula','=',$id)
		      				->where('r.id_tipo_resolucion','=','1')
		      				->where('r.estado','=','1')
							->get();
	}
	static function resolucionNumeroExiste($numero)
	{
		$resul=DB::table('resolucion as r')
		      				->select('r.*')
		      				->where('r.numero','=',$numero)
		      				->where('r.estado','=','1')
							->get();
		$bandera=false;
		foreach ($resul as $res) {
			$bandera=true;
		}
		return $bandera;
	}

	static function resolucionBaja($id)
	{
		return DB::table('resolucion as r')
		      				->select('r.*')
		      				->where('r.id_matricula','=',$id)
		      				->where('r.id_tipo_resolucion','=','3')
		      				->where('r.estado','=','1')
							->get();
	}


	static function localidades($id)
	{
		return DB::table('localidad as l')->where('l.id_departamento','=',$id)->get();
	}

	static function direccion($id)
	{
		$dir=DB::table('direccion as d')
				->join('localidad as l','d.id_localidad','=','l.id')
				->join('departamento as dep','l.id_departamento','=','dep.id')
				->select('d.*','l.nombre as localidad','dep.nombre as departamento')
				->where('id_matricula','=',$id)
				->where('id_tipo_direccion','=','1')
				->where('estado','=','1')
				->get();
		$direccion=new Direccion;
		foreach ($dir as $d) {
			$direccion=$d;
		}
		return $direccion;
	}

}
