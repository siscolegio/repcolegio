<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Colegio\Resolucion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Colegio\Http\Requests\MatriculaFormRequest;
use Colegio\Http\Requests\ResolucionFormRequest;
use DB;
use Carbon\Carbon;


class NuevoController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){
		      $query=trim($request->get('searchText'));
		      $matriculas=DB::table('matricula as m')
		      				->select('m.id','m.nombre','m.fecha_alta','m.numero_matricula','m.link_foto','m.nacionalidad','m.dni','m.link_dni','m.fecha_nacimiento','m.titulo','m.link_titulo','m.link_buena_conducta')
		      				->where('m.nombre','like','%'.$query.'%')
							->where('m.id_estado','=','1')
							->orderBy('m.id','desc')
							->paginate(10);

		      return view('colegio.nuevo.index',["matriculas"=>$matriculas,"searchText"=>$query]);
		};
	}

	public function create()
	{
		
	}

	public function store(ResolucionFormRequest $request)
	{
		
	}

	public function show($id)
	{
		return view('colegio.nuevo.show',["matricula"=>Matricula::findOrFail($id)]);
	}

	public function edit($id)
	{
		$matricula=Matricula::findOrFail($id);
		return view('colegio.nuevo.edit',["matricula"=>$matricula]);
	}

	public function update(Request $request,$id)
	{
		$this->validate($request,[
			'numero' => 'required|unique:resolucion|max:20',
            'fecha' => 'required',
            'lugar' => 'required|max:100',
            'link' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',/*Son los kb maximo que puede pesar el archivo (definir bien despues)*/
            'numero_matricula' => 'required|numeric|min:0|max:999999|unique:matricula',]);

		$matricula=Matricula::findOrFail($id);

		$matricula->numero_matricula=$request->get('numero_matricula');
		$matricula->fecha_alta=Carbon::now();
		$matricula->id_estado='2';

		if (($request->get('numero'))!="") {
			$resolucion=new Resolucion;
			$resolucion->numero=$request->get('numero');
			$resolucion->fecha=$request->get('fecha');
			$resolucion->lugar=$request->get('lugar');
			$resolucion->id_matricula=$matricula->id;
			$resolucion->id_tipo_resolucion='1';
			$resolucion->estado='1';

			if(Input::hasFile('link')){
				$file=Input::file('link');
				$file->move(public_path().'/imagenes/resolucion/matriculacion/',$matricula->numero_matricula." ".$matricula->nombre."-".$file->getClientOriginalName());
				$resolucion->link=$matricula->numero_matricula." ".$matricula->nombre."-".$file->getClientOriginalName();
			}

			$resolucion->save();
		}

		$matricula->update();

		return redirect()->route('nuevo.index')->with('info', 'Profesional Matriculado con éxito!');
	}

	public function destroy($id)
	{
		$matricula=Matricula::findOrFail($id);
		$matricula->id_estado='4';
		$matricula->update();
		return redirect()->route('nuevo.index')->with('info', 'Profesional pasado a Inactivo con éxito!');
	}

}
