<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Categoria;
use Illuminate\Support\Facades\Redirect;
use Colegio\Http\Requests\CategoriaFormRequest;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Persmission;

use DB;

class CategoriaController extends Controller
{
    public function __construct()
	{
		//$this->middleware('auth');
				
		$this->middleware('permission:colegio.categoria.create')->only(['create', 'store']);
		$this->middleware('permission:colegio.categoria.index')->only('index');
		$this->middleware('permission:colegio.categoria.edit')->only(['edit', 'update']);
		$this->middleware('permission:colegio.categoria.show')->only('show');
		$this->middleware('permission:colegio.categoria.destroy')->only('destroy');
		
	}
	/**
	*@return /Illuminate/Http/Response
	*/

	public function index(Request $request)
	{		
		if ($request){
		      $query=trim($request->get('searchText'));
		      $categorias=DB::table('categoria')->where('nombre','like','%'.$query.'%')
							->where('estado','=','1')
							->orderBy('id','asc')
							->paginate(10);
		      return view('colegio.categoria.index',["categorias"=>$categorias,"searchText"=>$query]);
		};
	}

	public function create()
	{
		return view("colegio.categoria.create");
	}

	public function store(CategoriaFormRequest $request)
	{
		$categoria=new Categoria;
		$categoria->nombre=$request->get('nombre');
		$categoria->estado='1';
		$categoria->save();
		//return Redirect::to('colegio/categoria');
		return redirect()->route('categoria.index')
            ->with('info', 'Categoria guardada con éxito');
	}

	public function show($id)
	{
	    return view("colegio.categoria.show",["categoria"=>Categoria::findOrFail($id)]);
	}

	public function edit($id)
	{
		return view("colegio.categoria.edit",["categoria"=>Categoria::findOrFail($id)]);
	}

	public function update(CategoriaFormRequest $request, $id)
	{
		$categoria=Categoria::findOrFail($id);
		$categoria->nombre=$request->get('nombre');
		$categoria->update();
		//return Redirect::to('colegio/categoria');
		return redirect()->route('categoria.index')
            ->with('info', 'Categoria actualizada con éxito');
	}

	public function destroy($id)
	{
		$categoria=Categoria::findOrFail($id);
		$categoria->estado='0';
		$categoria->update();
		//return Redirect::to('colegio/categoria');
		return back()->with('danger', 'Categoria eliminada correctamente');
	}

}
