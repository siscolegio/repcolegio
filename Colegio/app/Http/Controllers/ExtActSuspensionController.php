<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Suspension;
use Colegio\Matricula;
use Colegio\Resolucion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Colegio\Http\Requests\MatriculaFormRequest;
use Colegio\Http\Requests\ResolucionFormRequest;
use Colegio\Http\Requests\SuspensionFormRequest;
use DB;
use Carbon\Carbon;

class ExtActSuspensionController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{
		if ($request){

		      $query1=trim($request->get('searchText1'));
		      $query2=trim($request->get('searchText2'));

		      $query3=Carbon::now();

		      $suspensiones=DB::table('suspension as s')
		      				->join('matricula as m','s.id_matricula','=','m.id')
		      				->join('resolucion as r','s.id_resolucion','=','r.id')
		      				->select('s.id','s.desde','s.hasta','m.nombre as nombre','m.link_foto as foto','m.dni as dni','m.numero_matricula as numero_matricula','r.numero as numero','r.fecha as fecha')
		      				->where('s.estado','=','1')
		      				->where('s.actual','=','1')
		      				->where('m.nombre','like','%'.$query1.'%')
		      				->where('m.numero_matricula','like','%'.$query2.'%')

		      				->where('m.id_estado','=','3')
		      				->whereDate('s.hasta','<=',$query3)
		      				
							->orderBy('s.id','desc')
							->paginate(10);
		     
		     return view('colegio.extactsuspension.index',["suspensiones"=>$suspensiones,"searchText1"=>$query1,"searchText2"=>$query2]);
		 };

	}

	public function create()
	{
		
	}

	public function store(SuspensionFormRequest $request)
	{
		$suspension=new Suspension;
		$matricula=Matricula::findOrFail($request->get('id_matricula'));
		$suspension->id_matricula=$request->get('id_matricula');
		if (($request->get('numero'))!="") {
			$resolucion=new Resolucion;
			$resolucion->numero=$request->get('numero');
			$resolucion->fecha=$request->get('fecha');
			$resolucion->lugar=$request->get('lugar');
			$resolucion->id_matricula=$matricula->id;
			$resolucion->id_tipo_resolucion='2';
			$resolucion->estado='1';

			if(Input::hasFile('link')){
				$file=Input::file('link');
				$file->move(public_path().'/imagenes/resolucion/suspensiones/',$resolucion->numero." ".$matricula->nombre."-".$file->getClientOriginalName());
				$resolucion->link=$resolucion->numero." ".$matricula->nombre."-".$file->getClientOriginalName();
			}
			$resolucion->save();

			$resol=DB::table('resolucion')->where('estado','=','1')->where('numero','=',$resolucion->numero)->get();

			foreach ($resol as $r) {
			
				$suspension->id_resolucion=$r->id;	
			}

		}

		$suspension->desde=$request->get('desde');
		$suspension->hasta=$request->get('hasta');
		$suspension->estado='1';
		$suspension->save();
		
		$matricula->id_estado='3';
		$matricula->update();

		return redirect()->route('suspension.index')->with('info', 'Suspensión extendida con éxito!');
	}

	public function show($id)
	{

	}

	public function edit($id)
	{
		$suspension=Suspension::findOrFail($id);
		$matricula=Matricula::findOrFail($suspension->id_matricula);
		$resolucion=Resolucion::findOrFail($suspension->id_resolucion);
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orwhere('m.id_estado','=','3')
	      				->orderBy('m.nombre','asc')
						->get();

		$fecha = date($suspension->hasta);
		$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
		$suspension->hasta = date ( 'Y-m-j' , $nuevafecha );

		return view("colegio.extactsuspension.edit",["suspension"=>$suspension,"matricula"=>$matricula,"resolucion"=>$resolucion,"matriculas"=>$matriculas]);
	}

		public function update(Request $request,$id)
	{
		
		$this->validate($request,[
			'id_matricula' => 'required',
            'desde' => 'required',
            'hasta' => 'required',]);
		
		$suspension=Suspension::findOrFail($id);
		$matricula=Matricula::findOrFail($request->get('id_matricula'));
		$resolucion=Resolucion::findOrFail($suspension->id_resolucion);


		if (($request->get('id_matricula'))!=($suspension->id_matricula)) {
			$matOld=Matricula::findOrFail($suspension->id_matricula);
			$matOld->id_estado='2';
			$matricula->id_estado='3';
			$matOld->update();
			$matricula->update();
		}
		
		$suspension->id_matricula=$request->get('id_matricula');
		$suspension->desde=$request->get('desde');
		$suspension->hasta=$request->get('hasta');
		
		if ($resolucion->numero!=$request->get('numero')) {
			$this->validate($request,['numero' => 'required|unique:resolucion|max:20',]);
			$resolucion->numero=$request->get('numero');
		}
		$resolucion->fecha=$request->get('fecha');
		$resolucion->lugar=$request->get('lugar');

		if(Input::hasFile('link')){
			$file=Input::file('link');
			if(($resolucion->link)!=""){
				File::delete(public_path('imagenes/resolucion/suspensiones/'.$resolucion->link));
			}
			$file->move(public_path().'/imagenes/resolucion/suspensiones/',$resolucion->numero." ".$matricula->nombre."-".$file->getClientOriginalName());
			$resolucion->link=$resolucion->numero." ".$matricula->nombre."-".$file->getClientOriginalName();
		}

		$resolucion->update();
		$suspension->update();

		return redirect()->route('suspension.index')->with('info', 'Suspensión Extendida con éxito!');
	}

	public function destroy($id)
	{
		$suspension=Suspension::findOrFail($id);
		$matricula=Matricula::findOrFail($suspension->id_matricula);
		$matricula->id_estado='2';
		$matricula->update();
		return redirect()->route('extactsuspension.index')->with('info', 'Matriculado Activado con éxito!');
	}

}
