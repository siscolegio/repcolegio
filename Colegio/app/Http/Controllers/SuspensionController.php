<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Suspension;
use Colegio\Matricula;
use Colegio\Resolucion;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Colegio\Http\Requests\SuspensionFormRequest;
use DB;
use Carbon\Carbon;


class SuspensionController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){

		      $query1=trim($request->get('searchText1'));
		      $query2=trim($request->get('searchText2'));
		      $query3=$request->get('searchText3');
		      $query4=$request->get('searchText4');

		      $suspensiones=null;

		      if(isset($query4) || $query3!=''){
			      if (isset($query4)) {
			      	$fecha = date(Carbon::now());
					$nuevafecha = strtotime ( '+0 day' , strtotime ( $fecha ) ) ;
					$query3 = date ( 'Y-m-j' , $nuevafecha );
			      }

			      $suspensiones=DB::table('suspension as s')
			      				->join('matricula as m','s.id_matricula','=','m.id')
			      				->join('resolucion as r','s.id_resolucion','=','r.id')
			      				->select('s.id','s.desde','s.hasta','m.nombre as nombre','m.link_foto as foto','m.dni as dni','m.numero_matricula as numero_matricula','r.numero as numero','r.fecha as fecha','r.link as link')
			      				->where('s.estado','=','1')
			      				->where('m.nombre','like','%'.$query1.'%')
			      				->where('m.numero_matricula','like','%'.$query2.'%')

			      				->whereDate('s.hasta','>=',$query3)
			      				->whereDate('s.desde','<=',$query3)

								->orderBy('s.id','desc')
								->paginate(10);
			} else {
				$suspensiones=DB::table('suspension as s')
			      				->join('matricula as m','s.id_matricula','=','m.id')
			      				->join('resolucion as r','s.id_resolucion','=','r.id')
			      				->select('s.id','s.desde','s.hasta','m.nombre as nombre','m.link_foto as foto','m.dni as dni','m.numero_matricula as numero_matricula','r.numero as numero','r.fecha as fecha','r.link as link')
			      				->where('s.estado','=','1')
			      				->where('m.nombre','like','%'.$query1.'%')
			      				->where('m.numero_matricula','like','%'.$query2.'%')
								->orderBy('s.id','desc')
								->paginate(10);
			}

		     return view('colegio.suspension.index',["suspensiones"=>$suspensiones,"searchText1"=>$query1,"searchText2"=>$query2,"searchText3"=>$query3,"searchText4"=>$query4]);
		 };

	}

	public function create()
	{
	      $matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();

	     return view('colegio.suspension.create',["matriculas"=>$matriculas]);
		
	}

	public function store(SuspensionFormRequest $request)
	{
		$suspension=new Suspension;
		$matricula=Matricula::findOrFail($request->get('id_matricula'));
		$suspension->id_matricula=$request->get('id_matricula');
		if (($request->get('numero'))!="") {
			$resolucion=new Resolucion;
			$resolucion->numero=$request->get('numero');
			$resolucion->fecha=$request->get('fecha');
			$resolucion->lugar=$request->get('lugar');
			$resolucion->id_matricula=$matricula->id;
			$resolucion->id_tipo_resolucion='2';
			$resolucion->estado='1';

			if(Input::hasFile('link')){
				$file=Input::file('link');
				$file->move(public_path().'/imagenes/resolucion/suspensiones/',$resolucion->fecha." ".$matricula->nombre."-".$file->getClientOriginalName());
				$resolucion->link=$resolucion->fecha." ".$matricula->nombre."-".$file->getClientOriginalName();
			}

			$resolucion->save();
			/*Busca la resolución guardada para obtener su id*/
			$resol=DB::table('resolucion')->where('estado','=','1')->where('numero','=',$resolucion->numero)->get();
			/*Se recorre el resultado de la consulta y se asigna el id obtenido*/
			foreach ($resol as $r) {
				$suspension->id_resolucion=$r->id;
			}
		}
		
		/*Busca la suspensión actual del matriculado*/
		$msusp=DB::table('suspension as s')
		      				->where('s.id_matricula','=',$matricula->id)
		      				->where('s.actual','=','1')
		      				->where('s.estado','=','1')
							->get();
		/*Pone la suspenaión encontrada del matriculado como no actual*/
		foreach ($msusp as $ms) {
			$oS=Suspension::findOrFail($ms->id);
			$oS->actual='0';
			$oS->update();
		}

		$suspension->desde=$request->get('desde');
		$suspension->hasta=$request->get('hasta');
		$suspension->actual='1'; /*Pone a la nueva suspensión como actual*/
		$suspension->estado='1';
		$suspension->save();
		
		$matricula->id_estado='3';
		$matricula->update();

		return redirect()->route('suspension.index')->with('info', 'Suspensión guardada con éxito!');
	}

	public function show($id)
	{
		return view("colegio.suspension.show",["suspension"=>Suspension::findOrFail($id)]);
	}

	public function edit($id)
	{
		$suspension=Suspension::findOrFail($id);
		$matricula=Matricula::findOrFail($suspension->id_matricula);
		$resolucion=Resolucion::findOrFail($suspension->id_resolucion);
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orwhere('m.id_estado','=','3')
	      				->orderBy('m.nombre','asc')
						->get();
		return view("colegio.suspension.edit",["suspension"=>$suspension,"matricula"=>$matricula,"resolucion"=>$resolucion,"matriculas"=>$matriculas]);
	}

	public function update(Request $request,$id)
	{
		$this->validate($request,[
			'id_matricula' => 'required',
            'desde' => 'required',
            'hasta' => 'required',]);

		$suspension=Suspension::findOrFail($id);
		$matricula=Matricula::findOrFail($request->get('id_matricula'));
		$resolucion=Resolucion::findOrFail($suspension->id_resolucion);
		$matOld=Matricula::findOrFail($suspension->id_matricula);

		if (($request->get('id_matricula'))!=($matOld->id)) {
			$matOld->id_estado='2';
			$matricula->id_estado='3';
			$matOld->update();
			$matricula->update();
			
			/*Busca la suspensión actual del matriculado*/
			$msusp=DB::table('suspension as s')
			      				->where('s.id_matricula','=',$matricula->id)
			      				->where('s.actual','=','1')
			      				->where('s.estado','=','1')
								->get();
			/*Pone la suspenaión encontrada del matriculado como no actual*/
			foreach ($msusp as $ms) {
				$oS=Suspension::findOrFail($ms->id);
				$oS->actual='0';
				$oS->update();
			}
		}
		
		$suspension->id_matricula=$request->get('id_matricula');
		$suspension->desde=$request->get('desde');
		$suspension->hasta=$request->get('hasta');

		if ($resolucion->numero!=$request->get('numero')) {
			$this->validate($request,['numero' => 'required|unique:resolucion|max:20',]);
			$resolucion->numero=$request->get('numero');
		}
		
		$resolucion->fecha=$request->get('fecha');
		$resolucion->lugar=$request->get('lugar');

		if(Input::hasFile('link')){
			$file=Input::file('link');
			if(($resolucion->link)!=""){
				File::delete(public_path('imagenes/resolucion/suspensiones/'.$resolucion->link));
			}
			$file->move(public_path().'/imagenes/resolucion/suspensiones/',$resolucion->fecha." ".$matricula->nombre."-".$file->getClientOriginalName());
			$resolucion->link=$resolucion->fecha." ".$matricula->nombre."-".$file->getClientOriginalName();
		}
		
		$resolucion->update();
		$suspension->update();

		if (($request->get('id_matricula'))!=($matOld->id)) {
			/*Busca las suspensiones activas del matriculado anterior*/
			$msusp=DB::table('suspension as s')
			      				->where('s.id_matricula','=',$matOld->id)
			      				->where('s.estado','=','1')
			      				->orderBy('s.id','desc')
								->get();
			/*Recorre las suspenaiones encontrada del matriculado anterior y pone la última cargada como actual*/
			foreach ($msusp as $ms) {
				$oS=Suspension::findOrFail($ms->id);
				$oS->actual='1';
				$oS->update();
				break;
			}
		}

		return redirect()->route('suspension.index')->with('info', 'Suspensión editada con éxito!');
	}

	public function destroy($id)
	{
		$suspension=Suspension::findOrFail($id);
		$matricula=Matricula::findOrFail($suspension->id_matricula);
		$matricula->id_estado='2';
		$matricula->update();
		$suspension->estado='0';
		$suspension->update();

		/*Busca las suspensiones activas del matriculado*/
		$msusp=DB::table('suspension as s')
		      				->where('s.id_matricula','=',$suspension->id_matricula)
		      				->where('s.estado','=','1')
		      				->orderBy('s.id','desc')
							->get();
		/*Recorre las suspenaiones encontrada del matriculado y pone la última cargada como actual*/
		foreach ($msusp as $ms) {
			$oS=Suspension::findOrFail($ms->id);
			$oS->actual='1';
			$oS->update();
			break;
		}

		return redirect()->route('suspension.index')->with('info', 'Suspensión eliminada con éxito!');
	}

}