<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;
use Colegio\User;

use Caffeinated\Shinobi\Models\Role;
use Hash;

use DB;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:users.create')->only(['create', 'store']);
        $this->middleware('permission:users.index')->only('index');
        $this->middleware('permission:users.edit')->only(['edit', 'update']);
        $this->middleware('permission:users.show')->only('show');
        $this->middleware('permission:users.destroy')->only('destroy');
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    }
    public function index()
    {
        $users = User::paginate();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $roles = Role::get();

        return view('users.create', compact('user', 'roles'));
    }

    /**
    *Store a newly created resource in storage
    *
    *@param \Illuminate\Http\Request $request
    *@return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //$user = User::create($request->all());

        $user=new User;
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->password=bcrypt($request->get('password'));
        $user->save();

        //Sincroiza el usuario con las tablas relacionadas de roles
        $user->roles()->sync($request->get('id_rol'));

        return redirect()->route('users.index', $user->id)
            ->with('info', 'Usuario guardado con éxito');
    }

    /**
    * Display the specified resource.
    *
    * @param \Colegio\User $user
    * @return \Illuminate\Http\Response
    */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {   
        $roles = Role::get();
        $roleuser=DB::table('role_user')
                            ->where('user_id','=',$user->id)
                            ->get();

        return view('users.edit', compact('user', 'roles', 'roleuser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //$user->update($request->all());

        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->update();


        //Sincroniza el usuario con las tablas relacionadas de roles
        $user->roles()->sync($request->get('id_rol'));

        return redirect()->route('users.edit', $user->id)
            ->with('info', 'Usuario actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return back()->with('danger', 'Usuario eliminado correctamente');
    }

}