<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Colegio\Resolucion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Colegio\Http\Requests\MatriculaFormRequest;
use Colegio\Http\Requests\ResolucionFormRequest;
use DB;
//use Carbon\Carbon;

class BajaController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){
		    $query=trim($request->get('searchText'));
		    $mat=DB::table('matricula as m')
		      				->join('estado as e','m.id_estado','=','e.id')
		      				->select('m.*','e.nombre as estado')
							->where('m.id','=',$query)
							->where('m.id_estado','=','4')
							->get();
			$matricula=new Matricula;
			foreach ($mat as $m) {
				$matricula=Matricula::findOrFail($m->id);
			}
			$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.dni," ",m.nombre) as matricula'),'m.id')
	      				->where('m.id_estado','=','4')
	      				->orderBy('m.nombre','asc')
						->get();

		      return view('colegio.baja.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"searchText"=>$query]);
		};
	}

	public function create()
	{
		
	}

	public function store(ResolucionFormRequest $request)
	{
		
	}

	public function show($id)
	{
		
	}

	public function edit($id)
	{
		$matricula=Matricula::findOrFail($id);
		return view('colegio.baja.edit',["matricula"=>$matricula]);
	}

	public function update(Request $request,$id)
	{
		$this->validate($request,[
			'numero' => 'required|unique:resolucion|max:20',
            'fecha' => 'required',
            'fecha_baja' => 'required',
            'lugar' => 'required|max:100',
            'link' => 'mimes:jpeg,jpg,bmp,png,pdf|max:2000',/*Son los kb maximo que puede pesar el archivo (definir bien despues)*/
            'numero_matricula' => 'required|numeric|min:0|max:999999',]);

		$matricula=Matricula::findOrFail($id);

		$matricula->fecha_baja=$request->get('fecha_baja');
		$matricula->id_estado='4';

		if (($request->get('numero'))!="") {
			$resolucion=new Resolucion;
			$resolucion->numero=$request->get('numero');
			$resolucion->fecha=$request->get('fecha');
			$resolucion->lugar=$request->get('lugar');
			$resolucion->id_matricula=$matricula->id;
			$resolucion->id_tipo_resolucion='3';/*Baja*/
			$resolucion->estado='1';

			if(Input::hasFile('link')){
				$file=Input::file('link');
				$file->move(public_path().'/imagenes/resolucion/matriculacion/',$matricula->numero_matricula." ".$matricula->nombre."-".$file->getClientOriginalName());
				$resolucion->link=$matricula->numero_matricula." ".$matricula->nombre."-".$file->getClientOriginalName();
			}

			$resolucion->save();
		}

		$matricula->update();

		return redirect()->route('matricula.index')->with('info', 'Profesional dado de baja con éxito!');
	}

	public function destroy($id)
	{
		$matricula=Matricula::findOrFail($id);
		$matricula->fecha_alta=null;
		$matricula->fecha_baja=null;
		$matricula->id_estado='1';
		$matricula->update();
		return redirect()->route('matricula.index')->with('info', 'Profesional editado con éxito!');
	}
}
