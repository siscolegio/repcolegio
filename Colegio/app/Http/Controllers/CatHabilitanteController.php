<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Colegio\Categoria;
use Colegio\Especializacion;
use Colegio\Matricula_Especializacion;
use Colegio\Matricula_Categoria;
use Colegio\Direccion_Categoria;
use Illuminate\Support\Facades\Redirect;
use Colegio\Http\Requests\CatHabilitanteFormRequest;
use DB;

class CatHabilitanteController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){
		    $query=trim($request->get('searchText'));
		    $matriculas=DB::table('matricula as m')
		      				->join('estado as e','m.id_estado','=','e.id')
		      				->select('m.id','m.nombre','m.fecha_alta','m.numero_matricula','m.link_foto','m.nacionalidad','m.dni','m.link_dni','m.fecha_nacimiento','m.titulo','m.link_titulo','m.link_buena_conducta','e.nombre as estado')
							->where('m.nombre','like','%'.$query.'%')
							->where('m.id_estado','=','2')
							->orderBy('m.id','desc')
							->paginate(10);

		      return view('colegio.cathabilitante.index',["matriculas"=>$matriculas,"searchText"=>$query]);
		};
	}

	public function create()
	{

	}

	public function store(SuspensionFormRequest $request)
	{
		
	}

	public function show($id)
	{

	}

	public function edit($id)
	{
		$matricula=Matricula::findOrFail($id);

		$especializaciones=DB::table('especializacion as e')
	      				->select('e.id','e.nombre')
	      				->where('e.estado','=','1')
	      				->orderBy('e.id','asc')
						->get();

		$categorias=DB::table('categoria as c')
	      				->select('c.id','c.nombre')
	      				->where('c.estado','=','1')
	      				->orderBy('c.id','asc')
						->get();

		$espHabilitante=$this::especializaciones($id);
		$catHabilitante=$this::categorias($id);

		return view('colegio.cathabilitante.edit',["matricula"=>$matricula,"especializaciones"=>$especializaciones,"categorias"=>$categorias,"espHabilitante"=>$espHabilitante,"catHabilitante"=>$catHabilitante]);
	}

	public function update(CatHabilitanteFormRequest $request,$id)
	{
		$matricula=Matricula::findOrFail($id);
		$espHabilitante=$this::especializaciones($id);			/** Lista todas las especializaciones que tiene habilitada el Matriculado (Activas) **/
		$catHabilitante=$this::categorias($id);					/** Lista todas las categorías que tiene habilitada el Matriculado (Activas) **/
		$especializaciones=DB::table('especializacion as e') 	/** Lista todas las especializaciones (Activas) **/
	      				->select('e.id','e.nombre')
	      				->where('e.estado','=','1')
	      				->orderBy('e.id','asc')
						->get();
		$categorias=DB::table('categoria as c')					/** lista todas las categorías (Activas) **/
	      				->select('c.id','c.nombre')
	      				->where('c.estado','=','1')
	      				->orderBy('c.id','asc')
						->get();
		/* Recorre todas las especializaciones (Activas) */
		foreach ($especializaciones as $esp) {
			/* Pregunta si se tildo esa especialización */
			if ($request->get('esp'.$esp->id)==$esp->id) {
				/* Busca si esa especialización ya no se encuentra cargada en la DB (Activa) */
				$bandera=true;
				foreach ($espHabilitante as $espH) {
					if ($espH->id==$esp->id) {
						$bandera=false;
					}
				}
				/* Si no esta o no esta activa */
				if ($bandera) {
					$matEsp=DB::table('matricula_especializacion as me') /* Busca si hay una especialización del matriculado inactiva*/
		      				->select('me.id')
		      				->where('me.id_matricula','=',$matricula->id)
		      				->where('me.id_especializacion','=',$esp->id)
							->get();
					$aux=true;
					/* Se recorre el resultado obtenido (por mas que solo sea un registro encontrado) */
					foreach ($matEsp as $me) {
						/* Si se encontro registro se activa */
						if ($me->id!='') {
							$aux=false;
							/* Se instancia un objeto y se carga mediante el ID con los datos de la DB */
							$mEsp=Matricula_Especializacion::findOrFail($me->id);
							/* Se activa el registro */
							$mEsp->estado='1';
							$mEsp->update();
							/*echo "Se modifico (Matricula $matricula->id : esp $esp->id) <br>";*/
						}
					}
					/* Si no se encontro registro se crea y se guarda en la DB*/
					if ($aux) {
						$mEsp=new Matricula_Especializacion;
							$mEsp->id_matricula=$matricula->id;
							$mEsp->id_especializacion=$esp->id;
							/* Se activa el registro */
							$mEsp->estado='1';
							$mEsp->save();
							/*echo "Se creo una relación (Matricula $matricula->id : esp $esp->id) <br>";*/
					}	
				}
			} else {
				/* Si no esta tildado */
				foreach ($espHabilitante as $espH) {
					/* Pregunta si no estaba avtiva esa especialización en la DB */
					if ($espH->id==$esp->id) {
						$matEsp=DB::table('matricula_especializacion as me') /* Busca en la DB ese registro para obtener su ID */
		      				->select('me.id')
		      				->where('me.id_matricula','=',$matricula->id)
		      				->where('me.id_especializacion','=',$esp->id)
							->get();
						/* Se recorre el resultado obtenido (por mas que solo sea un registro encontrado) */
						foreach ($matEsp as $me) {
							/* Se instancia un objeto y se carga mediante el ID con los datos de la DB */
							$mEs=Matricula_Especializacion::findOrFail($me->id);
							/* Se desactiva el registro */
							$mEs->estado='0';
							$mEs->update();
							/*echo "Se borro (Matricula $matricula->id : esp $esp->id) <br>";*/
						}
					}
				}
			}	
		}
		
		foreach ($categorias as $cat) {
			if ($request->get('cat'.$cat->id)==$cat->id) {
				$bandera=true;
				foreach ($catHabilitante as $catH) {
					if ($catH->id==$cat->id) {
						$bandera=false;
					}
				}
				if ($bandera) {
					$matCat=DB::table('matricula_categoria as mc')
		      				->select('mc.id')
		      				->where('mc.id_matricula','=',$matricula->id)
		      				->where('mc.id_categoria','=',$cat->id)
							->get();
					$aux=true;
					foreach ($matCat as $mc) {
						if ($mc->id!='') {
							$aux=false;
							$mCat=Matricula_Categoria::findOrFail($mc->id);
							$mCat->estado='1';
							$mCat->update();
							/*echo "Se modifico (Matricula $matricula->id : cat $cat->id) <br>";*/
						}
					}
					if ($aux) {
						$mCat=new Matricula_Categoria;
							$mCat->id_matricula=$matricula->id;
							$mCat->id_categoria=$cat->id;
							$mCat->estado='1';
							$mCat->save();
							/*echo "Se creo una relación (Matricula $matricula->id : cat $cat->id) <br>";*/
					}
				}
			} else {
				foreach ($catHabilitante as $catH) {
					if ($catH->id==$cat->id) {
						$matCat=DB::table('matricula_categoria as mc')
		      				->select('mc.id')
		      				->where('mc.id_matricula','=',$matricula->id)
		      				->where('mc.id_categoria','=',$cat->id)
							->get();
						foreach ($matCat as $mc) {	
							$mCt=Matricula_Categoria::findOrFail($mc->id);
							$mCt->estado='0';
							$mCt->update();
							/*Si se inactiva una categoría habilitante se debe poner inactiva tambien en la direccion que este asociada*/
							$dCat=DB::table('direccion_categoria as dc')	/*Bueca en las direcciones del matriculado si existe la categoria*/
								->join('direccion as d','d.id','=','dc.id_direccion')
			      				->select('dc.id')
			      				->where('d.id_matricula','=',$matricula->id)
			      				->where('dc.id_categoria','=',$cat->id)
								->get();
							foreach ($dCat as $dc) {
								$oDC=Direccion_Categoria::findOrFail($dc->id);
								$oDC->estado='0'; /*Si se encuantra una asociacion se coloca inactiva*/
								$oDC->update();
							}
							/*echo "Se borro (Matricula $matricula->id : cat $cat->id) <br>";*/
						}
					}
				}
			}
		}

		return redirect()->route('cathabilitante.index')->with('info', 'Especializaciones y Categorias habilitantes cargadas con éxito!');
	}

	public function destroy($id)
	{
		
	}

	static function especializaciones($id)
	{
		return DB::table('especializacion as e')
		      				->join('matricula_especializacion as me','e.id','=','me.id_especializacion')
		      				->select('e.id as id','e.nombre as nombre')
		      				->where('me.id_matricula','=',$id)
		      				->where('me.estado','=','1')
		      				->where('e.estado','=','1')
							->get();
		
	}

	static function categorias($id)
	{
		return DB::table('categoria as c')
		      				->join('matricula_categoria as mc','c.id','=','mc.id_categoria')
		      				->select('c.id as id','c.nombre as nombre')
		      				->where('mc.id_matricula','=',$id)
		      				->where('mc.estado','=','1')
		      				->where('c.estado','=','1')
							->get();
	}
	
}
