<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Especializacion;
use Illuminate\Support\Facades\Redirect;
use Colegio\Http\Requests\EspecializacionFormRequest;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Persmission;

use DB;

class EspecializacionController extends Controller
{
    public function __construct()
	{
		$this->middleware('permission:colegio.especializacion.create')->only(['create', 'store']);
		$this->middleware('permission:colegio.especializacion.index')->only('index');
		$this->middleware('permission:colegio.especializacion.edit')->only(['edit', 'update']);
		$this->middleware('permission:colegio.especializacion.show')->only('show');
		$this->middleware('permission:colegio.especializacion.destroy')->only('destroy');
	}

	public function index(Request $request)
	{		
		if ($request){
		      $query=trim($request->get('searchText'));
		      $especializaciones=DB::table('especializacion')->where('nombre','like','%'.$query.'%')
							->where('estado','=','1')
							->orderBy('id','asc')
							->paginate(10);
		      return view('colegio.especializacion.index',["especializaciones"=>$especializaciones,"searchText"=>$query]);
		};
	}

	public function create()
	{
		return view("colegio.especializacion.create");
	}

	public function store(EspecializacionFormRequest $request)
	{
		$especializacion=new Especializacion;
		$especializacion->nombre=$request->get('nombre');
		$especializacion->estado='1';
		$especializacion->save();
		//return Redirect::to('colegio/especializacion');
			return redirect()->route('especializacion.index')
            ->with('info', 'Especialización guardada con éxito');
	}

	public function show($id)
	{
		return view("colegio.especializacion.show",["especializacion"=>Especializacion::findOrFail($id)]);
	}

	public function edit($id)
	{
		return view("colegio.especializacion.edit",["especializacion"=>Especializacion::findOrFail($id)]);
	}

	public function update(especializacionFormRequest $request,$id)
	{
		$especializacion=Especializacion::findOrFail($id);
		$especializacion->nombre=$request->get('nombre');
		$especializacion->update();
		//return Redirect::to('colegio/especializacion');
		return redirect()->route('especializacion.index')
            ->with('info', 'Especialización actualizada con éxito');
	}

	public function destroy($id)
	{
		$especializacion=Especializacion::findOrFail($id);
		$especializacion->estado='0';
		$especializacion->update();
		//return Redirect::to('colegio/especializacion');
		return back()->with('danger', 'Especialización eliminada correctamente');
	}
}
