<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Colegio\Pago;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Colegio\Http\Requests\PagoFormRequest;
use DB;
use Carbon\Carbon;

class MorosoController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){
		    $query1=trim($request->get('searchText1'));
		    $query2=trim($request->get('searchText2'));

			$matricula=new Matricula;
			$matri=DB::table('matricula as m')
	      				->select('m.*')
						->where('m.id','=',$query1)
						->get();
			foreach ($matri as $m) {
				$matricula=Matricula::findOrFail($m->id);
			}
		    

			$tipo_pagos=DB::table('tipo_pago')->get();

			$matriculas=DB::table('matricula as m')
		      				->select(DB::raw('CONCAT(m.dni," ",m.nombre) as matricula'),'m.id')
		      				->where('m.id_estado','<>','4')
		      				->orderBy('m.nombre','asc')
							->get();
			
			$mats=DB::table('matricula as m')
		      				->select('m.*')
		      				->where('m.id_estado','<>','4')
		      				->orderBy('m.nombre','asc')
							->get();
			
			$morosos = Array();
			/*Busca todos los matriculados que adeuden la Inscripción*/
			if ($query2!='') {
				if ($query2==1){
					if ($query1!='') {
						$morosos=$this->debe_inscripcion($query1);
					} else {
						$morosos=$this->deben_inscripcion();
					}
				} else if ($query2==2) { /*Busca todos los matriculados que adeuden una cuota*/					
					if ($query1!='') {
						if ($matricula->numero_matricula!=''){
							/*Función que busca las cuotas que adeuda el Matriculado*/
							$arrayPPdebe = $this->periodos_adeudados($matricula);
							$contPPdebe=count($arrayPPdebe);
							/*Si adeuda alguna cuota*/
							if($contPPdebe>0){
								/*Cargar Matriculado a una lista como Moroso($morosos)*/
								array_push($morosos,$matricula);
							}
							/*echo "Array Todos los Periodos Adeudados<br>";
							echo var_dump($arrayPPdebe)."<br>";
							echo "FIN... <br><br>";*/
						}
					} else {
						foreach ($mats as $mat) {
							if ($mat->numero_matricula!=''){
								/*Función que busca las cuotas que adeuda el Matriculado*/
								$arrayPPdebe = $this->periodos_adeudados($mat);
								$contPPdebe=count($arrayPPdebe);
								/*Si adeuda alguna cuota*/
								if($contPPdebe>0){
									/*Cargar Matriculado a una lista como Moroso($morosos)*/
									array_push($morosos,$mat);
								}
								/*echo "Array Todos los Periodos Adeudados<br>";
								echo var_dump($arrayPPdebe)."<br>";
								echo "FIN... <br><br>";*/
							}
						}
					}
				}
			} else if ($query1!='') {

				//Buscar Si debe la Matricula y las cuotas el matriculado
				/*Ver si pongo esta opción*/

			}
		
			return view('colegio.moroso.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"morosos"=>$morosos,"tipo_pagos"=>$tipo_pagos,"searchText1"=>$query1,"searchText2"=>$query2]);
		};
	}

	public function create()
	{
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.numero_matricula," ",m.nombre," ",m.dni) as matricula'),'m.id')
	      				->where('m.id_estado','=','2')
	      				->orderBy('m.nombre','asc')
						->get();
		$tipo_pagos=DB::table('tipo_pago')->get();

		return view('colegio.pago.create',["matriculas"=>$matriculas,"tipo_pagos"=>$tipo_pagos]);
	}

	public function store(PagoFormRequest $request)
	{
		
	}

	public function show($id)
	{
		
	}

	public function edit($id)
	{

	}

	public function update(CapacitacionFormRequest $request,$id)
	{

	}

	public function destroy($id)
	{
		
	}

	static function periodos_pago($id)
	{
		return DB::table('periodo_pago as pp')
		      				->select('pp.*')
		      				->where('pp.id_pago_matricula','=',$id)
		      				->where('pp.estado','=','1')
		      				->orderBy('pp.id','desc')
							->get();
	}

	static function periodos_adeudados($mat)
	{
		/*Ver si hacer una función para buscar los periodos de pagos adeudados*/
		$fecha = date(Carbon::now());
		$nuevafecha = strtotime ( '+0 day' , strtotime ( $fecha ) ) ;
		$mesAct = (int) date ( 'm' , $nuevafecha );
		$anioAct= (int) date ( 'Y' , $nuevafecha );
		$arrayPPMorosos = array();
		/*Buscar cuotas adeudadas*/
		if ($mat->numero_matricula!=''){
			$fecha2 = date($mat->fecha_alta);
			$nuevafecha2 = strtotime ( '+0 day' , strtotime ( $fecha2 ) ) ;
			$mesMat= (int) date ( 'm' , $nuevafecha2 );
			$anioMat= (int) date ( 'Y' , $nuevafecha2 );
			$aux=0;
			$pps=DB::table('periodo_pago as pp')
					->join('pago_matricula as pm','pm.id','=','pp.id_pago_matricula')
      				->select('pp.id','pp.mes','pp.anio')
      				->where('pm.id_matricula','=',$mat->id)
      				->where('pm.estado','=','1')
      				->where('pp.estado','=','1')
      				->orderBy('pp.id','asc')
      				->get();

      		$anioA=$anioAct+1;$mesM=$mesMat+1;
      		/*echo "id_matricula = ".$mat->id." // Período de Alta ( ".$mesMat." / ".$anioMat." ) // ";
      		echo "Período Actual ( ".$mesAct." / ".$anioAct." )<br>";*/
      		if ($mesMat==12){
      			$mesM=1;
      			$anioMat=$anioMat+1;
      		}
     		$periodosDeActividad=0;
			$arrayPP = Array();
      		for ($anioMat; $anioMat < $anioA; $anioMat++) {
      			if ($anioMat==$anioAct) {
      				if ($aux==0){
      				 	for ($mesM; $mesM < $mesAct; $mesM++) {
      						/*cargar array mesMat;anioMat*/
      						/*echo "Año actual - ";
      						echo "Mes/Año( ".$mesM." / ".$anioMat." )<br>";*/

      						$arrayPP[] = array("mes"=>$mesM,"anio"=>$anioMat);
      						$periodosDeActividad++;
      					}
      				} else {
      					for ($j=1; $j < $mesAct; $j++) {
      						/*cargar array mesMat;anioMat*/
      						/*echo "Año actual - ";
      						echo "Mes/Año( ".$j." / ".$anioMat." )<br>";*/

      						$arrayPP[] = array("mes"=>$j,"anio"=>$anioMat);
      						$periodosDeActividad++;
      					}
      				}
	      		} else if ($aux==0) {
	      			for ($mesM; $mesM < 13; $mesM++) { 
      					$aux=1;
	      				/*cargar array mesMat;anioMat*/
      					/*echo "Año matriculación - ";
      					echo "Mes/Año( ".$mesM." / ".$anioMat." )<br>";*/

      					$arrayPP[] = array("mes"=>$mesM,"anio"=>$anioMat);
      					$periodosDeActividad++;
	      			}
	      		} else {
      				for ($i=1; $i < 13; $i++) { 
      					/*cargar array mesMat;anioMat*/
  						/*echo "Año intermedio - ";
  						echo "Mes/Año( ".$i." / ".$anioMat." )<br>";*/

  						$arrayPP[] = array("mes"=>$i,"anio"=>$anioMat);
  						$periodosDeActividad++;
      				}
	      		}
      		}
      		/*echo "Array Todos los Periodos<br>";
      		echo var_dump($arrayPP);
      		echo "<br>Total = ".$periodosDeActividad."<br>";

      		echo var_dump($pps)."<br><br>";*/

      		/*Eliminar los periodos de no activos (suspendido)*/
      		/*Buscar las suspenciones*/
      		//echo "<br>Periodos de Suspension<br>";
      		$suspensiones=DB::table('suspension as s')
	      				->select('s.*')
	      				->where('s.id_matricula','=',$mat->id)
	      				->where('s.estado','=','1')
						->orderBy('s.id','desc')
						->get();

			$arrayPSus= Array();
	     	$periodosDeSuspension=0;
			foreach ($suspensiones as $suspension) {
				$fecha2 = date($suspension->desde);
				$nuevafecha2 = strtotime ( '+0 day' , strtotime ( $fecha2 ) ) ;
				$mesMat= (int) date ( 'm' , $nuevafecha2 );
				$anioMat= (int) date ( 'Y' , $nuevafecha2 );
				$fecha = date($suspension->hasta);
				$nuevafecha = strtotime ( '+0 day' , strtotime ( $fecha ) ) ;
				$mesAct = (int) date ( 'm' , $nuevafecha );
				$anioAct= (int) date ( 'Y' , $nuevafecha );
				$anioA=$anioAct+1;$mesM=$mesMat;
				$aux=0;
				/*echo "id_matricula = ".$mat->id." // Período de Alta ( ".$mesMat." / ".$anioMat." ) // ";
	      		echo "Período Actual ( ".$mesAct." / ".$anioAct." )<br>";*/
	      		if ($mesMat==12){
	      			$mesM=1;
	      			$anioMat=$anioMat+1;
	      		}
				for ($anioMat; $anioMat < $anioA; $anioMat++) {
	      			if ($anioMat==$anioAct) {
	      				if ($aux==0){
	      				 	for ($mesM; $mesM <= $mesAct; $mesM++) {
	      						/*cargar array mesMat;anioMat*/
	      						/*echo "Año actual - ";
	      						echo "Mes/Año( ".$mesM." / ".$anioMat." )<br>";*/

	      						$arrayPSus[] = array("mes"=>$mesM,"anio"=>$anioMat);
	      						$periodosDeSuspension++;
	      					}
	      				} else {
	      					for ($j=1; $j <= $mesAct; $j++) {
	      						/*cargar array mesMat;anioMat*/
	      						/*echo "Año actual - ";
	      						echo "Mes/Año( ".$j." / ".$anioMat." )<br>";*/

	      						$arrayPSus[] = array("mes"=>$j,"anio"=>$anioMat);
	      						$periodosDeSuspension++;
	      					}
	      				}
		      		} else if ($aux==0) {
		      			for ($mesM; $mesM < 13; $mesM++) { 
	      					$aux=1;
		      				/*cargar array mesMat;anioMat*/
	      					/*echo "Año matriculación - ";
	      					echo "Mes/Año( ".$mesM." / ".$anioMat." )<br>";*/

	      					$arrayPSus[] = array("mes"=>$mesM,"anio"=>$anioMat);
	      					$periodosDeSuspension++;
		      			}
		      		} else {
	      				for ($i=1; $i < 13; $i++) { 
	      					/*cargar array mesMat;anioMat*/
      						/*echo "Año intermedio - ";
      						echo "Mes/Año( ".$i." / ".$anioMat." )<br>";*/

      						$arrayPSus[] = array("mes"=>$i,"anio"=>$anioMat);
      						$periodosDeSuspension++;
	      				}
		      		}
      			}
      			//echo "Fin ciclo Suspension<br><br>";
			}
			/*echo "Array Todos los Periodos Suspendidos<br>";
      		echo var_dump($arrayPSus);
      		echo "<br>Total meses suspendido= ".$periodosDeSuspension."<br><br>";*/

      		/*Comparar los dos array para determinar que periodos estaban activos y cuales suspendido*/
			$contPaPagar=0;
      		$arrayPaPagar = array();
			for ($k=0;$k<$periodosDeActividad;$k++) {
				$pSus=1;
				for ($s=0;$s<$periodosDeSuspension;$s++) {
					if ($arrayPP[$k]["mes"] == $arrayPSus[$s]["mes"] && $arrayPP[$k]["anio"] == $arrayPSus[$s]["anio"]) {
						$pSus=0;
						//echo "- Suspension mes ".$arrayPP[$k]["mes"]." año ".$arrayPP[$k]["anio"]."<br>";
					}
				}
				if ($pSus==1) {
					//echo "- A Pagar mes ".$arrayPP[$k]["mes"]." año ".$arrayPP[$k]["anio"]."<br>";
					$contPaPagar++;/*Contar los períodos de pago que debe*/
					array_push($arrayPaPagar, $arrayPP[$k]);
				}
			}

			//echo "Listado de estados de los periodos<br>";
      		/*Comparar los dos array para determinar que periodos estan pagos y cuales se deben*/
			$contPPdebe=0;
			for ($k=0;$k<$contPaPagar;$k++) {
				$debe=1;
				foreach ($pps as $pp) {
					if ($arrayPaPagar[$k]["mes"] == $pp->mes && $arrayPaPagar[$k]["anio"] == $pp->anio) {
						$debe=0;
						//echo "- PAGO mes ".$arrayPaPagar[$k]["mes"]." año ".$arrayPaPagar[$k]["anio"]."<br>";
					} 
				}
				if ($debe==1) {
					//echo "- DEBE mes ".$arrayPaPagar[$k]["mes"]." año ".$arrayPaPagar[$k]["anio"]."<br>";
					$contPPdebe++;/*Contar los períodos de pago que debe*/
					array_push($arrayPPMorosos, $arrayPaPagar[$k]);
				}
			}
		}
		return $arrayPPMorosos;
	}

	static function debe_inscripcion($query1)
	{
		return DB::table('matricula as m')
							->select('m.*')
							->whereNotExists( function ($query) {
	            				$query->select('pm.id')
	                  				->from('pago_matricula as pm')
	                  				->whereRaw('pm.id_matricula = m.id')
				                    ->where('pm.id_tipo_pago', '=', '1')
				                    ->where('pm.estado', '=', '1');})
							->where('m.id_estado','<>','4')/*Filtrar Inactivos*/
							->where('m.id','=',$query1)/*Busca un id matricula*/
							->orderBy('m.nombre','asc')
							->get();
	}

	static function deben_inscripcion()
	{
		return DB::table('matricula as m')
							->select('m.*')
							->whereNotExists( function ($query) {
	            				$query->select('pm.id')
	                  				->from('pago_matricula as pm')
	                  				->whereRaw('pm.id_matricula = m.id')
				                    ->where('pm.id_tipo_pago', '=', '1')
				                    ->where('pm.estado', '=', '1');})
							->where('m.id_estado','<>','4')/*Filtrar Inactivos*/
							->orderBy('m.nombre','asc')
							->get();
	}

	static function mat_debe_inscripcion($query1)
	{
		$mor=MorosoController::debe_inscripcion($query1);
		$debe=false;
		foreach ($mor as $m) {
			$debe=true;
		}
		return $debe;
	}

}
