<?php

namespace Colegio\Http\Controllers;

use Illuminate\Http\Request;

use Colegio\Http\Requests;
use Colegio\Matricula;
use Colegio\Pago;
use Colegio\Periodo_Pago;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Colegio\Http\Requests\PagoFormRequest;
use DB;

class PagoController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{		
		if ($request){
		    $query1=trim($request->get('searchText1'));
		    $query2=trim($request->get('searchText2'));
		    $query4=trim($request->get('searchText4'));
		    $query3=$request->get('searchText3');
		    $mat=DB::table('matricula as m')
		      				->select('m.id','m.nombre','m.fecha_alta','m.numero_matricula','m.link_foto','m.nacionalidad','m.dni','m.fecha_nacimiento','m.titulo')
							->where('m.id','=',$query1)
							->get();
		$matricula=new Matricula;
		foreach ($mat as $m) {
			$matricula=Matricula::findOrFail($m->id);
		}
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.dni," ",m.nombre) as matricula'),'m.id')
	      				->where('m.id_estado','<>','4')
	      				//->where('m.id_estado','<>','3')
	      				->orderBy('m.nombre','asc')
						->get();
		/*Busca todas los pagos de los matriculado según Matriculado, Tipo de Pago y/o Fecha de Pago*/
		$pagos=null;
		if($query1!=''&&$query2!=''&&$query3!=''){
			$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.estado','=','1')
							->where('p.id_matricula','=',$matricula->id)
							->where('p.id_tipo_pago','=',$query2)
							->where('p.descripcion','like','%'.$query4.'%')
							->whereDate('p.fecha_pago','=',$query3)
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		} else if($query1!=''&&$query2!=''){
			$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.id_matricula','=',$matricula->id)
							->where('p.id_tipo_pago','=',$query2)
							->where('p.descripcion','like','%'.$query4.'%')
							->where('p.estado','=','1')
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		} else if ($query1!=''&&$query3!='') {
			$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.id_matricula','=',$matricula->id)
							->whereDate('p.fecha_pago','=',$query3)
							->where('p.descripcion','like','%'.$query4.'%')
							->where('p.estado','=','1')
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		} else if ($query2!=''&&$query3!=''){
			$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.estado','=','1')
							->where('p.id_tipo_pago','=',$query2)
							->where('p.descripcion','like','%'.$query4.'%')
							->whereDate('p.fecha_pago','=',$query3)
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		} else if($query1!=''){
			$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							/*->leftjoin('periodo_pago as pp', function ($join) {
            						$join->on('pp.id_pago_matricula','=','p.id')
                 					->where('p.id_tipo_pago','=','2')
                 					->where('pp.estado','=','1');})*/
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.id_matricula','=',$matricula->id)
							->where('p.descripcion','like','%'.$query4.'%')
							->where('p.estado','=','1')
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		} else if ($query2!='') {
						$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.id_tipo_pago','=',$query2)
							->where('p.descripcion','like','%'.$query4.'%')
							->where('p.estado','=','1')
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		} else if ($query3!=''){
			$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.estado','=','1')
							->whereDate('p.fecha_pago','=',$query3)
							->where('p.descripcion','like','%'.$query4.'%')
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		} else if ($query4!=''){
			$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.estado','=','1')
							->where('p.descripcion','like','%'.$query4.'%')
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		} else {
			$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.estado','=','1')
							->orderBy('p.fecha_pago','desc')
							->paginate(10);
		}

		$tipo_pagos=DB::table('tipo_pago')->get();
		
		return view('colegio.pago.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"pagos"=>$pagos,"tipo_pagos"=>$tipo_pagos,"searchText1"=>$query1,"searchText2"=>$query2,"searchText3"=>$query3,"searchText4"=>$query4]);
		};
	}

	public function create()
	{
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.dni," ",m.nombre) as matricula'),'m.id')
	      				->where('m.id_estado','<>','4')
	      				->orderBy('m.nombre','asc')
						->get();
		$tipo_pagos=DB::table('tipo_pago')->get();

		return view('colegio.pago.create',["matriculas"=>$matriculas,"tipo_pagos"=>$tipo_pagos]);
	}

	public function store(PagoFormRequest $request)
	{
		/*Alta de nuevo pago*/
		$this->validate($request,['descripcion' => 'unique:pago_matricula',]);
		$pago= new Pago;
		$matricula=Matricula::findOrFail($request->get('id_matricula'));
		$pago->id_matricula=$request->get('id_matricula');
		$pago->id_tipo_pago=$request->get('id_tipo_pago');
		$pago->descripcion=$request->get('descripcion');
		$pago->monto=$request->get('monto');
		$pago->fecha_pago=$request->get('fecha_pago');

		if(Input::hasFile('link')){
			$file=Input::file('link');
			$file->move(public_path().'/imagenes/pago/',$pago->fecha_pago." ".$matricula->nombre."-".$file->getClientOriginalName());
			$pago->link=$pago->fecha_pago." ".$matricula->nombre."-".$file->getClientOriginalName();
		}
		$pago->estado='1';

		/*Verigicar si ya no esta/n paga/s la/s cuota/s (si es Pago Cuota)*/
		if ($request->get('id_tipo_pago')=='2') {
			$cont=0;
			$banAuxErr=true;
			$error="La/s siguiente/s cuota/s cargada/s ya se encuentra/n paga/s: ";
			
			if($this->esta_paga_cuota($matricula->id,$request->get('mes1'),$request->get('anio1'))){
				$cont++;
				$error.=$request->get('mes1')."/".$request->get('anio1')."; ";
			} else if ($request->get('mes1')<>'' && $request->get('anio1')<>'') {$banAuxErr=false;}
			if($this->esta_paga_cuota($matricula->id,$request->get('mes2'),$request->get('anio2'))){
				$cont++;
				$error.=$request->get('mes2')."/".$request->get('anio2')."; ";
			} else if ($request->get('mes2')<>'' && $request->get('anio2')<>'') {$banAuxErr=false;}
			if($this->esta_paga_cuota($matricula->id,$request->get('mes3'),$request->get('anio3'))){
				$cont++;
				$error.=$request->get('mes3')."/".$request->get('anio3')."; ";
			} else if ($request->get('mes3')<>'' && $request->get('anio3')<>'') {$banAuxErr=false;}
			if($this->esta_paga_cuota($matricula->id,$request->get('mes4'),$request->get('anio4'))){
				$cont++;
				$error.=$request->get('mes4')."/".$request->get('anio4')."; ";
			} else if ($request->get('mes4')<>'' && $request->get('anio4')<>'') {$banAuxErr=false;}
			if($this->esta_paga_cuota($matricula->id,$request->get('mes5'),$request->get('anio5'))){
				$cont++;
				$error.=$request->get('mes5')."/".$request->get('anio5')."; ";
			} else if ($request->get('mes5')<>'' && $request->get('anio5')<>'') {$banAuxErr=false;}
			if($this->esta_paga_cuota($matricula->id,$request->get('mes6'),$request->get('anio6'))){
				$cont++;
				$error.=$request->get('mes6')."/".$request->get('anio6')."; ";
			} else if ($request->get('mes6')<>'' && $request->get('anio6')<>'') {$banAuxErr=false;}
			if($cont>'0'){
				//Error Uno o mas Cuotas ya se encuentran pagas
				$messages = ['err.required' => $error,];
				$this->validate($request,['err'=> 'required',],$messages);
			} if ($banAuxErr) {
				//Error Tipo pagop Cuota y ninguna cuota cargada para pagar
				$mensError1="Selecciono Tipo de Pago Cuota Mensual y No cargo ninguna cuota para pagar";
				$messages = ['err.required' => $mensError1,];
				$this->validate($request,['err'=> 'required',],$messages);
			} else {
				//echo " Cantidad de cuotas cargadas: ".$bandera;
				$pago->save();/*Guardo la cabecera del pago*/
				$pagResul=DB::table('pago_matricula as p')
				      				->select('p.*')
									->where('p.id_tipo_pago','=',$pago->id_tipo_pago)
									->where('p.id_matricula','=',$pago->id_matricula)
									->where('p.monto','=',$pago->monto)
									->whereDate('p.fecha_pago','=',$pago->fecha_pago)
									->where('p.descripcion','=',$pago->descripcion)
									->where('p.estado','=','1')
									->get();
				$pag=new Pago;
				foreach ($pagResul as $p) {
					$pag=Pago::findOrFail($p->id);
				}
				/*Cargar las cuotas a pagar*/
				//echo "Pago cargado id: ".$pag->id;
				$bandera=0;
				if($this->cargar_periodo_pago($pag->id,$request->get('mes1'),$request->get('anio1'))){
					$bandera++;
				}
				if($this->cargar_periodo_pago($pag->id,$request->get('mes2'),$request->get('anio2'))){
					$bandera++;
				}
				if($this->cargar_periodo_pago($pag->id,$request->get('mes3'),$request->get('anio3'))){
					$bandera++;
				}
				if($this->cargar_periodo_pago($pag->id,$request->get('mes4'),$request->get('anio4'))){
					$bandera++;
				}
				if($this->cargar_periodo_pago($pag->id,$request->get('mes5'),$request->get('anio5'))){
					$bandera++;
				}
				if($this->cargar_periodo_pago($pag->id,$request->get('mes6'),$request->get('anio6'))){
					$bandera++;
				}
				if($bandera=='0'){
					/*Error al cargar las cuotas*/
					$mensError2="Error: No se pudo cargar la/s cuota/s";
					$messages = ['err.required' => $mensError2];
					$this->validate($request,['err'=> 'required',],$messages);
				}
			}
		} else {
			/*Existe la inscripción*/
			if ($this->esta_paga_inscripcion($matricula->id)) {
				//Error El matriculado ya pagó su inscripción
				$mensError3="El Profesional ya registra paga su inscripción";
				$messages = ['err.required' => $mensError3,];
				$this->validate($request,['err'=> 'required',],$messages);
			} else {
				$pago->save();/*Guardo el pago Único*/
			}
		}

		/*Vuelve a cargar las variables para actualizar el index*/
		/*Busca todos los matriculados*/
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.dni," ",m.nombre) as matricula'),'m.id')
	      				->where('m.id_estado','<>','4')
	      				->orderBy('m.nombre','asc')
						->get();
		$tipo_pagos=DB::table('tipo_pago')->get();
		$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.estado','=','1')
							->where('p.id_matricula','=',$matricula->id)
							->where('p.id_tipo_pago','=',$pago->id_tipo_pago)
							->where('p.descripcion','like','%'.$pago->descripcion.'%')
							->whereDate('p.fecha_pago','=',$pago->fecha_pago)
							->orderBy('p.fecha_pago','desc')
							->paginate(10);

		return redirect()->route('pago.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"pagos"=>$pagos,"tipo_pagos"=>$tipo_pagos,"searchText1"=>$matricula->id,"searchText2"=>$pago->id_tipo_pago,"searchText4"=>$pago->descripcion,"searchText3"=>$pago->fecha_pago])->with('info', 'Pago guardado con éxito!');
	}

	public function show($id)
	{
		
	}

	public function edit($id)
	{
		$pago=Pago::findOrFail($id);
		$PP=$this->periodos_pago($pago->id);
		$matricula=Matricula::findOrFail($pago->id_matricula);

		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.dni," ",m.nombre) as matricula'),'m.id')
	      				->where('m.id_estado','<>','4')
	      				->orderBy('m.nombre','asc')
						->get();
		$tipo_pagos=DB::table('tipo_pago')->get();
		$aPP = array();
		$cont=0;
		for ($i=0; $i < 6; $i++) {
			$aPP[$i] = array("mes"=>'',"anio"=>'');
		}
		foreach ($PP as $pPag) {
			$aPP[$cont] = array("mes"=>$pPag->mes,"anio"=>$pPag->anio);
			$cont++;
		}

		return view('colegio.pago.edit',["pago"=>$pago,"matricula"=>$matricula,"matriculas"=>$matriculas,"tipo_pagos"=>$tipo_pagos,"aPP"=>$aPP]);
	}

	public function update(PagoFormRequest $request,$id)
	{
		/*Editar pago*/
		$pago= Pago::findOrFail($id);
		$matricula=Matricula::findOrFail($request->get('id_matricula'));
		$pago->id_matricula=$request->get('id_matricula');
		$pago->id_tipo_pago=$request->get('id_tipo_pago');
		if ($pago->descripcion!=$request->get('descripcion')) {
			$this->validate($request,['descripcion' => 'unique:pago_matricula',]);
		}
		$pago->descripcion=$request->get('descripcion');
		$pago->monto=$request->get('monto');
		$pago->fecha_pago=$request->get('fecha_pago');

		if(Input::hasFile('link')){
			$file=Input::file('link');
			if(($Pago->link)!=""){
				File::delete(public_path('imagenes/pago/'.$pago->link));
			}
			$file->move(public_path().'/imagenes/pago/',$pago->fecha_pago." ".$matricula->nombre."-".$file->getClientOriginalName());
			$pago->link=$pago->fecha_pago." ".$matricula->nombre."-".$file->getClientOriginalName();
		}
		$pago->estado='1';

		/*Verigicar si ya no esta/n paga/s la/s cuota/s (si es Pago Cuota)*/
		if ($request->get('id_tipo_pago')=='2') {
			$cont=0;
			$banAuxErr=true;
			$error="La/s siguiente/s cuota/s cargada/s ya se encuentra/n paga/s: ";
			/*Ver si hay cambios en las cuotas*/
			if($this->no_cuota_en_pago($pago->id,$request->get('mes1'),$request->get('anio1')) && $this->esta_paga_cuota($matricula->id,$request->get('mes1'),$request->get('anio1'))){
				$cont++;
				$error.=$request->get('mes1')."/".$request->get('anio1')."; ";
			} else if ($request->get('mes1')!='' && $request->get('anio1')!='') {$banAuxErr=false;}
			if($this->no_cuota_en_pago($pago->id,$request->get('mes2'),$request->get('anio2')) &&$this->esta_paga_cuota($matricula->id,$request->get('mes2'),$request->get('anio2'))){
				$cont++;
				$error.=$request->get('mes2')."/".$request->get('anio2')."; ";
			} else if ($request->get('mes2')!='' && $request->get('anio2')!='') {$banAuxErr=false;}
			if($this->no_cuota_en_pago($pago->id,$request->get('mes3'),$request->get('anio3')) && $this->esta_paga_cuota($matricula->id,$request->get('mes3'),$request->get('anio3'))){
				$cont++;
				$error.=$request->get('mes3')."/".$request->get('anio3')."; ";
			} else if ($request->get('mes3')!='' && $request->get('anio3')!='') {$banAuxErr=false;}
			if($this->no_cuota_en_pago($pago->id,$request->get('mes4'),$request->get('anio4')) && $this->esta_paga_cuota($matricula->id,$request->get('mes4'),$request->get('anio4'))){
				$cont++;
				$error.=$request->get('mes4')."/".$request->get('anio4')."; ";
			} else if ($request->get('mes4')!='' && $request->get('anio4')!='') {$banAuxErr=false;}
			if($this->no_cuota_en_pago($pago->id,$request->get('mes5'),$request->get('anio5')) && $this->esta_paga_cuota($matricula->id,$request->get('mes5'),$request->get('anio5'))){
				$cont++;
				$error.=$request->get('mes5')."/".$request->get('anio5')."; ";
			} else if ($request->get('mes5')!='' && $request->get('anio5')!='') {$banAuxErr=false;}
			if($this->no_cuota_en_pago($pago->id,$request->get('mes6'),$request->get('anio6')) && $this->esta_paga_cuota($matricula->id,$request->get('mes6'),$request->get('anio6'))){
				$cont++;
				$error.=$request->get('mes6')."/".$request->get('anio6')."; ";
			} else if ($request->get('mes6')!='' && $request->get('anio6')!='') {$banAuxErr=false;}
			if($cont>'0'){
				//Error Uno o mas Cuotas ya se encuentran pagas
				$messages = ['err.required' => $error,];
				$this->validate($request,['err'=> 'required',],$messages);
			} if ($banAuxErr) {
				//Error Tipo pagop Cuota y ninguna cuota cargada para pagar
				$mensError1="Selecciono Tipo de Pago Cuota Mensual y No cargo ninguna cuota para pagar";
				$messages = ['err.required' => $mensError1,];
				$this->validate($request,['err'=> 'required',],$messages);
			} else {
				/*Cargar las cuotas a pagar*/
				$bandera=0;
				if($this->no_cuota_en_pago($pago->id,$request->get('mes1'),$request->get('anio1')))
					if($this->cargar_periodo_pago($pago->id,$request->get('mes1'),$request->get('anio1'))){
						$bandera++;
					}
				if($this->no_cuota_en_pago($pago->id,$request->get('mes2'),$request->get('anio2')))
					if($this->cargar_periodo_pago($pago->id,$request->get('mes2'),$request->get('anio2'))){
						$bandera++;
					}
				if($this->no_cuota_en_pago($pago->id,$request->get('mes3'),$request->get('anio3')))
					if($this->cargar_periodo_pago($pago->id,$request->get('mes3'),$request->get('anio3'))){
					$bandera++;
				}
				if($this->no_cuota_en_pago($pago->id,$request->get('mes4'),$request->get('anio4')))
					if($this->cargar_periodo_pago($pago->id,$request->get('mes4'),$request->get('anio4'))){
					$bandera++;
				}
				if($this->no_cuota_en_pago($pago->id,$request->get('mes5'),$request->get('anio5')))
					if($this->cargar_periodo_pago($pago->id,$request->get('mes5'),$request->get('anio5'))){
					$bandera++;
				}
				if($this->no_cuota_en_pago($pago->id,$request->get('mes6'),$request->get('anio6')))
					if($this->cargar_periodo_pago($pago->id,$request->get('mes6'),$request->get('anio6'))){
					$bandera++;
				}
				/*Si tenia una cuota paga y ahora la borraron*/
				$this->borrar_cuotas_desestimadas($pago->id,$request);
				/*Guarda y Edita el pago*/
				$pago->update();
			}
		} else {
				$pago->update();/*Guarda y Edita el pago Único (Inscripción)*/
				}

		/*Vuelve a cargar las variables para actualizar el index*/
		/*Busca todos los matriculados*/
		$matriculas=DB::table('matricula as m')
	      				->select(DB::raw('CONCAT(m.dni," ",m.nombre) as matricula'),'m.id')
	      				->where('m.id_estado','<>','4')
	      				->orderBy('m.nombre','asc')
						->get();
		$tipo_pagos=DB::table('tipo_pago')->get();
		$pagos=DB::table('pago_matricula as p')
							->join('tipo_pago as tp','p.id_tipo_pago','=','tp.id')
							->join('matricula as m','p.id_matricula','=','m.id')
							->select('p.*','tp.descripcion as tipo_pago','m.nombre as nombre')
							->where('p.estado','=','1')
							->where('p.id_matricula','=',$matricula->id)
							->where('p.id_tipo_pago','=',$pago->id_tipo_pago)
							->where('p.descripcion','like','%'.$pago->descripcion.'%')
							->whereDate('p.fecha_pago','=',$pago->fecha_pago)
							->orderBy('p.fecha_pago','desc')
							->paginate(10);

		return redirect()->route('pago.index',["matriculas"=>$matriculas,"matricula"=>$matricula,"pagos"=>$pagos,"tipo_pagos"=>$tipo_pagos,"searchText1"=>$matricula->id,"searchText2"=>$pago->id_tipo_pago,"searchText4"=>$pago->descripcion,"searchText3"=>$pago->fecha_pago])->with('info', 'Pago editado con éxito!');
	}

	public function destroy($id)
	{
		$pago=Pago::findOrFail($id);

		if ($pago->id_tipo_pago=='2'){
			$pps=$this->periodos_pago($pago->id);
			foreach ($pps as $pp) {
				$oPP=Periodo_Pago::findOrFail($pp->id);
				$oPP->estado='0';
				$oPP->update();
			}
		}

		$pago->estado='0';
		$pago->update();
		return redirect()->route('pago.index')->with('info', 'Pago eliminado con éxito!');
	}

	static function periodos_pago($id)
	{
		return DB::table('periodo_pago as pp')
		      				->select('pp.*')
		      				->where('pp.id_pago_matricula','=',$id)
		      				->where('pp.estado','=','1')
		      				->orderBy('pp.id','desc')
							->get();
	}
	static function esta_paga_cuota($id_matricula,$mes,$anio)
	{
		$result= DB::table('periodo_pago as pp')
							->join('pago_matricula as p','p.id','=','pp.id_pago_matricula')
		      				->select('pp.*')
		      				->where('p.id_matricula','=',$id_matricula)
		      				->where('pp.mes','=',$mes)
		      				->where('pp.anio','=',$anio)
		      				->where('pp.estado','=','1')
							->get();
		$bandera=false;
		foreach ($result as $pp) {
			$bandera=true;
		}
		return $bandera;
	}
	static function esta_paga_inscripcion($id_matricula)
	{
		$result= DB::table('pago_matricula as p')
		      				->select('p.*')
		      				->where('p.id_matricula','=',$id_matricula)
		      				->where('p.id_tipo_pago','=','1')
		      				->where('p.estado','=','1')
							->get();
		$bandera=false;
		foreach ($result as $p) {
			$bandera=true;
		}
		return $bandera;
	}
	static function cargar_periodo_pago($id,$mes,$anio){
		$bandera=false;
		if($mes!='' && $anio!=''){
			$periodo_pago=new Periodo_Pago;
			$periodo_pago->id_pago_matricula=$id;
			$periodo_pago->mes=$mes;
			$periodo_pago->anio=$anio;
			$periodo_pago->estado='1';
			$periodo_pago->save();
			$bandera=true;
		}
		return $bandera;
	}
	static function no_cuota_en_pago($id_pago,$mes,$anio)
	{
		$PCont= new PagoController;
		$result= $PCont->periodos_pago($id_pago);
		$bandera=true;
		foreach ($result as $pp) {
			if ($pp->mes==$mes && $pp->anio==$anio) {
				$bandera=false;
			}
		}
		return $bandera;
	}
	static function borrar_cuotas_desestimadas($id_pago,$request)
	{
		$PCont= new PagoController;
		$result= $PCont->periodos_pago($id_pago);
		$bandera=true;
		foreach ($result as $pp) {
			$bandera=true;
			if($pp->mes==$request->get('mes1')&&$pp->anio==$request->get('anio1')){$bandera=false;} else if($pp->mes==$request->get('mes2')&&$pp->anio==$request->get('anio2')){$bandera=false;} else if($pp->mes==$request->get('mes3')&&$pp->anio==$request->get('anio3')){$bandera=false;} else if($pp->mes==$request->get('mes4')&&$pp->anio==$request->get('anio4')){$bandera=false;} else if($pp->mes==$request->get('mes5')&&$pp->anio==$request->get('anio5')){$bandera=false;} else if($pp->mes==$request->get('mes6')&&$pp->anio==$request->get('anio6')){$bandera=false;}
			if($bandera){
				$oPP= Periodo_Pago::findOrFail($pp->id);
				$oPP->estado='0';
				$oPP->update();
			}
		}
	}
}
