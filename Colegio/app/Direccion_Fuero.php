<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Direccion_Fuero extends Model
{
    protected $table='direccion_fuero';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_direccion',
		'id_fuero',
		'estado'
	];

	protected $guarded = [	

	];
}
