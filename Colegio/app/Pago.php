<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $table='pago_matricula';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_matricula',
		'id_tipo_pago',
		'descripcion',
		'monto',
		'fecha_pago',
		'link',
		'estado'
	];

	protected $guarded = [	

	];
}
