<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Resolucion extends Model
{
    protected $table='resolucion';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_tipo_resolucion',
		'numero',
		'id_matricula',
		'fecha',
		'lugar',
		'link',
		'estado'
	];

	protected $guarded = [	

	];
}
