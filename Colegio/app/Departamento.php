<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table='departamento';

	protected $primaryKey='id';

	public $timestamps = false; 

		protected $fillable = [
		'nombre'
	];

	protected $guarded = [	

	];
}
