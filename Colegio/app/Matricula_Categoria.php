<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Matricula_Categoria extends Model
{
    protected $table='matricula_categoria';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_matricula',
		'id_categoria',
		'estado'
	];

	protected $guarded = [	

	];
}
