<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Direccion_Categoria extends Model
{
    protected $table='direccion_categoria';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_direccion',
		'id_categoria',
		'estado'
	];

	protected $guarded = [	

	];
}
