<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class TipoDireccion extends Model
{
    protected $table='tipo_direccion';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'nombre',
		'estado'
	];

	protected $guarded = [	

	];
}
