<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Capacitacion extends Model
{
    protected $table='capacitacion';

	protected $primaryKey='id';

	public $timestamps = false; 

		protected $fillable = [
		'id_matricula',
		'descripcion',
		'fechao',
		'link',
		'estado'
	];

	protected $guarded = [	

	];
}
