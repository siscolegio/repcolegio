<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Fuero extends Model
{
    protected $table='fuero';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'nombre',
		'estado'
	];

	protected $guarded = [	

	];

}
