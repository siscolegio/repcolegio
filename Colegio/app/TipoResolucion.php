<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class TipoResolucion extends Model
{
    protected $table='tipo_resolucion';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'nombre',
		'estado'
	];

	protected $guarded = [	

	];
}
