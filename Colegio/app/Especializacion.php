<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Especializacion extends Model
{
   protected $table='especializacion';

	protected $primaryKey='id';

	public $timestamps = false; 

		protected $fillable = [
		'nombre',
		'estado'
	];

	protected $guarded = [	

	];

}
