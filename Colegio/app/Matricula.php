<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    protected $table='matricula';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'nombre',
		'fecha_alta',
		'numero_matricula',
		'link_foto',
		'nacionalidad',
		'dni',
		'link_dni',
		'fecha_nacimiento',
		'titulo',
		'link_titulo',
		'link_buena_conducta',
		'id_estado'			
	];

	protected $guarded = [	

	];
}
