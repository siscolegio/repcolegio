<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Periodo_Pago extends Model
{
    protected $table='periodo_pago';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_pago_matricula',
		'mes',
		'anio',
		'estado'
	];

	protected $guarded = [	

	];
}
