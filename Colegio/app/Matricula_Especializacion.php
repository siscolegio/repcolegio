<?php

namespace Colegio;

use Illuminate\Database\Eloquent\Model;

class Matricula_Especializacion extends Model
{
    protected $table='matricula_especializacion';

	protected $primaryKey='id';
	
	public $timestamps = false;
	
	protected $fillable = [
		'id_matricula',
		'id_especializacion',
		'estado'
	];

	protected $guarded = [	

	];
}
