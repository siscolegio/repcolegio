<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});
/*Ruta de prueba*/
Route::get('admin', function(){
	return view('admin.dashboard');
});

Route::resource('colegio/matricula','MatriculaController');

Route::resource('colegio/suspension','SuspensionController');
Route::resource('colegio/extactsuspension','ExtActSuspensionController');
Route::resource('colegio/direccion','DireccionController');
Route::resource('colegio/capacitacion','CapacitacionController');
Route::resource('colegio/pago','PagoController');
Route::resource('colegio/moroso','MorosoController');
Route::resource('colegio/excel','ExcelController');
Route::resource('colegio/baja','BajaController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('colegio/nuevo','NuevoController');

Route::resource('colegio/cathabilitante','CatHabilitanteController');

//Route::get('foo', 'Photos\SuspensionController@buscarMatriculados');

Route::middleware(['auth'])->group(function()	{
		
		//Categoria
			Route::resource('categoria', 'CategoriaController');

		//Especializacion
			Route::resource('especializacion','EspecializacionController');

		//Fuero
			Route::resource('fuero','FueroController');

		//Tipo Direccion
			Route::resource('tipo_direccion','TipoDireccionController');

		//Tipo Resolucion
			Route::resource('tipo_resolucion','TipoResolucionController');
			
		//Usuarios
			Route::resource('users', 'UserController');
/*		Route::post('users/store', 'UserController@store')->name('users.store')
			->middleware('permission:users.create');
		
		Route::get('users/', 'UserController@index')->name('users.index')
			->middleware('permission:users.index');
		
		Route::get('users/create', 'UserController@create')->name('users.create')
			->middleware('permission:users.create');
		
		Route::put('users/{user}', 'UserController@update')->name('users.update')
			->middleware('permission:users.edit');
		
		Route::get('users/{user}', 'UserController@show')->name('users.show')
			->middleware('permission:users.show');
		
		Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy')
			->middleware('permission:users.destroy');
		
		Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')
			->middleware('permission:users.edit');*/

	});

			
	