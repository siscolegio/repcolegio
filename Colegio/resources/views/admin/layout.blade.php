<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>COPCACER</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <link rel="stylesheet" href="{{asset('/css/bootstrap-select.min.css')}}">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/adminlte/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="/adminlte/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="{{URL::action('HomeController@index')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <img class="logo-mini" align="centrado" src="/adminlte/img/logo-cac-50x50.png" width="50" height="50">
      <!-- <span class="logo-mini"><b>A</b>LT</span> -->
      <!-- logo for regular state and mobile devices -->
      <img src="/adminlte/img/logo-copcacer-s-fondo.png" width="110" height="45">
      <!-- <span class="logo-lg"><b>COPCACER</b></span> -->
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <!--MENU DE MENSAJE
            !<-- Menu toggle button ->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">1</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Tu tienes 1 mensaje</li>
              <li>
                !<-- inner menu: contains the messages ->
                <ul class="menu">
                  <li>!<-- start message ->
                    <a href="#">
                      <div class="pull-left">
                        !<-- User Image ->
                        <img src="/adminlte/img/user0-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      !<-- Message title and timestamp ->
                      <h4>
                        Rodrigo
                        <small><i class="fa fa-clock-o"></i> 2 mins</small>
                      </h4>
                      !<-- The message ->
                      <p>Ponte a programar!!!</p>
                    </a>
                  </li>
                  !<-- end message ->
                </ul>
                !<-- /.menu ->
              </li>
              <li class="footer"><a href="#">Ver todo el mensaje</a></li>
            </ul>
          </li> -->
          <!-- /.messages-menu 
          HASTA ACA MENU DE MENSAJE
          -->

          <!-- Notifications Menu ->
          <li class="dropdown notifications-menu">
            !<-- Menu toggle button ->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">2</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 2 notifications</li>
              <li>
                !<-- Inner Menu: contains the notifications ->
                <ul class="menu">
                  <li>!<-- start notification ->
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 2 new members joined today
                    </a>
                  </li>
                  !<-- end notification ->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          HASTA ACA MENU DE NOTIFICACION -->


          <!-- Tasks Menu ->
          <li class="dropdown tasks-menu">
            !<-- Menu Toggle Button ->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">3</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 3 tasks</li>
              <li>
                !<-- Inner menu: contains the tasks ->
                <ul class="menu">
                  <li>!<-- Task item ->
                    <a href="#">
                      <-- Task title and progress text ->
                      <h3>
                        Avance del sistema...
                        <small class="pull-right">10%</small>
                      </h3>
                      !<-- The progress bar ->
                      <div class="progress xs">
                       !<-- Change the css width attribute to simulate progress ->
                        <div class="progress-bar progress-bar-aqua" style="width: 10%" role="progressbar"
                             aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">10% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  !<-- end task item ->
                </ul>
                </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          HASTA ACA MENU DE TAREAS -->

          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <!-- <img src="/adminlte/img/user0-160x160.jpg" class="user-image" alt="User Image">
              -->
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="/adminlte/img/usuario.png" class="img-circle" alt="Imagen Perfil">
                <p>
                  <!-- <small>Usuario: </small> -->
                  {{ Auth::user()->email }}
                </p>
              </li>
              <!-- Menu Body -->
              <!--
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
              -->
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <!-- <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                  </div>
                <div class="pull-right">
                -->
                <div class="text-center">
                  <!-- desde aca el por defecto de la plantilla -->
                  <!-- <a href="" class="btn btn-default btn-flat">Cambiar Contraseña</a>-->
                  <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Cerrar sesión
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                  </form>
                  
                  <!-- HASTA ACA - ESTO ERA EL POR DEFECTO
                    <a href="#" class="btn btn-default btn-flat">Cerrar sesión</a> -->
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel">
        <div class="pull-left image">
          <img src="/adminlte/img/user0-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Inicio</p>
          Status Y USER          
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        
        </div>
      </div>-->

      <!-- search form (Optional) -->
      <!-- 
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      -->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><b>MENU</b></li>
        <!-- Optionally, you can add icons to the links -->
        <!-- <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li> -->
        <li><a href="{{URL::action('HomeController@index')}}"><img src="/adminlte/img/home.png" height="20" width="20" class="img-circle"> <span>Inicio</span></a></li>
        
        <li class="treeview">
          <a href="#"><img src="/adminlte/img/verde.png" height="20" width="20" class="img-circle">&nbsp;<span>Matriculados</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{URL::action('MatriculaController@index')}}"><span>Consultar</span></a></li>
            <li><a href="{{URL::action('NuevoController@index')}}"><span>Pendientes de Matricular</span></a></li>
            <li><a href="{{URL::action('CapacitacionController@index')}}"><span>Capacitaciones</span></a></li> 
            <li><a href="{{URL::action('CatHabilitanteController@index')}}"><span>Especialización-Categorías</span></a></li>
            <li><a href="{{URL::action('DireccionController@index')}}"><span>Direcciones</span></a></li>

          </ul>
        </li>

        <li class="treeview">
          <a href="#"><img src="/adminlte/img/lila.png" height="20" width="20" class="img-circle">&nbsp;<span>Suspensiones</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">  
            <li class="nav-item"><a class="nav-link" href="{{URL::action('SuspensionController@index')}}">Consultar</a></li>
            
            <li class="nav-item"><a class="nav-link" href="{{URL::action('ExtActSuspensionController@index')}}">Suspensiones Vencidas</a></li>

          </ul>
        </li>

        <li class="treeview">
          <a href="#"><img src="/adminlte/img/rojo.png" height="20" width="20" class="img-circle">&nbsp;<span>Pagos</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{URL::action('PagoController@index')}}"><i class="fa fa-link"></i> <span>Consultar</span></a></li>
              <li><a href="{{URL::action('MorosoController@index')}}"><i class="fa fa-link"></i> <span>Morosos</span></a></li>
          </ul>
        </li>
      
        <li class="treeview">
          <a href="#"><img src="/adminlte/img/amarillo.png" height="20" width="20" class="img-circle">&nbsp;<span>Otras Configuraciones</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
           
            <!-- <li><a href="{{URL::action('CategoriaController@index')}}">Categorías</a></li> -->
        
            @can('colegio.categoria.index')    
            <li class="nav-item">
              <a class="nav-link" href="{{ route ('categoria.index') }}">Categorías</a>
            </li>
            @endcan
            
            @can('colegio.especializacion.index')    
            <li class="nav-item">
              <a class="nav-link" href="{{ route ('especializacion.index') }}">Especializaciones</a>
            </li>
            @endcan

            @can('colegio.fuero.index') 
            <li class="nav-item">
              <a class="nav-link" href="{{ route ('fuero.index') }}">Fueros</a>
            </li>
            @endcan
            
            @can('colegio.tipo_direccion.index') 
              <li class="nav-item">
                <a class="nav-item" href="{{ route ('tipo_direccion.index') }}">Tipos de Direcciones</a>
              </li>
            @endcan

            @can('colegio.tipo_direccion.index') 
              <li class="nav-item">
                <a class="nav-item" href="{{ route ('tipo_resolucion.index') }}">Tipos de Resoluciones</a>
              </li>
            @endcan

            <li><a href="{{URL::action('BajaController@index')}}">Volver a Matricular</a></li>
          </ul>
        </li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
        -->        
        @can('users.index')
        <li><a href="{{ route ('users.index') }}"><i class="fa fa-link"></i> <span>Usuarios</span></a></li>
        @endcan
      </ul>
      <!-- /.sidebar-menu -->

    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <!--
       <h1>
        Pagina de Inicio
        <small>Optional description</small> -->
      </h1>
  <!--SECCION de CAMINO DE HORMIGA
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Level</a></li>
        <li class="active">Here</li>
      </ol>
  -->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    <!-- agregre para mostrar variable info como mensaje -->
        @if(session('info'))
        <div class="container">
            <div class="row">
              <!-- col-md-offset-0 alin izquierda, 1 centrado, 2 alin derecha  -->
                <div class="col-md-12 col-md-offset-0">
                    <div class="alert alert-success">
                        {{  session('info')  }}
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if(session('danger'))
        <div class="container">
            <div class="row">
              <!-- col-md-offset-0 alin izquierda, 1 centrado, 2 alin derecha  -->
                <div class="col-md-12 col-md-offset-0">
                    <div class="alert alert-danger">
                        {{  session('danger')  }}
                    </div>
                </div>
            </div>
        </div>
        @endif

		@yield('contenido')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <!-- Anything you want -->
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">B&T Soluciones</a>.</strong> Todos los derechos reservados.
  </footer>

  <!-- Control Sidebar -->
  <!-- ESTO ES LA BARRA LATERAL DE HERRAMIENTAS
  <aside class="control-sidebar control-sidebar-dark">
     Create the tabs 
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
     Tab panes 
    <div class="tab-content">
      Home tab content
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
         /.control-sidebar-menu

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        /.control-sidebar-menu 

      </div>
       /.tab-pane 
       Stats tab content
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
       /.tab-pane
      Settings tab content 
       <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
           /.form-group 
        </form>
      </div>
       /.tab-pane 
    </div>
  </aside> -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="{{asset('/js/bootstrap-select.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="/adminlte/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>