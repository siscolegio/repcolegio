@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Capacitaciones de los Matriculados 
			<a href="{{asset('colegio/capacitacion/create')}}">
				<button class="btn btn-success pull-right">Nueva Capacitacion</button>
			</a>
		</h3>
	</div>
</div>

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
			@include('colegio.capacitacion.search')	
	</div>
</div>
@if($searchText!='')
	<div class="panel panel-primary" title="Datos del Matriculado">
		<div class="panel-body">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="form-group" align="center">
					<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_foto)}}" alt="{{$matricula->link_foto}}" height="150px" width="150px" class="img-thumbnail">
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre"><h4><strong>{{$matricula->nombre}}</strong></h4></label><br>
					<label for="numero_matricula"><h4><strong>Matrícula: {{$matricula->numero_matricula}}</strong></h4></label><br>
					<label for="dni">DNI: {{$matricula->dni}}</label><br>
					<label for="titulo">Título: {{$matricula->titulo}}</label>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-primary" title="Capacitaciones">
		<div class="panel-body">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
					<table class="table table-striped table-borderd table-condensed table-hover">
						<thead>
							<th>Id</th>
							<th>Descripción</th>
							<th>Fecha</th>
							<th>Link</th>
							<th>Opciones</th>
						</thead>
						@foreach ($capacitaciones as $cap)
						<tr>
							<td>{{$cap->id}}</td>
							<td>{{$cap->descripcion}}</td>
							<?php
								$dt = new DateTime($cap->fecha);
								$fecha=$dt->format('d/m/Y');
							?>
							<td>{{$fecha}}</td>
							<td><a href="{{asset('imagenes/capacitacion/'.$cap->link)}}">{{$cap->link}}</a></td>
							<td>
								<a href="{{URL::action('CapacitacionController@edit',$cap->id)}}">
									<button class="btn btn-sm btn-info">Editar</button>
								</a>
								<a href="" data-target="#modal-delete-{{$cap->id}}" data-toggle="modal"><button class="btn btn-sm btn-danger">Eliminar</button></a>
							</td>
						</tr>
						@include('colegio.capacitacion.modal')
						@endforeach
					</table>
				</div>
				<div align="center">{{$capacitaciones->render()}}</div>
			</div>
		</div>
	</div>
@else
	<h4>Seleccione un Matriculado...</h4>
@endif

@endsection