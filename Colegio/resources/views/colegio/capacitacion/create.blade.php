@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Cargar Nueva Capacitación</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
	</div>
</div>

	{!!Form::open(array('url'=>'colegio/capacitacion','method'=>'POST','autocomplete'=>'off','files'=>'true'))!!}
	{{Form::token()}}

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
					<label for="nombre">Matriculado(*)</label>
					<select name="id_matricula" id="id_matricula" required="true" class="form-control selectpicker" data-live-search="true">	
						@foreach($matriculas as $mat)
							<option value="{{$mat->id}}">{{$mat->matricula}}</option>
						@endforeach
					</select>
			</div>
		</div>
	</div>
</div>

<h3>Capacitación</h3>
<div class="panel panel-primary" title="Capacitación del Profesional">
	<div class="panel-body">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="descripcion">Descripción(*)</label>
					<input type="text" name="descripcion" required value="{{old('descripcion')}}" class="form-control" placeholder="Descripción...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link">Comprobante Escaneado</label>
						<input type="file" name="link" class="form-control">
					</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<div class="form-group">
						<label for="fecha">Fecha(*)</label>
						<input type="date" name="fecha" required value="{{old('fecha')}}" class="form-control">
					</div>
				</div>
			</div>
	</div>
</div>

<div class="panel panel-primary" title="Acciones">
	<div class="panel-body">
			<div class="form-group" align="center">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
	</div>
</div>
			{!!Form::close()!!}
@endsection