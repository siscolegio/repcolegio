@extends('admin.layout')

@section('contenido')
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>Matriculados Morosos &#09;<a href="{{asset('colegio/pago/create')}}"><button class="btn btn-success">Nuevo pago</button></a></h3>
	</div>
</div>

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
		@include('colegio.moroso.search')
	</div>
</div>
@if($searchText2!='')
	<div class="panel panel-primary" title="Pagos">
		<div class="panel-body">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
					<table class="table table-striped table-borderd table-condensed table-hover">
						<thead>
							<th>Id</th>
							<th>Matrícula</th>
							<th>Fecha Matriculación</th>
							<th>DNI</th>
							<th>Apellido y Nombre</th>
							<th>Fecha Alta</th>
							<th>Opciones</th>
						</thead>
						@foreach ($morosos as $mor)
						<tr>
							<td>{{$mor->id}}</td>
							<td>{{$mor->numero_matricula}}</td>
							<?php
								if ($mor->fecha_alta!='') {
									$dt = new DateTime($mor->fecha_alta);
									$fechaAlt=$dt->format('d/m/Y');
								} else {
									$fechaAlt='';
								}
							?>
							<td>{{$fechaAlt}}</td>
							<td>{{$mor->dni}}</td>
							<td>{{$mor->nombre}}</td>
							<?php
								$dt = new DateTime($mor->fecha_creacion);
								$fechaCre=$dt->format('d/m/Y');
							?>
							<td>{{$fechaCre}}</td>
							<td>
								@if($searchText2==2)
									<a href="" data-target="#modal-ver-info-PP-{{$mor->id}}" data-toggle="modal"><button class="btn btn-info">Ver</button></a>
								@else
									Inscripción Pendiente
								@endif

							</td>
						</tr>
						@include('colegio.moroso.modalPPDebe')
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
@else
	<h4>Sin resultados...</h4>
@endif

@endsection