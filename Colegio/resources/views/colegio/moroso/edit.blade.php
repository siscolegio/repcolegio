@extends('admin.layout')

@section('contenido')

<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Edición de la Dirección del Matriculado</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
</div>

	{!!Form::model($capacitacion,['method'=>'PATCH','route'=>['capacitacion.update',$capacitacion->id],'files'=>'true'])!!}
	{{Form::token()}}

	<div class="panel panel-primary" title="Profesional">
		<div class="panel-body">				
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="form-group" align="center">
					<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_foto)}}" alt="{{$matricula->link_foto}}" height="150px" width="150px" class="img-thumbnail">
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre"><h4><strong>{{$matricula->nombre}}</strong></h4></label><br>
					<label for="numero_matricula"><h4><strong>Matrícula: {{$matricula->numero_matricula}}</strong></h4></label><br>
					<label for="dni">DNI: {{$matricula->dni}}</label><br>
					<label for="titulo">Título: {{$matricula->titulo}}</label>
				</div>
			</div>
		</div>
	</div>

<h3>Capacitación</h3>
<div class="panel panel-primary" title="Capacitación del Profesional">
	<div class="panel-body">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="descripcion">Descripción(*)</label>
					<input type="text" name="descripcion" required value="{{$capacitacion->descripcion}}" class="form-control" placeholder="Descripción...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link">Comprobante Escaneado</label>
						<input type="file" name="link" class="form-control">
						@if (($capacitacion->link)!="")
							<img src="{{asset('imagenes/capacitacion/'.$capacitacion->link)}}" alt="{{$capacitacion->link}}" height="100px" width="100px" class="img-thumbnail">
						@endif
					</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<div class="form-group">
						<label for="fecha">Fecha(*)</label>
						<input type="date" name="fecha" required value="{{$capacitacion->fecha}}" class="form-control">
					</div>
				</div>
			</div>
	</div>
</div>
	
	<div class="panel panel-primary">
		<div class="panel-body">
			<div class="form-group" align="center">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
		</div>
	</div>

	{!!Form::close()!!}

@endsection