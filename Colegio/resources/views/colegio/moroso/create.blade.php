@extends('admin.layout')

@section('contenido')


	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Cargar Nuevo Pago</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>

	{!!Form::open(array('url'=>'colegio/pago','method'=>'POST','autocomplete'=>'off','files'=>'true'))!!}
	{{Form::token()}}

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<div class="form-group">
					<label for="nombre">Matriculado(*)</label>
					<select name="id_matricula" id="id_matricula" required="true" class="form-control selectpicker" data-live-search="true">	
						<option value="">Seleccione un matriculado</option>
						@foreach($matriculas as $mat)
							<option value="{{$mat->id}}">{{$mat->matricula}}</option>
						@endforeach
					</select>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-primary" title="Pago del Profesional">
	<div class="panel-body">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label>Tipo de Pago(*)</label>
					<select name="id_tipo_pago" id="id_tipo_pago" class="form-control selectpicker">
						<option value="" selected>Seleccione un tipo de pago</option>
						@foreach($tipo_pagos as $tp)
							<option value="{{$tp->id}}">{{$tp->descripcion}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label>Descripción Factura(*)</label>
					<input type="text" name="descripcion" required value="{{old('descripcion')}}" class="form-control" placeholder="Descripción...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label>Monto Factura(*)</label>
					<input type="text" name="monto" required value="{{old('monto')}}" class="form-control" placeholder="$000,00">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Comprobante Escaneado</label>
						<input type="file" name="link" class="form-control">
					</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<div class="form-group">
						<label>Fecha Factura(*)</label>
						<input type="date" name="fecha" required value="{{old('fecha')}}" class="form-control">
					</div>
				</div>
			</div>
	</div>
</div>

<h3>Pago Cuota</h3>
<div class="panel panel-primary" title="Pago del Profesional">
	<div class="panel-body">

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				/*crear includ que cree chekbox del ultimo año (o todos) los meses que debe (o algo asi, dudo mucho, pensar mejor)*/
			</div>

	</div>
</div>
		<div class="panel panel-primary" title="Acciones">
			<div class="panel-body">
					<div class="form-group" align="center">
						<button class="btn btn-primary" type="submit">Guardar</button>
						<button class="btn btn-danger" type="reset">Cancelar</button>
					</div>
			</div>
		</div>

{!!Form::close()!!}
@endsection