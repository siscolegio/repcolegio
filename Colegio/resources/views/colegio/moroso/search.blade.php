{!!Form::open(array('url'=>'colegio/moroso','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}

		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<label for="matricula">Matriculado</label>
			<select name="searchText1" id="searchText1" class="form-control selectpicker" data-live-search="true">	
				<option value="" selected>Seleccione un matriculado</option>
				@foreach($matriculas as $mat)
						@if($mat->id == $matricula->id)
							<option value="{{$mat->id}}" selected>{{$mat->matricula}}</option>
						@else
							<option value="{{$mat->id}}">{{$mat->matricula}}</option>
						@endif
				@endforeach
			</select>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<label for="tipo_pago">Tipo de Pago</label>
			<select name="searchText2" id="searchText2" class="form-control selectpicker" required="true">
					<option value="" selected>Seleccione un tipo de pago</option>
				@foreach($tipo_pagos as $tp)
						@if($tp->id == $searchText2)
								<option value="{{$tp->id}}" selected>{{$tp->descripcion}}</option>
							@else
								<option value="{{$tp->id}}">{{$tp->descripcion}}</option>
							@endif
				@endforeach
			</select>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<span>
				<button type="submit" class="btn btn-primary">Buscar</button>
			</span>
		</div>

{{Form::close()}}