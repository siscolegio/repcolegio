<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-ver-info-PP-{{$mor->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-lebel="Close">
					<span aria-hidden="true">x</span>
				</button>
				<h4 class="modal-title">Detalle de las cuotas adeudadas</h4>
			</div>
			<div class="modal-body">
				<div class="panel panel-primary" title="Pagos">
					<div class="panel-body">
						<p>Matriculado: {{$mor->nombre}}</p>
						<?php
							use Colegio\Http\Controllers\MorosoController;
							$ppdebe=MorosoController::periodos_adeudados($mor);
							for ($i=0; $i < count($ppdebe); $i++) { 
								echo " - Cuota: ".$ppdebe[$i]["mes"]."/".$ppdebe[$i]["anio"]."<br>";
							}
						?>
					</dir>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>