@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Profesionales Pendientes de Matriculación
			<a href="{{asset('colegio/matricula')}}">
				<button class="btn btn-success pull-right">Ver Todos los Prefesionales</button>
			</a>
		</h3>
		@include('colegio.nuevo.search')
	</div>
</div>

<div class="panel panel-primary" title="Pendientes de Matriculación">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Foto</th>
						<th>DNI</th>
						<th>Nacionalidad</th>
						<th>Fecha nacimiento</th>
						<th>Título</th>
						<th>Pago Incripción</th>
						<th>Opciones</th>
					</thead>
					@foreach ($matriculas as $mat)
					<tr>
						<td>{{$mat->id}}</td>
						<td>{{$mat->nombre}}</td>
						
						<td>
							<img src="{{asset('imagenes/matricula/foto/'.$mat->link_foto)}}" alt="{{$mat->link_foto}}" height="80px" width="80px" class="img-thumbnail">
						</td>
						
						<td>{{$mat->dni}}</td>
						<td>{{$mat->nacionalidad}}</td>
						<?php
							$dtn = new DateTime($mat->fecha_nacimiento);
							$fechaNac=$dtn->format('d/m/Y');
						?>
						<td>{{$fechaNac}}</td>
						<td>{{$mat->titulo}}</td>
						<td>@include('colegio.nuevo.debeInscripcion')</td>
						<td>
							<a href="{{URL::action('NuevoController@edit',$mat->id)}}">
								<button class="btn btn-sm btn-info">Matricular</button>
							</a>
							<a href="" data-target="#modal-delete-{{$mat->id}}" data-toggle="modal">	<button class="btn btn-sm btn-danger">Eliminar</button>
							</a>
						</td>
					</tr>
					@include('colegio.nuevo.modal')
					@endforeach
				</table>
			</div>
			<div align="center">{{$matriculas->render()}}</div>
			<div align="right">
				<a href="{{URL::action('ExcelController@create')}}"><button class="btn btn-success">Exportar a Excel</button></a>
			</div>
		</div>
	</div>
</div>

@endsection