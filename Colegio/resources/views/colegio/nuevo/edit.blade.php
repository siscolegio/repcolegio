@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h3>Matricular</h3>
				@if (count($errors)>0)
				<div class="alert alert-danger">
					<ul>
					@foreach ($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
					</ul>
				</div>
				@endif
	</div>
</div>

	{!!Form::model($matricula,['method'=>'PATCH','route'=>['nuevo.update',$matricula->id],'files'=>'true'])!!}
	{{Form::token()}}

			<div class="panel panel-primary" title="Profesional">
				<div class="panel-body">
					
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="form-group" align="center">
							<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_foto)}}" alt="{{$matricula->link_foto}}" height="150px" width="150px" class="img-thumbnail">
						</div>
					</div>
					
					<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="nombre"><h3><strong>{{$matricula->nombre}}</strong></h3></label><br>
							<label for="dni">DNI: {{$matricula->dni}}</label><br>
							<label for="titulo">Título: {{$matricula->titulo}}</label>
						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="numero_matricula">Número de Matrícula(*)</label><br>
							<input type="text" name="numero_matricula" required value="{{old('numero_matricula')}}" class="form-control" placeholder="Número de Matrícula...">
						</div>
					</div>

				</div>
			</div>

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Cargar Resolución</h3>
	</div>
</div>
			<div class="panel panel-primary" title="Resolución">
				<div class="panel-body" >
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="numero">Número(*)</label>
							<input type="text" name="numero" required value="{{old('numero')}}" class="form-control" placeholder="Formato: 001/2019 Tº1 Fº1">
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="fecha">Fecha Resolución(*)</label>
							<input type="date" name="fecha" required value="{{old('fecha')}}" class="form-control">
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="lugar">Lugar de Emisión(*)</label>
							<input type="text" name="lugar" required value="{{old('lugar')}}" class="form-control" placeholder="Lugar...">
						</div>
					</div>
					<div class="col-lg-5 col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="link">Resolución Escaneada</label>
							<input type="file" name="link" class="form-control">
						</div>
					</div>
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="form-group" align="center">
						<button class="btn btn-primary" type="submit">Guardar</button>
						<button class="btn btn-danger" type="reset">Cancelar</button>
					</div>
				</div>
			</div>

	{!!Form::close()!!}

@endsection