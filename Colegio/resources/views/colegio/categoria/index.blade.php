@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Listado de Categorías
			@can ('categoria.create')
			<a href="{{	route ('categoria.create')	}}">
				<button class="btn btn-success pull-right">Nuevo</button>
			</a>
			@endcan
		</h3>			
	@include('colegio.categoria.search')
	</div>
</div>

<div class="panel panel-primary" title="Categorías">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Opciones</th>
					</thead>
					@foreach ($categorias as $cat)
					<tr>
						<td>{{$cat->id}}</td>
						<td>{{$cat->nombre}}</td>
						<td>
						@can ('categoria.edit')
						 <a href="{{	route ('categoria.edit', $cat->id)}}"> 
							<button class="btn btn-sm btn-info">Editar</button>
							</a>
						@endcan
						@can ('categoria.destroy')
							<a href="" data-target="#modal-delete-{{$cat->id}}" data-toggle="modal">
								<button class="btn btn-sm btn-danger">Eliminar</button>
							</a>
						@endcan
						</td>
					</tr>
					@include('colegio.categoria.modal')
					@endforeach
				</table>
			</div>
			<div align="center">{{$categorias->render()}}</div>
		</div>
	</div>
</div>

@endsection