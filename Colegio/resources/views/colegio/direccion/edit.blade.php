@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h3>Edición de la Dirección del Matriculado</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
</div>

			{!!Form::model($direccion,['method'=>'PATCH','route'=>['direccion.update',$direccion->id],'files'=>'true'])!!}
			{{Form::token()}}

	<div class="panel panel-primary" title="Profesional">
		<div class="panel-body">				
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="form-group" align="center">
					<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_foto)}}" alt="{{$matricula->link_foto}}" height="150px" width="150px" class="img-thumbnail">
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre"><h4><strong>{{$matricula->nombre}}</strong></h4></label><br>
					<label for="numero_matricula"><h4><strong>Matrícula: {{$matricula->numero_matricula}}</strong></h4></label><br>
					<label for="dni">DNI: {{$matricula->dni}}</label><br>
					<label for="titulo">Título: {{$matricula->titulo}}</label>
				</div>
			</div>
		</div>
	</div>

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Dirección del Matriculado</h3>
	</div>
</div>

	<div class="panel panel-primary" title="Dirección del Profesional">
		<div class="panel-body">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="localidad">Departamento y Localidad(*)</label>
						<select name="id_localidad" id="id_localidad" required="true" class="form-control selectpicker" data-live-search="true">
							@foreach($localidades as $loc)
							@if($loc->id == $direccion->id_localidad)
									<option value="{{$loc->id}}" selected>{{$loc->localidad}}</option>
								@else
									<option value="{{$loc->id}}">{{$loc->localidad}}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="calle">Calle(*)</label>
						<input type="text" name="calle" required value="{{$direccion->calle}}" class="form-control" placeholder="Calle...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="numero">Número(*)</label>
						<input type="text" name="numero" required value="{{$direccion->numero}}" class="form-control" placeholder="Número...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="telefono">Teléfono(*)</label>
						<input type="text" name="telefono" required value="{{$direccion->telefono}}" class="form-control" placeholder="Teléfono...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="correo">Correo(*)</label>
						<input type="text" name="correo" required value="{{$direccion->correo}}" class="form-control" placeholder="Correo...">
					</div>
				</div>
		</div>
	</div>

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Categorías y Fueros</h3>
	</div>
</div>

	<div class="panel panel-primary" title="Categorias Habilitantes y Fueros">
		<div class="panel-body">					
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
		            <h2>Categorías</h2>
		            @foreach($categorias as $cat)
		            <div class="checkbox">
		                <label>
		                    <?php
		                		$bandera=false;
		                		foreach($catHabilitante as $cH)
									if($cH->id == $cat->id)
			                    		$bandera=true;
		                	?>
							@if($bandera)
	                    		<input type="checkbox" name="cat{{$cat->id}}" id="cat{{$cat->id}}" value="{{$cat->id}}" checked>
	                    		<strong>{{$cat->nombre}}</strong>
	                    	@else
	                    		<input type="checkbox" name="cat{{$cat->id}}" id="cat{{$cat->id}}" value="{{$cat->id}}">
	                    		<strong>{{$cat->nombre}}</strong>
	                    	@endif
		                </label>
		            </div>
		            @endforeach
		        </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
		            <h2>Fueros</h2>
		            @foreach($fueros as $fue)
		            <div class="checkbox">
		                <label>
		                	<?php
		                		$bandera=false;
		                		foreach($fueHabilitante as $fH)
									if($fH->id == $fue->id)
			                    		$bandera=true;
		                	?>
							@if($bandera)
	                    		<input type="checkbox" name="fue{{$fue->id}}" id="fue{{$fue->id}}" value="{{$fue->id}}" checked>
	                    		<strong>{{$fue->nombre}}</strong>
	                    	@else
	                    		<input type="checkbox" name="fue{{$fue->id}}" id="fue{{$fue->id}}" value="{{$fue->id}}">
	                    		<strong>{{$fue->nombre}}</strong>
	                    	@endif
		                </label>
		            </div>
		            @endforeach
		        </div>
			</div>
		</div>
	</div>
	
	<div class="panel panel-primary">
		<div class="panel-body">
			<div class="form-group" align="center">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
		</div>
	</div>

	{!!Form::close()!!}

@endsection