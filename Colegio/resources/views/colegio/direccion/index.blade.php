@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Direcciones de los Matriculados
			<a href="{{asset('colegio/direccion/create')}}">
				<button class="btn btn-success pull-right">Nueva Dirección</button>
			</a>
		</h3>
	</div>
</div>

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
			@include('colegio.direccion.search')	
	</div>
</div>
@if($searchText!='')
	<div class="panel panel-primary" title="Datos del Matriculado">
		<div class="panel-body">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="form-group" align="center">
					<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_foto)}}" alt="{{$matricula->link_foto}}" height="150px" width="150px" class="img-thumbnail">
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre"><h4><strong>{{$matricula->nombre}}</strong></h4></label><br>
					<label for="numero_matricula"><h4><strong>Matrícula: {{$matricula->numero_matricula}}</strong></h4></label><br>
					<label for="dni">DNI: {{$matricula->dni}}</label><br>
					<label for="titulo">Título: {{$matricula->titulo}}</label>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-primary" title="Dirección">
		<div class="panel-body">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
					<table class="table table-striped table-borderd table-condensed table-hover">
						<thead>
							<th>Id</th>
							<th>Tipo</th>
							<th>Departamento</th>
							<th>Localidad</th>
							<th>Calle</th>
							<th>Nº</th>
							<th>Teléfono</th>
							<th>Correo</th>
							<th>Categorías</th>
							<th>Fueros</th>
							<th>Fecha Actualización</th>
							<th>Opciones</th>
						</thead>
						@foreach ($direcciones as $dir)
						<tr>
							<td>{{$dir->id}}</td>
							<td>{{$dir->tipo_direccion}}</td>
							<td>{{$dir->departamento}}</td>
							<td>{{$dir->localidad}}</td>
							<td>{{$dir->calle}}</td>
							<td>{{$dir->numero}}</td>
							<td>{{$dir->telefono}}</td>
							<td>{{$dir->correo}}</td>
							<td align="center">
								<strong>@include('colegio.direccion.searchCat')</strong>
							</td>
							<td>
								<strong>@include('colegio.direccion.searchFue')</strong>
							</td>
							<?php
								$dt = new DateTime($dir->fecha_modificacion);
								$fecha=$dt->format('d/m/Y');
							?>
							<td>{{$fecha}}</td>
							<td>
								<a href="{{URL::action('DireccionController@edit',$dir->id)}}">
									<button class="btn btn-sm btn-info">Editar</button>
								</a>
								<a href="" data-target="#modal-delete-{{$dir->id}}" data-toggle="modal">
									<button class="btn btn-sm btn-danger">Eliminar</button>
								</a>
							</td>
						</tr>
						@include('colegio.direccion.modal')
						@endforeach
					</table>
				</div>
				<div align="center">{{$direcciones->render()}}</div>
			</div>
		</div>
	</div>
@else
	<h4>Seleccione un Matriculado...</h4>
@endif

@endsection