@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h3>Cargar Nueva Dirección</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>

	{!!Form::open(array('url'=>'colegio/direccion','method'=>'POST','autocomplete'=>'off','files'=>'true'))!!}
	{{Form::token()}}

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
					<label for="nombre">Matriculado(*)</label>
					<select name="id_matricula" id="id_matricula" required="true" class="form-control selectpicker" data-live-search="true">	
						@foreach($matriculas as $mat)
							<option value="{{$mat->id}}">{{$mat->matricula}}</option>
						@endforeach
					</select>
			</div>
		</div>
	</div>
</div>

<h3>Dirección Procesal</h3>
<div class="panel panel-primary" title="Dirección Procesal del Profesional">
	<div class="panel-body">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="localidad">Departamento y Localidad(*)</label>
					<select name="id_localidad" id="id_localidad" required="true" class="form-control selectpicker" data-live-search="true">
						@foreach($localidades as $loc)
							<option value="{{$loc->id}}">{{$loc->localidad}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="calle">Calle(*)</label>
					<input type="text" name="calle" required value="{{old('calle')}}" class="form-control" placeholder="Calle...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="numero">Número(*)</label>
					<input type="text" name="numero" required value="{{old('numero')}}" class="form-control" placeholder="Número...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="telefono">Teléfono(*)</label>
					<input type="text" name="telefono" required value="{{$direccion->telefono}}" class="form-control" placeholder="Teléfono...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="correo">Correo(*)</label>
					<input type="text" name="correo" required value="{{$direccion->correo}}" class="form-control" placeholder="Correo...">
				</div>
			</div>
	</div>
</div>

<div class="panel panel-primary" title="Acciones">
	<div class="panel-body">
			<div class="form-group" align="center">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
	</div>
</div>
			{!!Form::close()!!}
@endsection