@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h3>Especializaciones y Categorías Habilitantes</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
	</div>
</div>

			{!!Form::model($matricula,['method'=>'PATCH','route'=>['cathabilitante.update',$matricula->id],'files'=>'true'])!!}
			{{Form::token()}}

			<div class="panel panel-primary" title="Profesional">
				<div class="panel-body">
					
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="form-group" align="center">
							<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_foto)}}" alt="{{$matricula->link_foto}}" height="150px" width="150px" class="img-thumbnail">
						</div>
					</div>
					
					<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="nombre"><h4><strong>{{$matricula->nombre}}</strong></h4></label><br>
							<label for="numero_matricula"><h4><strong>Matrícula: {{$matricula->numero_matricula}}</strong></h4></label><br>
							<label for="dni">DNI: {{$matricula->dni}}</label><br>
							<label for="titulo">Título: {{$matricula->titulo}}</label>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-primary" title="Especializaciones y Categorias Habilitantes">
				<div class="panel-body">					
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
				            <h2>Especializaciones</h2>
				            @foreach($especializaciones as $esp)
				            <div class="checkbox">
				                <label>
				                	<?php
				                		$bandera=false;
				                		foreach($espHabilitante as $eH)
											if($eH->id == $esp->id)
					                    		$bandera=true;
				                	?>
									@if($bandera)
			                    		<input type="checkbox" name="esp{{$esp->id}}" id="esp{{$esp->id}}" value="{{$esp->id}}" checked>
			                    		<strong>{{$esp->nombre}}</strong>
			                    	@else
			                    		<input type="checkbox" name="esp{{$esp->id}}" id="esp{{$esp->id}}" value="{{$esp->id}}">
			                    		<strong>{{$esp->nombre}}</strong>
			                    	@endif
				                </label>
				            </div>
				            @endforeach
				        </div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
				            <h2>Categorías</h2>
				            @foreach($categorias as $cat)
				            <div class="checkbox">
				                <label>
				                    <?php
				                		$bandera=false;
				                		foreach($catHabilitante as $cH)
											if($cH->id == $cat->id)
					                    		$bandera=true;
				                	?>
									@if($bandera)
			                    		<input type="checkbox" name="cat{{$cat->id}}" id="cat{{$cat->id}}" value="{{$cat->id}}" checked>
			                    		<strong>{{$cat->nombre}}</strong>
			                    	@else
			                    		<input type="checkbox" name="cat{{$cat->id}}" id="cat{{$cat->id}}" value="{{$cat->id}}">
			                    		<strong>{{$cat->nombre}}</strong>
			                    	@endif
				                </label>
				            </div>
				            @endforeach
				        </div>
					</div>
				</div>
			</div>

			
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="form-group" align="center">
						<button class="btn btn-primary" type="submit">Guardar</button>
						<button class="btn btn-danger" type="reset">Cancelar</button>
					</div>
				</div>
			</div>

	{!!Form::close()!!}

@endsection