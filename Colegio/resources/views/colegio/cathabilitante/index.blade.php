@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Especializaciones y Categorias Habilitantes
			<a href="{{asset('especializacion')}}">
				<button class="btn btn-success pull-right">Ver Especializaciones</button>
			</a>
			<a href="{{asset('categoria')}}">
				<button class="btn btn-success pull-right">Ver Categorías</button>
			</a>
		</h3>
		@include('colegio.cathabilitante.search')
	</div>
</div>

<div class="panel panel-primary" title="Especializaciones y Categorías">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Foto</th>
						<th>Matrícula</th>
						<th>DNI</th>
						<th>Nombre</th>
						<th>Título</th>
						<th>Especializaciones Habilitantes</th>
						<th>Categorías Habilitantes</th>
						<th>Opciones</th>
					</thead>
					@foreach ($matriculas as $mat)
					<tr>
						<td>{{$mat->id}}</td>
						<td>
							<img src="{{asset('imagenes/matricula/foto/'.$mat->link_foto)}}" alt="{{$mat->link_foto}}" height="100px" width="100px" class="img-thumbnail">
						</td>
						<td>{{$mat->numero_matricula}}</td>
						<td>{{$mat->dni}}</td>
						<td>{{$mat->nombre}}</td>
						<td>{{$mat->titulo}}</td>

						<td align="center">
							<strong>@include('colegio.cathabilitante.searchEsp')</strong>
						</td>
						
						<td align="center">
							<strong>@include('colegio.cathabilitante.searchCat')</strong>
						</td>
						
						<td>
							<a href="{{URL::action('CatHabilitanteController@edit',$mat->id)}}">
								<button class="btn btn-sm btn-info">Editar</button>
							</a>
						</td>
					</tr>
					@endforeach
				</table>
			</div>
			<div align="center">{{$matriculas->render()}}</div>
		</div>
	</div>
</div>

@endsection