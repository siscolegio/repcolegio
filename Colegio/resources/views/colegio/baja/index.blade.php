@extends('admin.layout')

@section('contenido') 

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>Reingresar Matriculado dado de Baja&#09;<a href="{{asset('colegio/matricula')}}"><button class="btn btn-success">Lista de Matriculados</button></a></h3>
	</div>
</div>

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
			@include('colegio.baja.search')	
	</div>
</div>
@if($searchText!='')
	<div class="panel panel-primary" title="Datos del Matriculado">
		<div class="panel-body">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="form-group" align="center">
					<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_foto)}}" alt="{{$matricula->link_foto}}" height="150px" width="150px" class="img-thumbnail">
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre"><h4><strong>{{$matricula->nombre}}</strong></h4></label><br>
					<label for="numero_matricula"><h4><strong>Matrícula: {{$matricula->numero_matricula}}</strong></h4></label><br>
					<label for="dni">DNI: {{$matricula->dni}}</label><br>
					<label for="titulo">Título: {{$matricula->titulo}}</label>
					<label for="titulo">Fecha Matriculacón: {{$matricula->fecha_alta}}</label><br>
					<label for="titulo">Fecha Baja: {{$matricula->fecha_baja}}</label>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-primary" title="Dirección">
		<div class="panel-body">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center">
				<a href="" data-target="#modal-delete-{{$matricula->id}}" data-toggle="modal"><button class="btn btn-danger">Pasar el Profesional a Estado Nuevo</button></a>
				@include('colegio.baja.modal')
			</div>
		</div>
	</div>
@else
	<h4>Seleccione un Matriculado...</h4>
@endif

@endsection