{!!Form::open(array('url'=>'colegio/baja','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
	<div class="form-group">
		<div class="input-group">
			<label for="nombre">Matriculado(*)</label>
			<select name="searchText" id="searchText" required="true" class="form-control selectpicker" data-live-search="true">	
				@foreach($matriculas as $mat)
						@if($mat->id == $matricula->id)
								<option value="{{$mat->id}}" selected>{{$mat->matricula}}</option>
							@else
								<option value="{{$mat->id}}">{{$mat->matricula}}</option>
							@endif
				@endforeach
			</select>
			<span>
				<button type="submit" class="btn btn-primary">Buscar</button>
			</span>
		</div>
	</div>
</div>

{{Form::close()}}