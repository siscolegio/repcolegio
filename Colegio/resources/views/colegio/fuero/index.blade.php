@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Listado de Fueros 
			@can ('fuero.create')
			<a href="{{	route ('fuero.create')	}}">
				<button class="btn btn-success pull-right">Nuevo</button>
			</a>
			@endcan
		</h3>
		@include('colegio.fuero.search')
	</div>
</div>

<div class="panel panel-primary" title="Fueros">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Opciones</th>
					</thead>
					@foreach ($fueros as $fue)
					<tr>
						<td>{{$fue->id}}</td>
						<td>{{$fue->nombre}}</td>
						<td>
						@can ('fuero.edit')
							<a href="{{ route ('fuero.edit',$fue->id)}}">
								<button class="btn btn-sm btn-info">Editar</button>
							</a>
						@endcan 
						@can ('fuero.destroy')
							<a href="" data-target="#modal-delete-{{$fue->id}}" data-toggle="modal">
								<button class="btn btn-sm btn-danger">Eliminar</button>
							</a>
						@endcan
						</td>
					</tr>
					@include('colegio.fuero.modal')
					@endforeach
				</table>
			</div>
			<div align="center">{{$fueros->render()}}</div>
		</div>
	</div>
</div>

@endsection