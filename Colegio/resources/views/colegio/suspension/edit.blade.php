@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h3>Editar Suspensión</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
</div>

			{!!Form::model($suspension,['method'=>'PATCH','route'=>['suspension.update',$suspension->id],'files'=>'true'])!!}
			{{Form::token()}}

			<div class="panel panel-primary" title="Suspensión">
				<div class="panel-body">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="nombre">Matriculado(*)</label>
							<select name="id_matricula" id="id_matricula" required="true" class="form-control selectpicker" data-live-search="true">
								@foreach($matriculas as $mat)
									@if($mat->id == $suspension->id_matricula)
										<option value="{{$mat->id}}" selected>{{$mat->matricula}}</option>
									@else
										<option value="{{$mat->id}}">{{$mat->matricula}}</option>
									@endif
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="desde">Fecha Desde(*)</label>
							<input type="date" name="desde" required value="{{$suspension->desde}}" class="form-control">
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="hasta">Fecha Hasta(*)</label>
							<input type="date" name="hasta" required value="{{$suspension->hasta}}" class="form-control">
						</div>
					</div>
				</div>
			</div>

			<h3>Cargar Resolución</h3>
			<div class="panel panel-primary" title="Resolución">
				<div class="panel-body" >
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="numero">Número(*)</label>
							<input type="text" name="numero" required value="{{$resolucion->numero}}" class="form-control" placeholder="Número de Resolución...">
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="fecha">Fecha Resolución(*)</label>
							<input type="date" name="fecha" required value="{{$resolucion->fecha}}" class="form-control">
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="lugar">Lugar de Emisión(*)</label>
							<input type="text" name="lugar" required value="{{$resolucion->lugar}}" class="form-control" placeholder="Lugar...">
						</div>
					</div>
					<div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="link">Resolución Escaneada</label>
							<input type="file" name="link" class="form-control">
							@if (($resolucion->link)!="")
								<img src="{{asset('imagenes/resolucion/suspensiones/'.$resolucion->link)}}" alt="{{$resolucion->link}}" height="100px" width="100px" class="img-thumbnail">
							@endif
						</div>
					</div>
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="form-group" align="center">
						<button class="btn btn-primary" type="submit">Guardar</button>
						<button class="btn btn-danger" type="reset">Cancelar</button>
					</div>
				</div>
			</div>

	{!!Form::close()!!}

@endsection