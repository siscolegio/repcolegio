<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-periodo-pago">
	{{Form::Open(array('action'=>array('PagoController@index'),'method'=>'GET'))}}
	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-lebel="Close">
					<span aria-hidden="true">x</span>
				</button>
				<h4 class="modal-title">Cargar Período de Pago</h4>
			</div>
			<div class="modal-body">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="mes">Mes</label>
						<select name="mes" id="mes" class="form-control selectpicker" data-live-search="true">
							<option value="">Seleccione Mes</option>
							@for ($i=1; $i < 13; $i++)
								<option value="{{$i}}">{{$i}}</option>
							@endfor
						</select>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="anio">Año</label>
						<select name="anio" id="anio" class="form-control selectpicker" data-live-search="true">
							<option value="">Seleccione Año</option>
							@for ($i=2014; $i < 2036; $i++)
								<option value="{{$i}}">{{$i}}</option>
							@endfor
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>