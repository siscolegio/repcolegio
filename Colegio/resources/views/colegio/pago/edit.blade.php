@extends('admin.layout')

@section('contenido')

<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Pago</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
</div>

	{!!Form::model($pago,['method'=>'PATCH','route'=>['pago.update',$pago->id],'files'=>'true'])!!}
	{{Form::token()}}

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<div class="form-group">
					<label for="nombre">Matriculado(*)</label>
					<select name="id_matricula" id="id_matricula" required="true" class="form-control selectpicker" data-live-search="true">	
						<option value="">Seleccione un matriculado</option>
						@foreach($matriculas as $mat)
							@if($pago->id_matricula == $mat->id))
								<option value="{{$mat->id}}" selected>{{$mat->matricula}}</option>
							@else
								<option value="{{$mat->id}}">{{$mat->matricula}}</option>
							@endif
						@endforeach
					</select>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-primary" title="Pago del Profesional">
	<div class="panel-body">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label>Tipo de Pago(*)</label>
					<select name="id_tipo_pago" id="id_tipo_pago" class="form-control selectpicker">
						<option value="" selected>Seleccione un tipo de pago</option>
						@foreach($tipo_pagos as $tp)
							@if($pago->id_tipo_pago == $tp->id))
								<option value="{{$tp->id}}" selected>{{$tp->descripcion}}</option>
							@else
								<option value="{{$tp->id}}">{{$tp->descripcion}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label>Descripción Factura(*)</label>
					<input type="text" name="descripcion" required value="{{$pago->descripcion}}" class="form-control" placeholder="Descripción...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label>Monto Factura(*)</label>
					<input type="text" name="monto" required value="{{$pago->monto}}" class="form-control" placeholder="$000,00">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label>Comprobante Escaneado</label>
					<input type="file" name="link" class="form-control">
					@if (($pago->link)!="")
						<img src="{{asset('imagenes/pago/'.$pago->link)}}" alt="{{$pago->link}}" height="100px" width="100px" class="img-thumbnail">
					@endif
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<div class="form-group">
						<label>Fecha Factura(*)</label>
						<input type="date" name="fecha_pago" required value="{{$pago->fecha_pago}}" class="form-control">
					</div>
				</div>
			</div>
	</div>
</div>

<h3>Pago Cuota</h3><h5>(para tipo de pago 'Cuota Mensual')</h5>
<div class="panel panel-primary" title="Pago del Profesional">
	<div class="panel-body">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">			
				<label>Cuota 1: Mes</label>
				<select name="mes1" id="mes1" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Mes</option>
					@for ($i=1; $i < 13; $i++)
						@if($aPP[0]["mes"]== $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
				<label>Cuota 1: Año</label>
				<select name="anio1" id="anio1" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Año</option>
					@for ($i=2014; $i < 2036; $i++)
						@if($aPP[0]["anio"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">			
				<label>Cuota 2: Mes</label>
				<select name="mes2" id="mes2" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Mes</option>
					@for ($i=1; $i < 13; $i++)
						@if($aPP[1]["mes"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif>
					@endfor
				</select>
				<label>Cuota 2: Año</label>
				<select name="anio2" id="anio2" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Año</option>
					@for ($i=2014; $i < 2036; $i++)
						@if($aPP[1]["anio"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">			
				<label>Cuota 3: Mes</label>
				<select name="mes3" id="mes3" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Mes</option>
					@for ($i=1; $i < 13; $i++)
						@if($aPP[2]["mes"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
				<label>Cuota 3: Año</label>
				<select name="anio3" id="anio3" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Año</option>
					@for ($i=2014; $i < 2036; $i++)
						@if($aPP[2]["anio"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">			
				<label>Cuota 4: Mes</label>
				<select name="mes4" id="mes4" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Mes</option>
					@for ($i=1; $i < 13; $i++)
						@if($aPP[3]["mes"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
				<label>Cuota 4: Año</label>
				<select name="anio4" id="anio4" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Año</option>
					@for ($i=2014; $i < 2036; $i++)
						@if($aPP[3]["anio"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">			
				<label>Cuota 5: Mes</label>
				<select name="mes5" id="mes5" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Mes</option>
					@for ($i=1; $i < 13; $i++)
						@if($aPP[4]["mes"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
				<label>Cuota 5: Año</label>
				<select name="anio5" id="anio5" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Año</option>
					@for ($i=2014; $i < 2036; $i++)
						@if($aPP[4]["anio"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">			
				<label>Cuota 6: Mes</label>
				<select name="mes6" id="mes6" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Mes</option>
					@for ($i=1; $i < 13; $i++)
						@if($aPP[5]["mes"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
				<label>Cuota 6: Año</label>
				<select name="anio6" id="anio6" class="form-control selectpicker" data-live-search="true">
					<option value="">Seleccione Año</option>
					@for ($i=2014; $i < 2036; $i++)
						@if($aPP[5]["anio"] == $i)
							<option value="{{$i}}" selected>{{$i}}</option>
						@else
							<option value="{{$i}}">{{$i}}</option>
						@endif
					@endfor
				</select>
			</div>
	</div>
</div>
	
		<div class="panel panel-primary" title="Acciones">
			<div class="panel-body">
					<div class="form-group" align="center">
						<button class="btn btn-primary" type="submit">Guardar</button>
						<button class="btn btn-danger" type="reset">Cancelar</button>
					</div>
			</div>
		</div>
<input type="text" name="err" disabled style="visibility:hidden" value="">
{!!Form::close()!!}

@endsection