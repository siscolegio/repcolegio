@extends('admin.layout')

@section('contenido')

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>Pagos de los Matriculados &#09;<a href="{{asset('colegio/pago/create')}}"><button class="btn btn-success">Nuevo pago</button></a></h3>
	</div>
</div>

<div class="panel panel-primary" title="Buscar Matriculado">
	<div class="panel-body">
			@include('colegio.pago.search')
	</div>
</div>
	<div class="panel panel-primary" title="Pagos">
		<div class="panel-body">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
					<table class="table table-striped table-borderd table-condensed table-hover">
						<thead>
							<th>Id</th>
							<th>Profesional</th>
							<th>Descripción</th>
							<th>Tipo Pago</th>
							<th>Período Pago</th>
							<th>Monto</th>
							<th>Nombre</th>
							<th>Fecha</th>
							<th>Link</th>
							<th>Opciones</th>
						</thead>
						@foreach ($pagos as $pag)
						<tr>
							<td>{{$pag->id}}</td>
							<td>{{$pag->nombre}}</td>
							<td>{{$pag->descripcion}}</td>
							<td>{{$pag->tipo_pago}}</td>
							@if($pag->id_tipo_pago=='2')
							<td align="center">
								<strong>@include('colegio.pago.searchPP')</strong>
							</td>
							@else
							<td align="center">Único</td>
							@endif
							<td>${{$pag->monto}}</td>
							<td>{{$pag->nombre}}</td>
							<?php
								$dt = new DateTime($pag->fecha_pago);
								$fecha=$dt->format('d/m/Y');
							?>
							<td>{{$fecha}}</td>
							<td><a href="{{asset('imagenes/pago/'.$pag->link)}}">{{$pag->link}}</a></td>
							<td>
								<a href="{{URL::action('PagoController@edit',$pag->id)}}"><button class="btn btn-info">Editar</button></a>
								<a href="" data-target="#modal-delete-{{$pag->id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
							</td>
						</tr>
						@include('colegio.pago.modal')
						@endforeach
					</table>
				</div>
				<div align="center">{{$pagos->render()}}</div>
			</div>
		</div>
	</div>

@endsection