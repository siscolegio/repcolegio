{!!Form::open(array('url'=>'colegio/pago','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}

		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<label for="matricula">Matriculado</label>
			<select name="searchText1" id="searchText1" class="form-control selectpicker" data-live-search="true">	
				<option value="" selected>Seleccione un matriculado</option>
				@foreach($matriculas as $mat)
						@if($mat->id == $matricula->id)
								<option value="{{$mat->id}}" selected>{{$mat->matricula}}</option>
							@else
								<option value="{{$mat->id}}">{{$mat->matricula}}</option>
							@endif
				@endforeach
			</select>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<label for="tipo_pago">Tipo de Pago</label>
			<select name="searchText2" id="searchText2" class="form-control selectpicker">
					<option value="" selected>Seleccione un tipo de pago</option>
				@foreach($tipo_pagos as $tp)
						@if($tp->id == $searchText2)
								<option value="{{$tp->id}}" selected>{{$tp->descripcion}}</option>
							@else
								<option value="{{$tp->id}}">{{$tp->descripcion}}</option>
							@endif
				@endforeach
			</select>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<label for="descripcion">Descripción</label>
			<input type="text" class="form-control" name="searchText4" value="{{$searchText4}}">
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<label for="fecha">Fecha del Pago</label>
			<input type="date" class="form-control" name="searchText3" value="{{$searchText3}}">
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<span>
				<button type="submit" class="btn btn-primary">Buscar</button>
			</span>
		</div>

{{Form::close()}}