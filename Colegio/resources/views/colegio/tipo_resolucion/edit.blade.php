@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Editar Tipo de Resolución: {{$tipo_resolucion->nombre}}</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

		{!!Form::model($tipo_resolucion, ['route'=>['tipo_resolucion.update', $tipo_resolucion->id], 'method'=>'PUT'])!!}
		{{Form::token()}}

<div class="panel panel-primary" title="Informacion de Categoría">
	<div class="panel-body">
		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" name="nombre" class="form-control" value="{{$tipo_resolucion->nombre}}" placeholder="Nombre...">
		</div>
	</div>
</div>

		<div class="form-group" align="center">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger" type="reset">Cancelar</button>
		</div>

		{!!Form::close()!!}

@endsection