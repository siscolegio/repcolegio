@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Listado de Tipos de Resoluciones
		@can ('tipo_resolucion.create')
			<a href="{{	route ('tipo_resolucion.create')	}}">
				<button class="btn btn-success pull-right">Nuevo</button>
			</a>
		@endcan
		</h3>
	@include('colegio.tipo_resolucion.search')
	</div>
</div>

<div class="panel panel-primary" title="Tipo Resolucion">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Opciones</th>
					</thead>
					@foreach ($tipo_resoluciones as $tres)
					<tr>
						<td>{{$tres->id}}</td>
						<td>{{$tres->nombre}}</td>
						<td>
						@can ('tipo_resolucion.edit')
							<a href="{{	route ('tipo_resolucion.edit', $tres->id)}}">
								<button class="btn btn-sm btn-info">Editar</button>
							</a>
						@endcan
						@can ('tipo_resolucion.destroy')
							<a href="" data-target="#modal-delete-{{$tres->id}}" data-toggle="modal">
								<button class="btn btn-sm btn-danger">Eliminar</button>
							</a>
						@endcan
						</td>
					</tr>
					@include('colegio.tipo_resolucion.modal')
					@endforeach
				</table>
			</div>
			<div align="center">{{$tipo_resoluciones->render()}}</div>
		</div>
	</div>
</div>

@endsection