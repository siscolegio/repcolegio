@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h3>Nuevo Profesional</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
			{!!Form::open(array('url'=>'colegio/matricula','method'=>'POST','autocomplete'=>'off','files'=>'true'))!!}
			{{Form::token()}}

<div class="panel panel-primary" title="Datos del Profesional">
	<div class="panel-body">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="nombre">Apellido y Nombre(*)</label>
						<input type="text" name="nombre" required value="{{old('nombre')}}" class="form-control" placeholder="Apellido y Nombre...">
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link_foto">Foto</label>
						<input type="file" name="link_foto" class="form-control">
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="nacionalidad">Nacionalidad(*)</label>
						<input type="text" name="nacionalidad" required value="{{old('nacionalidad')}}" class="form-control" placeholder="Nacionalidad...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="dni">DNI(*)</label>
						<input type="text" name="dni" required value="{{old('dni')}}" class="form-control" placeholder="Número de documento...">
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link_dni">DNI Escaneado</label>
						<input type="file" name="link_dni" class="form-control">
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="fecha_nacimiento">Fecha Nacimiento(*)</label>
						<input type="date" name="fecha_nacimiento" required value="{{old('fecha_nacimiento')}}" class="form-control" placeholder="dd/mm/aaaa">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="titulo">Título(*)</label>
						<input type="text" name="titulo" required value="{{old('titulo')}}" class="form-control" placeholder="Título...">
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link_titulo">Título Escaneado</label>
						<input type="file" name="link_titulo" class="form-control">
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link_buena_conducta">Certificado de Buena Conducta Escaneado</label>
						<input type="file" name="link_buena_conducta" class="form-control">
					</div>
				</div>

	</div>
</div>
<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Dirección Principal</h3>
	</div>
</div>
<div class="panel panel-primary" title="Dirección Principal del Profesional">
	<div class="panel-body">

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="localidad">Departamento y Localidad(*)</label>
					<select name="id_localidad" id="id_localidad" required="true" class="form-control selectpicker" data-live-search="true">
						<option value="">Seleccione una localidad</option>
						@foreach($localidades as $loc)
							<option value="{{$loc->id}}">{{$loc->localidad}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="calle">Calle(*)</label>
					<input type="text" name="calle" required value="{{old('calle')}}" class="form-control" placeholder="Calle...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="numero">Número(*)</label>
					<input type="text" name="numero" required value="{{old('numero')}}" class="form-control" placeholder="Número...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="telefono">Teléfono(*)</label>
					<input type="text" name="telefono" required value="{{old('telefono')}}" class="form-control" placeholder="Teléfono...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="correo">Correo(*)</label>
					<input type="text" name="correo" required value="{{old('correo')}}" class="form-control" placeholder="Correo...">
				</div>
			</div>

	</div>
</div>

<div class="panel panel-primary" title="Acciones">
	<div class="panel-body">
			<div class="form-group" align="center">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
	</div>
</div>
			{!!Form::close()!!}

@endsection