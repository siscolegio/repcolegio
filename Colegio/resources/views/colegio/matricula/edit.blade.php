@extends('admin.layout')

@section('contenido')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<h3>Editar Profesional: <strong>{{$matricula->nombre}}</strong></h3>
		@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
		@endif
</div>


	{!!Form::model($matricula,['method'=>'PATCH','route'=>['matricula.update',$matricula->id],'files'=>'true'])!!}
	{{Form::token()}}

@if($matricula->numero_matricula!='')
	<h3>Datos de Matriculación</h3>
	<div class="panel panel-primary" title="Matriculació">
		<div class="panel-body">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="numero_matricula">Número de Matrícula(*)</label><br>
					<input type="text" name="numero_matricula" required value="{{$matricula->numero_matricula}}" class="form-control">
				</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="numero">Nº Resolución(*)</label>
					<input type="text" name="numero" required value="{{$resolucion->numero}}" class="form-control">
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="fecha">Fecha Resolución(*)</label>
					<input type="date" name="fecha" required value="{{$resolucion->fecha}}" class="form-control">
				</div>
			</div>
			<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="lugar">Lugar de Emisión(*)</label>
					<input type="text" name="lugar" required value="{{$resolucion->lugar}}" class="form-control">
				</div>
			</div>
			<div class="col-lg-5 col-md-4 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="link">Resolución Escaneada</label>
					<input type="file" name="link" class="form-control">
					@if (($resolucion->link)!="")
						<img src="{{asset('imagenes/resolucion/matriculacion/'.$resolucion->link)}}" alt="{{$resolucion->link}}" height="100px" width="100px" class="img-thumbnail">
					@endif
				</div>
			</div>
		</div>
	</div>
	@if($matricula->id_estado==4&&$matricula->fecha_baja!='')
		<h3>Datos de Baja</h3>
		<div class="panel panel-primary" title="Matriculació">
			<div class="panel-body">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="fecha_baja">Fecha de baja(*)</label><br>
						<input type="date" name="fecha_baja" required value="{{$matricula->fecha_baja}}" class="form-control">
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="numeroB">Nº Resolución(*)</label>
						<input type="text" name="numero_resolucion_baja" required value="{{$resolucionBaja->numero}}" class="form-control">
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="fechaB">Fecha Resolución(*)</label>
						<input type="date" name="fechaRB" required value="{{$resolucionBaja->fecha}}" class="form-control">
					</div>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="lugarB">Lugar de Emisión(*)</label>
						<input type="text" name="lugarRB" required value="{{$resolucionBaja->lugar}}" class="form-control">
					</div>
				</div>
				<div class="col-lg-5 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="linkB">Resolución Escaneada</label>
						<input type="file" name="linkRB" class="form-control">
						@if (($resolucionBaja->link)!="")
							<img src="{{asset('imagenes/resolucion/baja/'.$resolucionBaja->link)}}" alt="{{$resolucionBaja->link}}" height="100px" width="100px" class="img-thumbnail">
						@endif
					</div>
				</div>
			</div>
		</div>
	@endif
@endif

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Datos Personales</h3>
	</div>
</div>

		<div class="panel panel-primary" title="Datos del Profesional">
			<div class="panel-body">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="nombre">Apellido y Nombre(*)</label>
						<input type="text" name="nombre" required value="{{$matricula->nombre}}" class="form-control">
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link_foto">Foto</label>
						<input type="file" name="link_foto" class="form-control">
						@if (($matricula->link_foto)!="")
							<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_foto)}}" alt="{{$matricula->link_foto}}" height="100px" width="100px" class="img-thumbnail">
						@endif
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="nacionalidad">Nacionalidad(*)</label>
						<input type="text" name="nacionalidad" required value="{{$matricula->nacionalidad}}" class="form-control">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="dni">DNI(*)</label>
						<input type="text" name="dni" required value="{{$matricula->dni}}" class="form-control">
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link_dni">DNI Escaneado</label>
						<input type="file" name="link_dni" class="form-control">
						@if (($matricula->link_dni)!="")
							<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_dni)}}" alt="{{$matricula->link_dni}}" height="100px" width="100px" class="img-thumbnail">
						@endif
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="fecha_nacimiento">Fecha Nacimiento(*)</label>
						<input type="date" name="fecha_nacimiento" required value="{{$matricula->fecha_nacimiento}}" class="form-control">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="titulo">Título(*)</label>
						<input type="text" name="titulo" required value="{{$matricula->titulo}}" class="form-control">
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link_titulo">Título Escaneado</label>
						<input type="file" name="link_titulo" class="form-control">
						@if (($matricula->link_titulo)!="")
							<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_titulo)}}" alt="{{$matricula->link_titulo}}" height="100px" width="100px" class="img-thumbnail">
						@endif
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="link_buena_conducta">Certificado de Buena Conducta Escaneado</label>
						<input type="file" name="link_buena_conducta" class="form-control">
						@if (($matricula->link_buena_conducta)!="")
							<img src="{{asset('imagenes/matricula/foto/'.$matricula->link_buena_conducta)}}" alt="{{$matricula->link_buena_conducta}}" height="100px" width="100px" class="img-thumbnail">
						@endif
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Estado</label>
						<select name="id_estado" class="form-control" >
							@foreach ($estados as $est)
								@if ($est->id == $matricula->id_estado)
								<option value="{{$est->id}}" selected>{{$est->nombre}}</option>
								@else
								<option value="{{$est->id}}">{{$est->nombre}}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>
		</div>
	</div>

	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h3>Dirección Principal</h3>
		</div>
	</div>

<div class="panel panel-primary" title="Dirección Principal del Profesional">
	<div class="panel-body">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="localidad">Departamento y Localidad(*)</label>
					<select name="id_localidad" id="id_localidad" required="true" class="form-control selectpicker" data-live-search="true">
						@foreach($localidades as $loc)
						@if($loc->id == $direccion->id_localidad)
								<option value="{{$loc->id}}" selected>{{$loc->localidad}}</option>
							@else
								<option value="{{$loc->id}}">{{$loc->localidad}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="calle">Calle(*)</label>
					<input type="text" name="calle" required value="{{$direccion->calle}}" class="form-control" placeholder="Calle...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="numeroDir">Número(*)</label>
					<input type="text" name="numeroDir" required value="{{$direccion->numero}}" class="form-control" placeholder="Número...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="telefono">Teléfono(*)</label>
					<input type="text" name="telefono" required value="{{$direccion->telefono}}" class="form-control" placeholder="Teléfono...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="correo">Correo(*)</label>
					<input type="text" name="correo" required value="{{$direccion->correo}}" class="form-control" placeholder="Correo...">
				</div>
			</div>
	</div>
</div>

<div class="panel panel-primary" title="Acciones">
	<div class="panel-body">
			<div class="form-group" align="center">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
	</div>
</div>
<input type="text" name="err" disabled style="visibility:hidden" value="">
	{!!Form::close()!!}

@endsection