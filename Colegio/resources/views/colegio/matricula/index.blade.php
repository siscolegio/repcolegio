@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Listado de Profesionales
			<a href="{{asset('colegio/matricula/create')}}">
				<button class="btn btn-success pull-right">Nuevo</button>
			</a>
		</h3>
		@include('colegio.matricula.search')
	</div>
</div>

<div class="panel panel-primary" title="Profesionales">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Fecha Matriculación</th>
						<th>Matrícula</th>
						<th>Foto</th>
						<th>DNI</th>
						<th>Nacionalidad</th>
						<th>Fecha Nacimiento</th>
						<th>Título</th>
						<th>Resolución N°</th>
						<th>Estado</th>
						<th>Opciones</th>
					</thead>
					@foreach ($matriculas as $mat)
					<tr>
						<td>{{$mat->id}}</td>
						<td>{{$mat->nombre}}</td>
						<?php
							if($mat->fecha_alta!=''){
								$dt = new DateTime($mat->fecha_alta);
								$fechaMat=$dt->format('d/m/Y');
							} else {$fechaMat='';}
							$dtn = new DateTime($mat->fecha_nacimiento);
							$fechaNac=$dtn->format('d/m/Y');
						?>
						<td>{{$fechaMat}}</td>
						<td>{{$mat->numero_matricula}}</td>
						
						<td>
							<img src="{{asset('imagenes/matricula/foto/'.$mat->link_foto)}}" alt="{{$mat->link_foto}}" height="80px" width="80px" class="img-thumbnail">
						</td>
						
						<td>{{$mat->dni}}</td>
						<td>{{$mat->nacionalidad}}</td>
						<td>{{$fechaNac}}</td>
						<td>{{$mat->titulo}}</td>
						<td>
						@include('colegio.matricula.searchResolucion')
						</td>
						<td>{{$mat->estado}}</td>

						<td>
							<a href="" data-target="#modal-ver-info-{{$mat->id}}" data-toggle="modal">
								<button class="btn btn-sm btn-primary">Ver</button>
							</a>
							<a href="{{URL::action('MatriculaController@edit',$mat->id)}}">
								<button class="btn btn-sm btn-info">Editar</button>
							</a>
							@if($mat->id_estado==1)
								<a href="" data-target="#modal-delete-{{$mat->id}}" data-toggle="modal"><button class="btn btn-sm btn-danger">Eliminar</button></a>
							@else
								<a href="{{URL::action('BajaController@edit',$mat->id)}}">
									<button class="btn btn-sm btn-danger">Eliminar</button>
								</a>
							@endif
						</td>
					</tr>
					@include('colegio.matricula.modal')
					@include('colegio.matricula.modalInfo')
					@endforeach
				</table>
			</div>
			<div align="center">{{$matriculas->render()}}</div>
			<div align="right">
				<a href="{{URL::action('ExcelController@index')}}"><button class="btn btn-success">Exportar a Excel</button></a>
			</div>
		</div>
	</div>
</div>

@endsection