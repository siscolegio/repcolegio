<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-ver-info-{{$mat->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-lebel="Close">
					<span aria-hidden="true">x</span>
				</button>
				<h4 class="modal-title">Información del Profesional</h4>
			</div>
			<div class="modal-body">
				<div class="panel panel-primary" title="Información del Profesional">
					<div class="panel-body">
						
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="form-group" align="center">
								<img src="{{asset('imagenes/matricula/foto/'.$mat->link_foto)}}" alt="{{$mat->link_foto}}" height="150px" width="150px" class="img-thumbnail">
							</div>
						</div>
					
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<div class="form-group">
								<label for="nombre"><h4><strong>{{$mat->nombre}}</strong></h4></label><br>
								<label for="numero_matricula"><h4><strong>Matrícula: {{$mat->numero_matricula}}</strong></h4></label><br>
								<label for="resolucion">Resolución:  @include('colegio.matricula.searchResolucion')
								<?php
									if($mat->fecha_alta!=''){
										$dt = new DateTime($mat->fecha_alta);
										$fechaMat=$dt->format('d/m/Y');
									} else {$fechaMat='';}
									$dtn = new DateTime($mat->fecha_nacimiento);
									$fechaNac=$dtn->format('d/m/Y');
									if($mat->fecha_baja!=''){
										$dt = new DateTime($mat->fecha_baja);
										$fechaBaja=$dt->format('d/m/Y');
									} else {$fechaBaja='';}
								?>
								<label for="fechaMat">Fecha Matriculación: {{$fechaMat}}</label><br>
								@if($mat->fecha_baja!='')
									<label for="resolucion">Resolución Baja: @include('colegio.matricula.searchResolucionBaja')
									<label for="fechaMat">Fecha Baja: {{$fechaBaja}}</label><br>
								@endif
							</div>
						</div>

						<hr width="95%" />
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="dni">DNI: {{$mat->dni}}&nbsp;</label>
								@if($mat->link_dni!='')
									(<a href="{{asset('imagenes/matricula/dni/'.$mat->link_dni)}}">Ver</a>)
								@endif
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="nacionalidad">Nacionalidad: {{$mat->nacionalidad}}</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="fechaNac">Fecha Nacimiento: {{$fechaNac}}</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="titulo">Título: {{$mat->titulo}}&nbsp;</label>
								@if($mat->link_dni!='')
									(<a href="{{asset('imagenes/matricula/titulo/'.$mat->link_titulo)}}">Ver</a>)
								@endif
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label for="link_buena_conducta">Certificado Buena Conducta: &nbsp;</label><a href="{{asset('imagenes/matricula/conducta/'.$mat->link_buena_conducta)}}">{{$mat->link_buena_conducta}}</a>
							</div>
						</div>

						<hr width="95%" />
						<?php
							use Colegio\Http\Controllers\MatriculaController;
							$direccion=MatriculaController::direccion($mat->id);
						?>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="calle">Departamento: {{$direccion->departamento}}</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="calle">Localidad: {{$direccion->localidad}}</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="calle">Calle: {{$direccion->calle}}</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="numero">Número: {{$direccion->numero}}</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="telefono">Teléfono: {{$direccion->telefono}}</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label for="correo">Correo: {{$direccion->correo}}</label>
							</div>
						</div>
						
					</dir>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>