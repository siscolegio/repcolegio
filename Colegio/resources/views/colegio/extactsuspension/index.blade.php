@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Suspensiones Vencidas
			<a href="{{asset('colegio/suspension')}}">
				<button class="btn btn-success pull-right">Ver suspensiones</button>
			</a>
		</h3>
	</div>
</div>

<div class="panel panel-primary" title="Suspensión">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			@include('colegio.extactsuspension.search')
		</div>
	</div>
</div>

<div class="panel panel-primary" title="Suspensión">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Fecha Desde</th>
						<th>Fecha Hasta</th>
						<th>Matrícula</th>
						<th>Nombre</th>
						<th>DNI</th>
						<th>Foto</th>
						<th>Resolución N°</th>
						<th>Fecha Resolución</th>
						<th>Opciones</th>
					</thead>
					@foreach ($suspensiones as $sus)
					<tr>
						<td>{{$sus->id}}</td>
						<?php
							if($sus->hasta!=''){
								$dt = new DateTime($sus->hasta);
								$fechaHasta=$dt->format('d/m/Y');
							} else {$fechaHasta='';}
							$dtn = new DateTime($sus->desde);
							$fechaDesde=$dtn->format('d/m/Y');
							if($sus->fecha!=''){
								$dt = new DateTime($sus->fecha);
								$fechaResol=$dt->format('d/m/Y');
							} else {$fechaResol='';}
						?>
						<td>{{$fechaDesde}}</td>
						<td>{{$fechaHasta}}</td>
						<td>{{$sus->numero_matricula}}</td>
						<td>{{$sus->nombre}}</td>
						<td>{{$sus->dni}}</td>
						
						<td>
							<img src="{{asset('imagenes/matricula/foto/'.$sus->foto)}}" alt="{{$sus->foto}}" height="80px" width="80px" class="img-thumbnail">
						</td>
						
						<td>{{$sus->numero}}</td>
						<td>{{$fechaResol}}</td>

						<td>
							<a href="{{URL::action('ExtActSuspensionController@edit',$sus->id)}}"><button class="btn btn-info">Extender Suspensión</button></a>
							<a href="" data-target="#modal-delete-{{$sus->id}}" data-toggle="modal"><button class="btn btn-danger">Activar Matrícula</button></a>
						</td>
					</tr>
					@include('colegio.extactsuspension.modal')
					@endforeach
				</table>
			</div>
			<div align="center">{{$suspensiones->render()}}</div>
		</div>
	</div>
</div>

@endsection