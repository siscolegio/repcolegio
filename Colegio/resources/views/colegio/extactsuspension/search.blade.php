{!!Form::open(array('url'=>'colegio/extactsuspension','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}

<div class="form-group">
	<div class="input-group">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label>Nombre</label>
				<input type="text" class="form-control" name="searchText1" placeholder="Buscar..." value="{{$searchText1}}">
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label>N° Matrícula</label>
				<input type="text" class="form-control" name="searchText2" placeholder="Buscar..." value="{{$searchText2}}">
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center">
			<p>&nbsp;</p>
				<span>
				<button type="submit" class="btn btn-primary">Buscar</button>
			</span>
		</div>
	</div>
</div>

{{Form::close()}}