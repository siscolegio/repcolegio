@extends('admin.layout')

@section('contenido')

<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Extender Suspensión</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
</div>

{!!Form::model($suspension,['method'=>'PATCH','route'=>['extactsuspension.update',$suspension->id],'files'=>'true'])!!}
	{{Form::token()}}

	<div class="panel panel-primary" title="Suspensión">
		<div class="panel-body">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre">Matriculado(*)</label>
					<select name="id_matricul" id="id_matricul" required="true" class="form-control selectpicker" disabled data-live-search="true">
						@foreach($matriculas as $mat)
							@if($mat->id == $suspension->id_matricula)
								<option value="{{$mat->id}}" selected>{{$mat->matricula}}</option>
							@else
								<option value="{{$mat->id}}">{{$mat->matricula}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="desde">Fecha Desde(*)</label>
					<input type="date" name="desde" required value="{{$suspension->hasta}}" readonly class="form-control">
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="hasta">Fecha Hasta(*)</label>
					<input type="date" name="hasta" required value="{{old('hasta')}}" class="form-control">
				</div>
			</div>
		</div>
	</div>

	<h3>Cargar Resolución</h3>
	<div class="panel panel-primary" title="Resolución">
		<div class="panel-body" >
			
			<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="numero">Número(*)</label>
					<input type="text" name="numero" required value="{{old('numero')}}" class="form-control" placeholder="Formato: 001/2019">
				</div>
			</div>
			<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="fecha">Fecha Resolución(*)</label>
					<input type="date" name="fecha" required value="{{old('fecha')}}" class="form-control">
				</div>
			</div>
			<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="lugar">Lugar de Emisión(*)</label>
					<input type="text" name="lugar" required value="{{old('lugar')}}" class="form-control" placeholder="Lugar...">
				</div>
			</div>
			<div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="link">Resolución Escaneada</label>
					<input type="file" name="link" class="form-control">
				</div>
			</div>

		</div>
	</div>
	
	<div class="panel panel-primary">
		<div class="panel-body">
			<div class="form-group" align="center">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
		</div>
	</div>
<input type="text" name="id_matricula" value="{{$suspension->id_matricula}}" style="visibility:hidden">
{!!Form::close()!!}

@endsection