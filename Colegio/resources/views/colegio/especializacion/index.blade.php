@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Listado de Especializaciones 
			@can ('especializacion.create')
				<a href="{{	route ('especializacion.create') }}">
					<button class="btn btn-success pull-right">Nuevo</button>
				</a>
			@endcan
		</h3>
		@include('colegio.especializacion.search')
	</div>
</div>

<div class="panel panel-primary" title="Especializaciones">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Opciones</th>
					</thead>
					@foreach ($especializaciones as $esp)
					<tr>
						<td>{{$esp->id}}</td>
						<td>{{$esp->nombre}}</td>
						<td>
							@can ('especializacion.edit')
								<a href="{{route ('especializacion.edit', $esp->id)}}">
									<button class="btn btn-sm btn-info">Editar</button>
								</a>
							@endcan
							@can ('categoria.destroy')
								<a href="" data-target="#modal-delete-{{$esp->id}}" data-toggle="modal">
									<button class="btn btn-sm btn-danger">Eliminar</button>
								</a>
							@endcan
						</td>
					</tr>
					@include('colegio.especializacion.modal')
					@endforeach
				</table>
			</div>
			<div align="center">{{$especializaciones->render()}}</div>
		</div>
	</div>
</div>

@endsection