@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Listado de Tipos de Direcciones 
			@can ('tipo_direccion.create')
			<a href="{{	route ('tipo_direccion.create')	}}">
				<button class="btn btn-success pull-right">Nuevo</button>
			</a>
			@endcan
		</h3>
	@include('colegio.tipo_direccion.search')
	</div>
</div>

<div class="panel panel-primary" title="Tipo Direccion">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Opciones</th>
					</thead>
					@foreach ($tipo_direcciones as $tdir)
					<tr>
						<td>{{$tdir->id}}</td>
						<td>{{$tdir->nombre}}</td>
						<td>
						@can ('tipo_direccion.edit')
							<a href="{{	route ('tipo_direccion.edit', $tdir->id)}}">
								<button class="btn btn-sm btn-info">Editar</button>
							</a>
						@endcan
						@can ('tipo_direccion.destroy')
							<a href="" data-target="#modal-delete-{{$tdir->id}}" data-toggle="modal">
								<button class="btn btn-sm btn-danger">Eliminar</button>
							</a>
						@endcan
						</td>
					</tr>
					@include('colegio.tipo_direccion.modal')
					@endforeach
				</table>
			</div>
			<div align="center">{{$tipo_direcciones->render()}}</div>
		</div>
	</div>
</div>

@endsection