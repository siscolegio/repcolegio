@extends('admin.layout')

@section('contenido')


                            <br>
                            <br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-1"><!--0(izquierda),1(Centrado),2(derecha) -->
            <div class="panel panel-default">
                <!-- <div class="panel-heading">Dashboard</div> -->
                <div align="center" class="panel-heading">
                    <h3>Bienvenidos a la base de datos de </h3>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                         <div align="center">
                            <img src="/adminlte/img/logo-copcacer-transparente.png">
                            <br>
                            <br>
                            <h4>!Acceso concedido!</h4>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
