@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Listado de Usuarios
			@can('users.create')
			<a href="{{ route('users.create') }}">
				<button class="btn btn-success pull-right">Nuevo</button></a>
			@endcan
		</h3>

	</div>
</div>

<div class="panel panel-primary" title="Usuarios">
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-borderd table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Email</th>
						<th>Opciones</th>
					</thead>
					@foreach ($users as $user)
					<tr>
						<td>{{$user->id}}</td>
						<td>{{$user->name}}</td>
						<td>{{$user->email}}</td>
						<td>
							@can('users.edit')
							<a href="{{ route('users.edit', $user->id)}}">
								<button class="btn btn-sm btn-info">Editar</button></a>
							@endcan
							@can('users.destroy')
							<a href="" data-target="#modal-delete-{{$user->id}}" data-toggle="modal">
								<button class="btn btn-sm btn-danger">Eliminar</button></a>
							@endcan
						</td>
					</tr>
					@include('users.modal')
					@endforeach
				</table>
			</div>
			<div align="center">
				{{$users->render()}}
			</div>
		</div>
	</div>
</div>

@endsection