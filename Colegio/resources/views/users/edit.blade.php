@extends('admin.layout')

@section('contenido')

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Editar Usuario: {{$user->nombre}}</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

		{!!	Form::model($user, ['route' => ['users.update', $user->id], 'method' =>'PUT'])	!!}
		{{Form::token()}}

<div class="panel panel-primary" title="Informacion del Usuario">
	<div class="panel-body">

		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" name="name" class="form-control" value="{{$user->name}}" placeholder="Nombre...">
		</div>
		<div class="form-group">
			<label for="email">Email</label>
			<input type="text" name="email" class="form-control" value="{{$user->email}}" placeholder="Mail...">
		</div>
	</div>
</div>

<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Asignación de Permisos</h3>
	</div>
</div>
			<div class="form-group">
				<ul class="list-unstyled">

	<!-- agregado que pasa el rol -->
<div class="panel panel-primary" title="Seleccionar Rol">
	<div class="panel-body">
			@foreach($roleuser as $ru)
				<div class="form-group">
					<select name="id_rol" id="id_rol" required="true" class="form-control selectpicker">
				@foreach($roles as $role)
					@if(($ru->role_id == $role->id)&&($ru->user_id == $user->id))
							<option value="{{$ru->role_id}}" selected="true">{{$role->name}}, ({{$role->description}})</option>	
					@endif			
						@if(($role->id!="1")&&($role->id!=$ru->role_id))
							<option value="{{$role->id}}">{{$role->name}}, ({{$role->description}})</option>
						@endif
				@endforeach
			</select>
			@endforeach	
		</div>

	</div>
</div>

		<div class="form-group" align="center">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger" type="reset">Cancelar</button>
		</div>

		{!!Form::close()!!}

	</div>
	</div>

@endsection