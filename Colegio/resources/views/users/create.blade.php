@extends('admin.layout')

@section('contenido')

	<div class="panel-body">
		<div div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h3>Nuevo Usuario</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>

	{!!	Form::open(['route' => 'users.store']) !!}
	{{Form::token()}}
	
	<div class="panel panel-primary" title="Informacion del Usuario">
		<div class="panel-body">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" name="name" class="form-control" placeholder="Nombre...">
			</div>
 			<div class="form-group">
				<label for="email">Email</label>
				<input type="text" name="email" class="form-control" placeholder="Mail...">
			</div>
			<div class="form-group">
				<label for="password">Contraseña</label>
				<input type="password" name="password" class="form-control" placeholder="Contraseña...">
			</div>
			<div class="form-group">
				<label for="password-confirm">Confirmar Contraseña</label>
				<input type="password" name="password-confirm" class="form-control" placeholder="Confirmar Contraseña...">
			</div>
		</div>
	</div>
	<div class="panel-body">
		
			<h3>Asignación de Permisos</h3>
		
	</div>
<div class="form-group">
	<ul class="list-unstyled">

	<!-- agregado que pasa el rol -->
<div class="panel panel-primary" title="Seleccionar Rol">
	<div class="panel-body">


		<div class="form-group">
			<select name="id_rol" id="id_rol" required="true" class="form-control selectpicker">	
							<option value="" selected="true"></option>
				@foreach($roles as $role)
						@if($role->id!="1")
							<option value="{{$role->id}}">{{$role->name}}, ({{$role->description}})</option>
						@endif
				@endforeach
			</select>
		</div>
	</div>
</div>

			<div class="form-group" align="center">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>

			{!!Form::close()!!}
		</div>
	</div>

@endsection
